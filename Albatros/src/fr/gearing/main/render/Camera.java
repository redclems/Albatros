package fr.gearing.main.render;

import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_TRANSFORM_BIT;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushAttrib;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;

import fr.gearing.main.game.save;
import fr.gearing.main.utility.Keyboards;
import fr.veridian.main.math.Vector3f;



public class Camera {


		
		static float fov;
		static float zNear;
		static float zFar;
		public boolean position1 = false;//for have other position totu the screen replace at the middle
		
		Vector3f position;
		Vector3f rotation = new Vector3f();
		
		public Camera(Vector3f position) {
			this.position = position;
		}
		
		public Camera setPerspectiveProjection(float Tfov, float TzNear, float TzFar) {
			fov = Tfov;
		    zNear = TzNear;
		    zFar = TzFar;
			
			return this;
		}
		
		public static void getPerspectiveProjection() {
			glEnable(GL_PROJECTION);
			glLoadIdentity();
			GLU.gluPerspective(fov, (float)Display.getWidth() / (float)Display.getHeight(), zNear, zFar);
			glEnable(GL_MODELVIEW);
		}

		public void update() {
			glPushAttrib(GL_TRANSFORM_BIT);
				glRotatef(rotation.getX(), 1, 0, 0);
				glRotatef(rotation.getY(), 0, 1, 0);
				glRotatef(rotation.getZ(), 0, 0, 1);
				glTranslatef(-position.getX(), -position.getY(), -position.getZ());
			glPopMatrix();

		}
		public Camera set_mouv(float x, float y, float z) {
			position.setX(x);
			position.setY(y);
			position.setZ(z);
			return this;
		}
		
		public float get_mouvX(){
			return position.getX();
		}
		public float get_mouvY(){
			return position.getY();
		}
		public float get_mouvZ(){
			return position.getZ();
		}
		
		public void mouv() {
		  if(position1 == true){
			 position1 = false;
			 set_mouv(0.0f,0.0f, 0.2f);
			 rotation.setX(0);
			 rotation.setY(0);
		  }
		  
		  //move in the display like x, y, z and rotation of the mouse

		  if(Render.mousse_move == true) {
			rotation.addX(-Mouse.getDY() * save.mouseSpeed);
			rotation.addY(Mouse.getDX() *  save.mouseSpeed);
				
			if (rotation.getX() > 90)rotation.setX(90);
			if (rotation.getX() < -90)rotation.setX(-90);
		  }
		  
		  if(Render.move_you == true) {
			
			
			float speed = save.moveSpeed;
			if(Keyboards.sprint) {
				speed = speed*2;
			}

			if (Keyboards.forward) {
				position.addZ(-speed);
			}
			if (Keyboards.backward) {
				position.addZ(speed);
			}
			if (Keyboards.left) {
				position.addX(-speed);
			}
			if (Keyboards.right) {
				position.addX( speed);
			}
			
			if (Keyboards.up) {
				position.addY( speed);
			}
			
			if (Keyboards.down) {
				position.addY(-speed);
			}
		  }
		}
		
		public Vector3f getForward() {
			Vector3f r = new Vector3f();

			Vector3f rot = new Vector3f(rotation);
			
			float cosY = (float) Math.cos(Math.toRadians(rot.getY() - 90));
			float sinY = (float) Math.sin(Math.toRadians(rot.getY() - 90));
			float cosP = (float) Math.cos(Math.toRadians(-rot.getX()));
			float sinP = (float) Math.sin(Math.toRadians(-rot.getX()));
			
			//Euler Angles
			r.setX(cosY * cosP);
			r.setY(sinP);
			r.setZ(sinY * cosP);
			
			r.normalize();
			
			return new Vector3f(r);
		}
		
		public Vector3f getBack() {
			return new Vector3f(getForward().mul(-1));
		}
		
		public Vector3f getRight() {
			Vector3f rot = new Vector3f(rotation);
			
			Vector3f r = new Vector3f();
			r.setX((float) Math.cos(Math.toRadians(rot.getY())));
			r.setZ((float) Math.sin(Math.toRadians(rot.getY())));
			r.normalize();
			
			return new Vector3f(r);
		}
		
		public Vector3f getLeft() {
			return new Vector3f(getRight().mul(-1));
		}
		
		public Vector3f getPosition() {
			return position;
		}

		public void setPosition(Vector3f position) {
			this.position = position;
		}

		public Vector3f getRotation() {
			return rotation;
		}

		public void setRotation(Vector3f rotation) {
			this.rotation = rotation;
		}
}
