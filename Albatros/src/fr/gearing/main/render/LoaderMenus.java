package fr.gearing.main.render;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import org.lwjgl.opengl.GL11;

public class LoaderMenus {
	
	private static void openGL() {
		GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glPushMatrix();
	}
	private static void closeGL() {
		GL11.glPopMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}
	
	private static float corection_X(float X, float Z) {
		//X = X + 1.0512f * Z - 0.2029f;
		return X;
	}
	private static float corection_Y(float Y, float Z) {
		Y = Y + 0.7047f * Z - 0.1326f;
		return Y;
	}
	
	public static void setCurseur( float Height, float X,float Y, float Z) {
		openGL();
		
		X = corection_X(X, Z);
		Y = corection_Y(Y, Z);
		
		float X1 = X + Height;
		float X2 = X ;
		
		float Y1 = Y ;
		float Y2 = Y - Height;
	
		GL11.glColor4d(1,1,1,1);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);glVertex3f(X2, Y1, Z);
		glTexCoord2f(1, 0);glVertex3f(X1, Y1, Z);
		glTexCoord2f(1, 1);glVertex3f(X1, Y2, Z);
		glTexCoord2f(0, 1);glVertex3f(X2, Y2, Z);
		glEnd();
		
		closeGL();
	}
	
	public static void setTextetData(float Police, float X,float Y, float Z) {
		openGL();
		
		X = corection_X(X, Z);
		Y = corection_Y(Y, Z);
		
		
		float width = 0.75f * Police;
		float height = 1 * Police;
		
		float X1 = X + (width/2);
		float X2 = X - (width/2);
		
		float Y1 = Y + (height/2);
		float Y2 = Y - (height/2);
		
	
		GL11.glColor4d(1,1,1,1);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 1);glVertex3f(X2, Y2, Z);
		glTexCoord2f(0, 0);glVertex3f(X2, Y1, Z);
		glTexCoord2f(1, 0);glVertex3f(X1, Y1, Z);
		glTexCoord2f(1, 1);glVertex3f(X1, Y2, Z);
		glEnd();
		
		closeGL();
	}
	public static void setTrans2D(float Width, float Height, float X,float Y, float Z) {
		openGL();
		
		X = corection_X(X, Z);
		Y = corection_Y(Y, Z);
		
		float X1 = X + (Width/2);
		float X2 = X - (Width/2);
		
		float Y1 = Y + (Height/2);
		float Y2 = Y - (Height/2);
		
		GL11.glColor4d(1,1,1,1);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);glVertex3f(X2, Y1, Z);
		glTexCoord2f(1, 0);glVertex3f(X1, Y1, Z);
		glTexCoord2f(1, 1);glVertex3f(X1, Y2, Z);
		glTexCoord2f(0, 1);glVertex3f(X2, Y2, Z);
		glEnd();
		
		closeGL();
	}
}
