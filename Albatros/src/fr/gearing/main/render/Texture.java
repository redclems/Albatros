package fr.gearing.main.render;

import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.*;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;


import fr.gearing.main.Albatros;




public class Texture {
	
	public static Texture espace = new Texture(Albatros.Srepcentral + "/textures/alphabet/ .png", GL_NEAREST);
	public static Map<String, Texture> alphabetBlack;
	public static Map<String, Texture> alphabetWhite;
	public static List<List<Texture>> biomeTexture;//the first id is the type oh biome and the other id is temperature

	public static void alphabet1() {

		alphabetBlack = new HashMap<>();
		String nomFichiers = Albatros.Srepcentral + "/textures/alphabet/black";
		File alphabet = new File(nomFichiers);
		String[] listFichiers = alphabet.list();
  		  		  		
  		for(String file : listFichiers) { 
  			if(file.charAt(0) != '.') { //to don't have the hidden folder

  				alphabetBlack.put(removeLastChar(file), new Texture(nomFichiers + "/" + file, GL_NEAREST));
  			}
  		}
	}
	public static void alphabet2() {

		alphabetWhite = new HashMap<>();
		String nomFichiers = Albatros.Srepcentral + "/textures/alphabet/white";
		File alphabet = new File(nomFichiers);
		String[] listFichiers = alphabet.list();
  		  		  		
  		for(String file : listFichiers) { 
  			if(file.charAt(0) != '.') { //to don't have the hidden folder

  				alphabetWhite.put(removeLastChar(file), new Texture(nomFichiers + "/" + file, GL_NEAREST));
  			}
  		}
	}

	public static void biomeLoad() {

		List<String> nameBiome= Arrays.asList("Fantasy","Montain","Oceant","Plain");
		List<String> listFile = Arrays.asList("freeze","cold", "normal", "hot", "desert");

		biomeTexture = new ArrayList<>();
		String nomFichiers = Albatros.Srepcentral + "/textures/biome/";


		for(String name : nameBiome) {
			String nomFichier = nomFichiers + name;

			List<Texture> biome = new ArrayList<>();
			for (String file : listFile) {
				try {
					biome.add(new Texture(nomFichier + "/" + file + ".png", GL_NEAREST));
				}catch(NullPointerException e){
					System.out.println(nomFichier+"/"+file);
				}
			}
			biomeTexture.add(biome);
		}
	}
	
    private static String removeLastChar(String str) {
    	String res = "";
    	int i = 0;
    	while(i < str.length() && str.charAt(i) != '.') {
    		res = res + str.charAt(i);
    		i++;
    	}
    	return res;
    }
	
	public static Texture curs = new Texture(Albatros.Srepcentral +"/textures/icon/curseur.png", GL_NEAREST);
	
	public static Texture bpM = new Texture(Albatros.Srepcentral +"/textures/bouton/bouton.png", GL_NEAREST);
	public static Texture bpG = new Texture(Albatros.Srepcentral +"/textures/bouton/bouton2.png", GL_NEAREST);
	
	public static Texture arrowR = new Texture(Albatros.Srepcentral +"/textures/bouton/arrowR.png", GL_NEAREST);
	public static Texture arrowL = new Texture(Albatros.Srepcentral +"/textures/bouton/arrowL.png", GL_NEAREST);
	
	public static Texture fon = new Texture(Albatros.Srepcentral +"/textures/icon/Fon.png", GL_NEAREST);
	public static Texture fon2 = new Texture(Albatros.Srepcentral +"/textures/icon/Fon2.png", GL_NEAREST);
	public static Texture fonscroll = new Texture(Albatros.Srepcentral +"/textures/icon/FonScroll.png", GL_NEAREST);
	public static Texture fonscroll2 = new Texture(Albatros.Srepcentral +"/textures/icon/FonScroll2.png", GL_NEAREST);
	public static Texture scroll = new Texture(Albatros.Srepcentral +"/textures/icon/scroll.png", GL_NEAREST);
	public static Texture scroll2 = new Texture(Albatros.Srepcentral +"/textures/icon/scroll2.png", GL_NEAREST);
	public static Texture backround = new Texture(Albatros.Srepcentral +"/textures/backgroung/fonLoader.png", GL_NEAREST);
	public static Texture backroundMenus = new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST);
	public static Texture albatros = new Texture(Albatros.Srepcentral +"/textures/icon/albatros.png", GL_NEAREST);
	
	public static Texture contBAR = new Texture(Albatros.Srepcentral +"/textures/icon/contBar.png", GL_NEAREST);
	
	public static Texture BARloadBlue = new Texture(Albatros.Srepcentral +"/textures/icon/loadBlue.png", GL_NEAREST);
	public static Texture BARloadGreen = new Texture(Albatros.Srepcentral +"/textures/icon/loadGreen.png", GL_NEAREST);
	public static Texture BARloadRED = new Texture(Albatros.Srepcentral +"/textures/icon/loadRed.png", GL_NEAREST);
	public static Texture BARloadYellow = new Texture(Albatros.Srepcentral +"/textures/icon/loadyellow.png", GL_NEAREST);
	public static Texture BARloadBrown = new Texture(Albatros.Srepcentral +"/textures/icon/loadBrown.png", GL_NEAREST);
	
	public static Texture bptemp = new Texture(Albatros.Srepcentral +"/textures/bouton/tempBP.png", GL_NEAREST);
	public static Texture bpbiome = new Texture(Albatros.Srepcentral +"/textures/bouton/biomeBP.png", GL_NEAREST);
	

	public static Texture bug  = new Texture(Albatros.Srepcentral +"/textures/bug.png", GL_NEAREST);

	public static Texture bar_bot_and_top  = new Texture(Albatros.Srepcentral +"/textures/menus/baground3.jpeg", GL_NEAREST);
	public static Texture icon  = new Texture(Albatros.Srepcentral +"/textures/icon/icon.png", GL_NEAREST);
	public static Texture cases1 = new Texture(Albatros.Srepcentral +"/textures/bouton/case1.png", GL_NEAREST);
	public static Texture cases2 = new Texture(Albatros.Srepcentral +"/textures/bouton/case2.png", GL_NEAREST);
	public static Texture cases3 = new Texture(Albatros.Srepcentral +"/textures/bouton/case3.png", GL_NEAREST);
	public static Texture cases4 = new Texture(Albatros.Srepcentral +"/textures/bouton/case4.png", GL_NEAREST);
	
	public static Texture kickBP = new Texture(Albatros.Srepcentral +"/textures/bouton/kickBP.png", GL_NEAREST);
	public static Texture rotation = new Texture(Albatros.Srepcentral +"/textures/bouton/rotation.png", GL_NEAREST);
	
	public static Texture upgradeBP = new Texture(Albatros.Srepcentral +"/textures/bouton/upgradeBP.png", GL_NEAREST);
	

	public static Texture none  = new Texture(Albatros.Srepcentral +"/textures/menus/404.png", GL_NEAREST);
	public static Texture argon = new Texture(Albatros.Srepcentral +"/textures/batimenticon/argon.png", GL_NEAREST);
	
	public static Texture cross = new Texture(Albatros.Srepcentral +"/textures/batimenticon/cross.png", GL_NEAREST);
	public static Texture notenough = new Texture(Albatros.Srepcentral +"/textures/batimenticon/notenough.png", GL_NEAREST);
	public static Texture iron = new Texture(Albatros.Srepcentral +"/textures/batimenticon/iron.png", GL_NEAREST);
	public static Texture obsidienne = new Texture(Albatros.Srepcentral +"/textures/batimenticon/obsidienne.png", GL_NEAREST);
	public static Texture stone = new Texture(Albatros.Srepcentral +"/textures/batimenticon/stone.png", GL_NEAREST);
	public static Texture wood = new Texture(Albatros.Srepcentral +"/textures/batimenticon/wood.png", GL_NEAREST);
	
	public static Texture fondbuildfalse = new Texture(Albatros.Srepcentral +"/textures/batimenticon/fondbuildfalse.png", GL_NEAREST);
	public static Texture fondbuildtrue = new Texture(Albatros.Srepcentral +"/textures/batimenticon/fondbuildtrue.png", GL_NEAREST);
	
	
	public static Texture BarGame  = new Texture(Albatros.Srepcentral +"/textures/menus/barGame.png", GL_NEAREST);
	public static Texture view  = new Texture(Albatros.Srepcentral +"/textures/menus/view.png", GL_NEAREST);
	public static Texture Lbouton  = new Texture(Albatros.Srepcentral +"/textures/menus/Lbouton.png", GL_NEAREST);
	public static Texture Rbouton  = new Texture(Albatros.Srepcentral +"/textures/menus/Rbouton.png", GL_NEAREST);	
	public static Texture minimap  = new Texture(Albatros.Srepcentral +"/textures/menus/minimapC.png", GL_NEAREST);	
	public static Texture BPgame  = new Texture(Albatros.Srepcentral +"/textures/menus/BPgame.png", GL_NEAREST);	
	public static Texture Moption  = new Texture(Albatros.Srepcentral +"/textures/menus/MenusOption.png", GL_NEAREST);	
	public static Texture background  = new Texture(Albatros.Srepcentral +"/textures/menus/background4.png", GL_NEAREST);
	public static Texture Mapbackground  = new Texture(Albatros.Srepcentral +"/textures/menus/fonMap.png", GL_NEAREST);
	public static Texture Miniapbackground  = new Texture(Albatros.Srepcentral +"/textures/menus/minifonMap.png", GL_NEAREST);
	public static Texture MiniMAP  = new Texture(Albatros.Srepcentral +"/textures/menus/miniMap2.png", GL_NEAREST);
	
	public static Texture bottowerwood  = new Texture(Albatros.Srepcentral +"/textures/batiment/bottowerwood.png", GL_NEAREST);
	public static Texture toptowerwood  = new Texture(Albatros.Srepcentral +"/textures/batiment/toptowerwood.png", GL_NEAREST);
	public static Texture flortowerwood  = new Texture(Albatros.Srepcentral +"/textures/batiment/flortowerwood.png", GL_NEAREST);
	public static Texture bottoweriron  = new Texture(Albatros.Srepcentral +"/textures/batiment/bottoweriron.png", GL_NEAREST);
	public static Texture toptoweriron = new Texture(Albatros.Srepcentral +"/textures/batiment/toptoweriron.png", GL_NEAREST);
	public static Texture flortoweriron  = new Texture(Albatros.Srepcentral +"/textures/batiment/flortoweriron.png", GL_NEAREST);
	public static Texture bottowerstone  = new Texture(Albatros.Srepcentral +"/textures/batiment/bottowerstone.png", GL_NEAREST);
	public static Texture toptowerstone  = new Texture(Albatros.Srepcentral +"/textures/batiment/toptowerstone.png", GL_NEAREST);
	public static Texture flortowerstone  = new Texture(Albatros.Srepcentral +"/textures/batiment/flortowerstone.png", GL_NEAREST);
	public static Texture bottowerobs  = new Texture(Albatros.Srepcentral +"/textures/batiment/bottowerobs.png", GL_NEAREST);
	public static Texture toptowerobs  = new Texture(Albatros.Srepcentral +"/textures/batiment/toptowerobs.png", GL_NEAREST);
	public static Texture flortowerobs  = new Texture(Albatros.Srepcentral +"/textures/batiment/flortowerobs.png", GL_NEAREST);
	public static Texture doorwood = new Texture(Albatros.Srepcentral +"/textures/batiment/doorwood.png", GL_NEAREST);
	public static Texture stairgate = new Texture(Albatros.Srepcentral +"/textures/batiment/stairgate.png", GL_NEAREST);
	public static Texture marble = new Texture(Albatros.Srepcentral +"/textures/batiment/marble.png", GL_NEAREST);
	public static Texture archT = new Texture(Albatros.Srepcentral +"/textures/batiment/archT.png", GL_NEAREST);
	public static Texture DoorGraph = new Texture(Albatros.Srepcentral +"/textures/batiment/DoorGraph.png", GL_NEAREST);
	public static Texture rooftemple = new Texture(Albatros.Srepcentral +"/textures/batiment/rooftemple.png", GL_NEAREST);
	public static Texture wallwood = new Texture(Albatros.Srepcentral +"/textures/batiment/wallwood.png", GL_NEAREST);
	public static Texture trees = new Texture(Albatros.Srepcentral +"/textures/batiment/trees.png", GL_NEAREST);
	public static Texture metal = new Texture(Albatros.Srepcentral +"/textures/batiment/metal.png", GL_NEAREST);
	public static Texture woodstick = new Texture(Albatros.Srepcentral +"/textures/batiment/woodstick.png", GL_NEAREST);
	public static Texture lumberjackHousse = new Texture(Albatros.Srepcentral +"/textures/batiment/lumberjackHousse.png", GL_NEAREST);
	public static Texture military = new Texture(Albatros.Srepcentral +"/textures/batiment/military.png", GL_NEAREST);
	public static Texture roof2 = new Texture(Albatros.Srepcentral +"/textures/batiment/roof2.png", GL_NEAREST);
	public static Texture militaryHousse = new Texture(Albatros.Srepcentral +"/textures/batiment/militaryHousse.png", GL_NEAREST);
	public static Texture gravel = new Texture(Albatros.Srepcentral +"/textures/batiment/gravel.png", GL_NEAREST);
	public static Texture Mine = new Texture(Albatros.Srepcentral +"/textures/batiment/Mine.png", GL_NEAREST);
	public static Texture Farmsland = new Texture(Albatros.Srepcentral +"/textures/batiment/Farmsland.png", GL_NEAREST);
	public static Texture Wool = new Texture(Albatros.Srepcentral +"/textures/batiment/Wool.png", GL_NEAREST);
	public static Texture campgraph = new Texture(Albatros.Srepcentral +"/textures/batiment/campgraph.png", GL_NEAREST);
	public static Texture fire = new Texture(Albatros.Srepcentral +"/textures/batiment/fire.png", GL_NEAREST);
	public static Texture firecamp = new Texture(Albatros.Srepcentral +"/textures/batiment/firecamp.png", GL_NEAREST);
	public static Texture pumpArgon = new Texture(Albatros.Srepcentral +"/textures/batiment/pumpArgon.png", GL_NEAREST);
	public static Texture mageTower1 = new Texture(Albatros.Srepcentral +"/textures/batiment/mageTower.png", GL_NEAREST);
	public static Texture arrow = new Texture(Albatros.Srepcentral +"/textures/batiment/arrow.png", GL_NEAREST);
	public static Texture floor = new Texture(Albatros.Srepcentral +"/textures/batiment/floor.png", GL_NEAREST);

	
	public static Texture BuilderHousse = new Texture(Albatros.Srepcentral +"/textures/batimenticon/BuilderHousse.png", GL_NEAREST);
	public static Texture camp = new Texture(Albatros.Srepcentral +"/textures/batimenticon/camp.png", GL_NEAREST);
	public static Texture casern = new Texture(Albatros.Srepcentral +"/textures/batimenticon/casern.png", GL_NEAREST);
	public static Texture catapult = new Texture(Albatros.Srepcentral +"/textures/batimenticon/catapult.png", GL_NEAREST);
	public static Texture farms = new Texture(Albatros.Srepcentral +"/textures/batimenticon/farms.png", GL_NEAREST);
	public static Texture forge = new Texture(Albatros.Srepcentral +"/textures/batimenticon/forge.png", GL_NEAREST);

	public static Texture lumberjackhousse = new Texture(Albatros.Srepcentral +"/textures/batimenticon/lumberjackhousse.png", GL_NEAREST);
	public static Texture mageTower = new Texture(Albatros.Srepcentral +"/textures/batimenticon/mageTower.png", GL_NEAREST);
	public static Texture mine = new Texture(Albatros.Srepcentral +"/textures/batimenticon/mine.png", GL_NEAREST);
	public static Texture PumpArgon = new Texture(Albatros.Srepcentral +"/textures/batimenticon/PumpArgon.png", GL_NEAREST);
	public static Texture temple = new Texture(Albatros.Srepcentral +"/textures/batimenticon/temple.png", GL_NEAREST);
	public static Texture tower = new Texture(Albatros.Srepcentral +"/textures/batimenticon/tower.png", GL_NEAREST);
	
	public static Texture wall = new Texture(Albatros.Srepcentral +"/textures/batimenticon/wall.png", GL_NEAREST);
	public static Texture walldoor = new Texture(Albatros.Srepcentral +"/textures/batimenticon/walldoor.png", GL_NEAREST);
	public static Texture zeplin = new Texture(Albatros.Srepcentral +"/textures/batimenticon/zeplin.png", GL_NEAREST);
	public static Texture ballist = new Texture(Albatros.Srepcentral +"/textures/batimenticon/ballist.png", GL_NEAREST);
	
	
	
	public static Texture mouv      = new Texture(Albatros.Srepcentral +"/textures/personage/deplacement.png", GL_NEAREST);
	public static Texture fermium1  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion explorateur.png", GL_NEAREST);
	public static Texture fermium2  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion archer.png", GL_NEAREST);
	public static Texture fermium3  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion paysan.png", GL_NEAREST);
	public static Texture fermium4  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion minere.png", GL_NEAREST);
	public static Texture fermium5  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion mage.png", GL_NEAREST);
	public static Texture fermium6  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion guerrier.png", GL_NEAREST);
	public static Texture fermium7  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion bucheron.png", GL_NEAREST);
	public static Texture fermium8  = new Texture(Albatros.Srepcentral +"/textures/personage/Minion batisseur.png", GL_NEAREST);
	
	
	
	public static Texture T5 = new Texture(Albatros.Srepcentral +"/textures/temperature/veryHot.png", GL_NEAREST);
	public static Texture T4 = new Texture(Albatros.Srepcentral +"/textures/temperature/Hot.png", GL_NEAREST);
	public static Texture T3 = new Texture(Albatros.Srepcentral +"/textures/temperature/medium.png", GL_NEAREST);
	public static Texture T2 = new Texture(Albatros.Srepcentral +"/textures/temperature/cold.png", GL_NEAREST);
	public static Texture T1 = new Texture(Albatros.Srepcentral +"/textures/temperature/verycold.png", GL_NEAREST);
	
	
	
	
	private int id;
	private int width, height;

	public Texture(String path, int filter) {
		int[] pixels = null;
		try {
			BufferedImage image = ImageIO.read(new File(path));
			width = image.getWidth();
			height = image.getHeight();
			pixels = new int[width * height];
			image.getRGB(0, 0, width, height, pixels, 0, width);
		} catch (IOException e) {
			
			
		}
		
		int[] data = new int[pixels.length];
		for (int i = 0; i < pixels.length; i++) {
			int a = (pixels[i] & 0xff000000) >> 24;
			int r = (pixels[i] & 0xff0000) >> 16;
			int g = (pixels[i] & 0xff00) >> 8;
			int b = (pixels[i] & 0xff);

			data[i] = a << 24 | b << 16 | g << 8 | r;
		}

		int id = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, id);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);


		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		
		this.id = id;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void bind() {
		glBindTexture(GL_TEXTURE_2D, id);


	}

}

