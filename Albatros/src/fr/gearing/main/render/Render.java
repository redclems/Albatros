package fr.gearing.main.render;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import fr.gearing.main.game.Gamegenerator;
import fr.gearing.main.game.save;
import fr.gearing.main.utility.Keyboards;
import fr.gearing.main.utility.Mousse;
import fr.veridian.main.math.Vector3f;


public class Render {
	
    static Camera cam;
	
	static boolean running = false;//game is start or off
    public static boolean move_you = true;// move in the 3d screen
    public static boolean mousse_move = true; // mouv in the 3d screen which mousse
    public static boolean grabble = true; //if the mouse is grab or not

    public static boolean mousView = false;
	public static boolean click = false;
    static float Fov = 70.0f;
    static float Znear = 0.1f;
    static float Zfar = 1000.0f;
    
    public static int FPS = 0;
    public static int TICKS = 0;
    
    public static float coordonee[] = new float [3];
    
    
	
	public static void display() {
		cam = new Camera(new Vector3f(0, 0, 0));
		cam.setPerspectiveProjection(Fov, Znear, Zfar);
	}
	public static void display_button() {
		cam.position1 = true;
	}
	public static void start() {
		save.list_tipe_keyboards.add("AZERTY");
		save.list_tipe_keyboards.add("QWERTY");
		running = true;
		run();
		
	}
	public static void stop() {
		running = false;
		
	}
	public static void exit() {
		DisplayManager.dispose();
		System.exit(0);
		
	}
	public static void run() {
		
		//tick calcule
		long lastTickTime = System.nanoTime();
		long lastRenderTime = System.nanoTime();

		double tickTime = 1000000000.0 / 60.0;
		double renderTime = 1000000000.0 / save.fps;

		int ticks = 0;
		int frames = 0;

		long timer = System.currentTimeMillis();
		
		while(running) {
			renderTime = 1000000000.0 / save.fps;
			if(DisplayManager.isClosed()) {
				stop();
			}
			if (System.nanoTime() - lastTickTime > tickTime) {
				lastTickTime += tickTime;
				
				//action constante
				update();
				
				ticks++;
			} else if (System.nanoTime() - lastRenderTime > renderTime) {
				lastRenderTime += renderTime;
				
				//action
				screen();
				
				
				
				frames++;
			}else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			}

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
						 TICKS = ticks;
						 FPS = frames;
				ticks = 0;
				frames = 0;
			}
		}
		
		exit();
		
	}

	private static void update() {
		
		
		
		if(grabble == true) {//quit grab the mouse when 
			if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
					Mouse.setGrabbed(false);
					click = false;	
					mousView = false;
			}	
			
			if (Mouse.isButtonDown(0) && !Mouse.isGrabbed()) {//for grab the mouse
				Mouse.setGrabbed(true);
				mousView = true;
			}
			if (!Mouse.isGrabbed()) return;		
		}
		
		Keyboards.KEY();//listen the key
		Keyboards.keyforaction();//action for key
		
		//for mouv in the map
		cam.mouv();
		
		coordonee[0] = cam.get_mouvX();
		coordonee[1] = cam.get_mouvY();
		coordonee[2] = cam.get_mouvZ();
		
		
	}

	public static void screen() {
		
		if(Display.wasResized()) {//for mouv the size of dislplay
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
		}
		
		//update for the display
		DisplayManager.clearBuffers();
		Camera.getPerspectiveProjection();
		cam.update();
		
		//when the mouse is click on the screen of the game is tp at 0 0.1f
	    if(click == false) click = true;
	    
	    
		Gamegenerator.game();//game generation
		
	
		if (Mouse.isGrabbed()) Mousse.MoussePosition();//for follow the mouse in the screen when his grab
		
		//if (!Mouse.isGrabbed() && Gamegenerator.screen == 8) {//menus option
			
		//	Gamegenerator.pause();
		//}
			
		DisplayManager.update();
	}
	

}



