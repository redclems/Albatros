package fr.gearing.main;

import java.io.File;

import fr.gearing.main.render.DisplayManager;
import fr.gearing.main.render.Render;


public class Albatros {
	public static File repcentral= new File ("/Applications/Albatros");
	public static File repsave= new File ("/Applications/Albatros/save");
	
	public static String Srepcentral = ("/Applications/Albatros");
	public static String Srepsave = ("/Applications/Albatros/save");
	
	public Albatros() {
		DisplayManager.create(960, 480, "Albatros");
		Render.display();
	}
	
	
	public static void main(String[] args) {
		new Albatros();
		Render.start();
		
	}

}
