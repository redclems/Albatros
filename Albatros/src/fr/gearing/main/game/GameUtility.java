package fr.gearing.main.game;



import org.lwjgl.opengl.Display;

import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Render;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Keyboards;

public class GameUtility {
	
	
	public static float pixel_X(float z) {
	    int height = Display.getHeight();
	    
		float distance_view = -((Render.coordonee[2] -0.2f) + z);
		float taille_ecran_width = (480.0000f/height);

		float pixel_x = (0.0029f*distance_view + 0.00060f) * taille_ecran_width;
		return pixel_x;
	}
	
	public static float pixel_Y(float z) {
	    int height = Display.getHeight();
	    
		float distance_view = -((Render.coordonee[2] -0.2f) + z);
		float taille_ecran_height = (960.0000f/height);
    
		float pixel_y = (0.0015f*distance_view + 0.00025f) * taille_ecran_height;
		return pixel_y;
	}
	public static float change_percentage_by_Co_X_no_zoom(int coordoner, float Z) {
		/*
		 * change percentage by coordone on the display
		 */
		float width = Display.getWidth();
		float pixel = pixel_X(Z);
		//float corection = pixel*(width/2.004f);
		float X =((width/100)*coordoner) * pixel; 
		return X;
	}
	public static float change_percentage_by_Co_Y_no_zoom(int coordoner, float Z) {
		/*
		 * change percentage by coordone on the display
		 */
		float height = Display.getHeight();
		float pixel = pixel_Y(Z);
		float corection = pixel*(height/2.004f);
		float Y = ((height/100)*coordoner) * pixel + corection;
		return Y;
	}
	public static float change_percentage_by_width_no_zoom(int coordoner, float Z) {
		/*
		 * change percentage by width on the display
		 */
		int width_display = Display.getWidth();
		float width = (coordoner/100.00f) * (pixel_X(Z) * width_display); 
		return width;
	}
	public static float change_percentage_by_height_no_zoom(int coordoner, float Z) {
		/*
		 * change percentage by height on the display
		 */
		float height_display = (Display.getHeight());
		float height = (coordoner/100.00f) * (pixel_Y(Z) * height_display); 
		return height;
	}
	
	public static float change_percentage_by_Co_X(int coordoner, float Z) {
		/*
		 * change percentage by coordone on the display
		 */
		float width = Display.getWidth();
		float pixel = pixel_X(Z);
		//float corection = pixel*(width/2.004f);
		float X =((width/100)*coordoner) * pixel; 
		return X/save.zoom;
	}
	
	public static int change_percentage_by_Co_X_float(float X, float Z) {
		/*
		 * change percentage by coordone on the display
		 */
		float width = Display.getWidth();
		float pixel = pixel_X(Z);
		//float corection = pixel*(width/2.004f);
		int x =(int) ((X/pixel)/(width/100));  
		return (int) (x*save.zoom);
	}
	public static float change_percentage_by_Co_Y(int coordoner, float Z) {
		/*
		 * change percentage by coordone on the display
		 */
		float height = Display.getHeight();
		float pixel = pixel_Y(Z);
		float corection = pixel*(height/2.004f);
		float Y = ((height/100)*coordoner) * pixel + corection*save.zoom;
		return Y/save.zoom;
	}
	public static int change_percentage_by_Co_Y_float(float y, float Z) {
		/*
		 * change percentage by coordone on the display in float
		 */
		float height = Display.getHeight();
		float pixel = pixel_Y(Z);
		float corection = pixel*(height/2.004f);
		int Y = (int) (((((y-corection*save.zoom)/pixel) / (height/100)))*save.zoom); 
		return Y;
	}
	
	public static float change_percentage_by_width(int coordoner, float Z) {
		/*
		 * change percentage by width on the display
		 */
		int width_display = Display.getWidth();
		float width = (coordoner/100.00f) * (pixel_X(Z) * width_display); 
		return width/save.zoom;
	}
	public static float change_percentage_by_height(int coordoner, float Z) {
		/*
		 * change percentage by height on the display
		 */
		float height_display = (Display.getHeight());
		float height = (coordoner/100.00f) * (pixel_Y(Z) * height_display); 
		return height/save.zoom;
	}
	public static int change_percentage_by_height_float(float height, float Z) {
		/*
		 * change percentage by coordone on the display in float
		 */
		float height_display = Display.getHeight();
		int coordoner =(int) ((height / (pixel_Y(Z) * height_display))*100); 
		return (int) (coordoner*save.zoom);
	}

	public static void setBar() {
		/*
		 * this function create banner for choose the texture of banner use Texture.backround.bind(); before this function
		 * 
		 */
		float Z = -2;
		float height_width_zoom = change_percentage_by_height(100, Z);
		float width = change_percentage_by_width_no_zoom(100, Z);
		float height = change_percentage_by_height_no_zoom(50, Z);
		float x = change_percentage_by_Co_X_no_zoom(0, Z);
		float y1 = change_percentage_by_Co_Y_no_zoom(0, Z) + (height_width_zoom/2 + height/2);
		float y2 = change_percentage_by_Co_Y_no_zoom(0, Z) - (height_width_zoom/2 + height/2);

		Texture.bar_bot_and_top.bind();
		LoaderMenus.setTrans2D(width, height, x, y1, Z);	
		LoaderMenus.setTrans2D(width, height, x, y2, Z);
	}
	
	public static void setBarPosition(int Height, int X, float Y ) {
		/*
		 * this function create banner for choose the texture of banner use Texture.backround.bind(); before this function
		 * 
		 */
		float Z = -5;
		float width = change_percentage_by_width_no_zoom(100, Z);
		float height = change_percentage_by_height(Height, Z);
		float x = change_percentage_by_Co_X(X, Z);
		float y = Y;


		Texture.bar_bot_and_top.bind();
		LoaderMenus.setTrans2D(width, height, x, y, Z);	

	}
	
	public static float echelleToOther(float Val, float Vmin, float Vmax, float Vmin_new, float Vmax_new) {
		/*
		 * 
		 */
		
		double p1 = Val-Vmin;
		double p2 = Vmax-Vmin;
		double p3 = p1/p2;

		double p4 = Vmax_new-Vmin_new;
		double p5 = p4 * p3;
		
		double p6 = p5 + Vmin_new;
		
		return (float) p6;
	}
	public static int echelleToOther(int Val, int Vmin, int Vmax, int Vmin_new, int Vmax_new) {
		/*
		 * 
		 */
		double p1 = Val-Vmin;
		double p2 = Vmax-Vmin;
		double p3 = p1/p2;

		double p4 = Vmax_new-Vmin_new;
		double p5 = p4 * p3;
		
		double p6 = p5 + Vmin_new;

		
		return (int) p6;
	}	
	
	public static int echelleToOther(float Val, float Vmin, float Vmax, int Vmin_new, int Vmax_new) {
		double p1 = Val-Vmin;
		double p2 = Vmax-Vmin;
		double p3 = p1/p2;

		double p4 = Vmax_new-Vmin_new;
		double p5 = p4 * p3;
		
		double p6 = p5 + Vmin_new;

		
		return (int) p6;
	}
	public static void echap(String lieux) {
		/*
		 * for go to the fenetre when echap is using
		 */
		
		if(Keyboards.kickMenu) { 
			Gamegenerator.screen = lieux;
			Gamegenerator.switche = true;
		}
	}


	
	

}
