package fr.gearing.main.game;

import java.util.ArrayList;
import java.util.List;

public class save {
	
	//option
	public static float zoom = 1.25f;
	public static int zoomInt = 2;
	public static int fps = 100;
	public  static boolean fpsview = false;
	public  static int chunks = 10;//distance render to the game
	public  static float tchunk = 1.0f;//size of the case
	public  static float tstructure = tchunk/3;//size of the case
	
	//music
	public  static int songMain = 100;
	public  static int songGame = 100; 
	public  static int songMenus = 100;
	public  static int songVox = 100;
	public  static int songMusic = 100;
	
	//option move mouse
	public  static float mouseSpeed = 0.3f;
	public  static float moveSpeed = 0.01f;
	
	//key for have action
	//NameEntersQWERTY, Render, Gamegnerator use this List
	public static List<String> list_tipe_keyboards = new ArrayList<String>();
	public static int type_keyboards = 0;
	
	public static int forward = 23;
	public static  int backward = 19;
	public static  int right = 4;
	public static  int left = 1;
	public static  int sprint = 17;
	public static  int up = 29;
	public static  int down = 31;
	public static  int kick = 27;
	
	public static  int rotation = 18;//action rotation for build in the map
		
	
	//durability of structure to level1 for the level 2 it is necessary to multiply by 2
	public static  int dTower = 100;
	public static  int dWall = 100;
	public static  int dDoor = 100;
	
	public static  int nbBiome = 5;//number of biome in the game
	//prix batiment
	public static  int woodP[] = new int[999];
	public static  int stoneP[] = new int[999];
	public static  int ironP[] = new int[999];
	public static  int argonP[] = new int[999];
	public static  int obsidienneP[] = new int[999];
	
	//niv for its posible to build the batiment
	public static  int batiment[] = new int[999];
	
	
	public void up() {
		
		int niv = 0; 
		int b = 0;
		//1
		for(int N = 0; N < 4; N++) {
			batiment[1 + b] = 0 + niv;
			batiment[2 + b] = 1 + niv;
			batiment[3 + b] = 1 + niv;
			batiment[4 + b] = 1 + niv;
			batiment[5 + b] = 1 + niv;
			batiment[6 + b] = 1 + niv;
			batiment[7 + b] = 1 + niv;
			batiment[8 + b] = 1 + niv;
			batiment[9 + b] = 10 + niv;
			batiment[10 + b] = 10 + niv;
			batiment[11 + b] = 1 + niv;
			batiment[12 + b] = 20 + niv;
			batiment[13 + b] = 25 + niv;
			batiment[14 + b] = 30 + niv;
			batiment[15 + b] = 45 + niv;
			batiment[16 + b] = 48 + niv;
			niv = niv + 50;
			b = b + 100;
		}
		//2
		

		
	}
	
	
	public void pay() {
		//niv 1
			//temple
				int i = 1;
				woodP[i] = 50;
				stoneP[i] = 200;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;
			//tower
				i ++;
				woodP[i] = 30;
				stoneP[i] = 0;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//wall
				i++;
				woodP[i] = 20;
				stoneP[i] = 0;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//wall door
				i++ ;
				woodP[i] = 25;
				stoneP[i] = 0;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;
			//lumberjackHousse
				i++ ;
				woodP[i] = 30;
				stoneP[i] = 5;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//mine
				i++ ;
				woodP[i] = 10;
				stoneP[i] = 20;
				ironP[i] = 5;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//farms
				i++ ;
				woodP[i] = 30;
				stoneP[i] = 5;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//builderHousse
				i++ ;
				woodP[i] = 30;
				stoneP[i] = 5;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//forge
				i++ ;
				woodP[i] = 45;
				stoneP[i] = 10;
				ironP[i] = 10;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//casern
				i++ ;
				woodP[i] = 10;
				stoneP[i] = 60;
				ironP[i] = 5;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//camp
				i++ ;
				woodP[i] = 15;
				stoneP[i] = 2;
				ironP[i] = 0;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//pump argon
				i++ ;
				woodP[i] = 5;
				stoneP[i] = 30;
				ironP[i] = 100;
				argonP[i] = 0;
				obsidienneP[i] = 0;	
			//catapult
				i++ ;
				woodP[i] = 100;
				stoneP[i] = 20;
				ironP[i] = 50;
				argonP[i] = 5;
				obsidienneP[i] = 0;	
			//ballist
				i++ ;
				woodP[i] = 200;
				stoneP[i] = 20;
				ironP[i] = 50;
				argonP[i] = 5;
				obsidienneP[i] = 0;	
			//tower mage
				i++ ;
				woodP[i] = 100;
				stoneP[i] = 300;
				ironP[i] = 5;
				argonP[i] = 5;
				obsidienneP[i] = 0;	
			//zeplin
				i++ ;
				woodP[i] = 0;
				stoneP[i] = 0;
				ironP[i] = 50;
				argonP[i] = 100;
				obsidienneP[i] = 5;	
				
			//niv 2
				//temple
					i = 101;
					woodP[i] = 50;
					stoneP[i] = 100;
					ironP[i] = 0;
					argonP[i] = 100;
					obsidienneP[i] = 10;
				//tower
					i ++;
					woodP[i] = 0;
					stoneP[i] = 35;
					ironP[i] = 0;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//wall
					i++;
					woodP[i] = 0;
					stoneP[i] = 25;
					ironP[i] = 0;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//wall door
					i++ ;
					woodP[i] = 5;
					stoneP[i] = 30;
					ironP[i] = 0;
					argonP[i] = 0;
					obsidienneP[i] = 0;
				//lumberjackHousse
					i++ ;
					woodP[i] = 60;
					stoneP[i] = 10;
					ironP[i] = 5;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//mine
					i++ ;
					woodP[i] = 30;
					stoneP[i] = 20;
					ironP[i] = 15;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//farms
					i++ ;
					woodP[i] = 60;
					stoneP[i] = 10;
					ironP[i] = 0;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//builderHousse
					i++ ;
					woodP[i] = 60;
					stoneP[i] = 10;
					ironP[i] = 0;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//forge
					i++ ;
					woodP[i] = 80;
					stoneP[i] = 20;
					ironP[i] = 20;
					argonP[i] = 5;
					obsidienneP[i] = 0;	
				//casern
					i++ ;
					woodP[i] = 20;
					stoneP[i] = 80;
					ironP[i] = 10;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//camp
					i++ ;
					woodP[i] = 35;
					stoneP[i] = 6;
					ironP[i] = 0;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//pump argon
					i++ ;
					woodP[i] = 5;
					stoneP[i] = 30;
					ironP[i] = 100;
					argonP[i] = 0;
					obsidienneP[i] = 0;	
				//catapult
					i++ ;
					woodP[i] = 200;
					stoneP[i] = 30;
					ironP[i] = 100;
					argonP[i] = 15;
					obsidienneP[i] = 0;	
				//ballist
					i++ ;
					woodP[i] = 400;
					stoneP[i] = 35;
					ironP[i] = 100;
					argonP[i] = 15;
					obsidienneP[i] = 0;	
				//tower mage
					i++ ;
					woodP[i] = 200;
					stoneP[i] = 500;
					ironP[i] = 10;
					argonP[i] = 20;
					obsidienneP[i] = 5;	
				//zeplin
					i++ ;
					woodP[i] = 0;
					stoneP[i] = 0;
					ironP[i] = 100;
					argonP[i] = 300;
					obsidienneP[i] = 10;	
					
			//niv 3
					//temple
						i = 201;
						woodP[i] = 100;
						stoneP[i] = 200;
						ironP[i] = 10;
						argonP[i] = 400;
						obsidienneP[i] = 40;
					//tower
						i ++;
						woodP[i] = 0;
						stoneP[i] = 0;
						ironP[i] = 55;
						argonP[i] = 0;
						obsidienneP[i] = 0;	
					//wall
						i++;
						woodP[i] = 0;
						stoneP[i] = 0;
						ironP[i] = 50;
						argonP[i] = 0;
						obsidienneP[i] = 0;	
					//wall door
						i++ ;
						woodP[i] = 10;
						stoneP[i] = 0;
						ironP[i] = 50;
						argonP[i] = 0;
						obsidienneP[i] = 0;
					//lumberjackHousse
						i++ ;
						woodP[i] = 120;
						stoneP[i] = 30;
						ironP[i] = 10;
						argonP[i] = 0;
						obsidienneP[i] = 0;	
					//mine
						i++ ;
						woodP[i] = 100;
						stoneP[i] = 80;
						ironP[i] = 35;
						argonP[i] = 0;
						obsidienneP[i] = 10;	
					//farms
						i++ ;
						woodP[i] = 120;
						stoneP[i] = 20;
						ironP[i] = 0;
						argonP[i] = 2;
						obsidienneP[i] = 0;	
					//builderHousse
						i++ ;
						woodP[i] = 60;
						stoneP[i] = 10;
						ironP[i] = 0;
						argonP[i] = 0;
						obsidienneP[i] = 0;	
					//forge
						i++ ;
						woodP[i] = 160;
						stoneP[i] = 40;
						ironP[i] = 40;
						argonP[i] = 10;
						obsidienneP[i] = 5;	
					//casern
						i++ ;
						woodP[i] = 50;
						stoneP[i] = 160;
						ironP[i] = 30;
						argonP[i] = 0;
						obsidienneP[i] = 10;	
					//camp
						i++ ;
						woodP[i] = 45;
						stoneP[i] = 10;
						ironP[i] = 0;
						argonP[i] = 5;
						obsidienneP[i] = 0;	
					//pump argon
						i++ ;
						woodP[i] = 10;
						stoneP[i] = 60;
						ironP[i] = 200;
						argonP[i] = 40;
						obsidienneP[i] = 10;	
					//catapult
						i++ ;
						woodP[i] = 400;
						stoneP[i] = 60;
						ironP[i] = 200;
						argonP[i] = 30;
						obsidienneP[i] = 10;	
					//ballist
						i++ ;
						woodP[i] = 700;
						stoneP[i] = 75;
						ironP[i] = 200;
						argonP[i] = 30;
						obsidienneP[i] = 10;	
					//tower mage
						i++ ;
						woodP[i] = 800;
						stoneP[i] = 1000;
						ironP[i] = 20;
						argonP[i] = 40;
						obsidienneP[i] = 10;	
					//zeplin
						i++ ;
						woodP[i] = 0;
						stoneP[i] = 0;
						ironP[i] = 200;
						argonP[i] = 600;
						obsidienneP[i] = 50;	
			//niv 4
					//temple
						i = 301;
						woodP[i] = 200;
						stoneP[i] = 400;
						ironP[i] = 20;
						argonP[i] = 800;
						obsidienneP[i] = 80;
					//tower
						i ++;
						woodP[i] = 0;
						stoneP[i] = 0;
						ironP[i] = 0;
						argonP[i] = 0;
						obsidienneP[i] = 70;	
					//wall
						i++;
						woodP[i] = 0;
						stoneP[i] = 0;
						ironP[i] = 0;
						argonP[i] = 0;
						obsidienneP[i] = 65;	
					//wall door
						i++ ;
						woodP[i] = 20;
						stoneP[i] = 0;
						ironP[i] = 0;
						argonP[i] = 0;
						obsidienneP[i] = 65;
					//lumberjackHousse
						i++ ;
						woodP[i] = 240;
						stoneP[i] = 60;
						ironP[i] = 20;
						argonP[i] = 5;
						obsidienneP[i] = 0;	
					//mine
						i++ ;
						woodP[i] = 200;
						stoneP[i] = 160;
						ironP[i] = 70;
						argonP[i] = 0;
						obsidienneP[i] = 20;	
					//farms
						i++ ;
						woodP[i] = 240;
						stoneP[i] = 40;
						ironP[i] = 0;
						argonP[i] = 5;
						obsidienneP[i] = 0;	
					//builderHousse
						i++ ;
						woodP[i] = 120;
						stoneP[i] = 20;
						ironP[i] = 0;
						argonP[i] = 10;
						obsidienneP[i] = 0;	
					//forge
						i++ ;
						woodP[i] = 320;
						stoneP[i] = 80;
						ironP[i] = 80;
						argonP[i] = 20;
						obsidienneP[i] = 10;	
					//casern
						i++ ;
						woodP[i] = 100;
						stoneP[i] = 320;
						ironP[i] = 60;
						argonP[i] = 5;
						obsidienneP[i] = 20;	
					//camp
						i++ ;
						woodP[i] = 90;
						stoneP[i] = 30;
						ironP[i] = 0;
						argonP[i] = 10;
						obsidienneP[i] = 0;	
					//pump argon
						i++ ;
						woodP[i] = 30;
						stoneP[i] = 120;
						ironP[i] = 400;
						argonP[i] = 80;
						obsidienneP[i] = 25;	
					//catapult
						i++ ;
						woodP[i] = 900;
						stoneP[i] = 120;
						ironP[i] = 400;
						argonP[i] = 60;
						obsidienneP[i] = 20;	
					//ballist
						i++ ;
						woodP[i] = 1400;
						stoneP[i] = 150;
						ironP[i] = 400;
						argonP[i] = 60;
						obsidienneP[i] = 20;	
					//tower mage
						i++ ;
						woodP[i] = 1600;
						stoneP[i] = 2000;
						ironP[i] = 40;
						argonP[i] = 80;
						obsidienneP[i] = 20;	
					//zeplin
						i++ ;
						woodP[i] = 0;
						stoneP[i] = 0;
						ironP[i] = 500;
						argonP[i] = 1200;
						obsidienneP[i] = 100;
						
										
	}

	
	
	
	
	
	
	
}
