package fr.gearing.main.game;

import fr.gearing.main.Mappeur.Map;
import fr.gearing.main.Mappeur.ParamMap;
import fr.gearing.main.Structure.TestStructure;
import fr.gearing.main.render.Render;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.ColorPrint;
import fr.gearing.main.utility.Keyboards;
import fr.gearing.main.utility.Mousse;
import fr.gearing.main.utility.Screen.Screen;
import fr.gearing.main.utility.Screen.ScreenSave;




public class Gamegenerator {

		
	public static String screen = "start";//etat du jeux
	public static boolean bar = false; //if there are a bar to the bottom and the top of the display
	public static boolean switche = true; 
	private static int milisecond = 0;
	
	public static Map carte;

	private static Screen display;
	private static ScreenSave displaySave;
	private static final TestStructure struc = new TestStructure();

	
	public static void game() {
				
		Mousse.mousseClick();
		milisecond += 1;
		if(switche) {
			gameload();
			switche = false;
			
		}else{
			//all load
			
			if(bar)GameUtility.setBar();

			switch (screen) {
				case "start" -> Start();
				case "Menus" -> Menus();
				case "MenusGame" -> MenusGame();
				case "Options" -> options();
				case "Option music" -> optionsMusic();
				case "Option display" -> optionsDisplay();
				case "Option Keybords" -> optionKeybords();
				case "Option graphics" -> optionGraphics();
				case "NewGame" -> new_game();
				case "generateGame" -> generateGame();
				case "Game" -> displayGame();
				case "Test" -> Test();
			}
		
		}
		
		if(Render.mousView) {
			Mousse.MoussePosition();
		}
		
		
	}
	private static void  gameload() {

		System.out.println(ColorPrint.GREEN + "------" + screen + "-------" + ColorPrint.RESET);
		Mousse.event_click = true;
		milisecond = 0;
		
		if(screen.equals( "start")) {
			displaySave = new ScreenSave();
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = false;

			display = displaySave.getScreen("Load");
			
			
		}
		if(screen.equals( "Menus")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = false;
			
			display = displaySave.getScreen("Menus");

		}
		if(screen.equals( "MenusGame")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = true;
			
			display = displaySave.getScreen("MenusGame");
		}

		if(screen.equals( "NewGame")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = true;
			
			display = displaySave.getScreen("NewGame");

		}
		if(screen.equals( "Options")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = true;
			
			display = displaySave.getScreen("Options");

		}
		
		if(screen.equals( "Option music")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = true;		
			
			display = displaySave.getScreen("OptionsMusic");

			
		}
		if(screen.equals( "Option display")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = true;
			bar = true;
			
			display = displaySave.getScreen("OptionsDisplay");
		}
		if(screen.equals("Option Keybords")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = true;
			
			display = displaySave.getScreen("OptionsKey");
			
		}
		if(screen.equals("Option graphics")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = true;
		
			display = displaySave.getScreen("OptionsGraphics");

		}
		if(screen.equals("Test")) {
			Render.grabble = false;
			Render.move_you = true;
			Render.mousse_move = true;
			bar = false;
			
		}
		
		if(screen.equals("generateGame")) {
			Render.display_button();
			Render.grabble = false;
			Render.move_you = false;
			Render.mousse_move = false;
			bar = false;
			
			display = displaySave.getScreen("Load2");
			display.barSet(0, 0);
		}
		if(screen.equals("Game")) {
			Render.display_button();
			Render.grabble = true;
			Render.move_you = true;
			Render.mousse_move = false;
			bar = false;
		}
		
	}
	
	private static void  Start(){
		//for move the screen
		display.Display();
		
		//load of the game
		if(milisecond > 1) {
			milisecond = 0;
			display.barAdd(0, 1);
		}
		if(display.barGet(0) == 1) displaySave.screen2();
		else if(display.barGet(0) == 2) displaySave.screen3();
		else if(display.barGet(0) == 3) displaySave.screen4();
		else if(display.barGet(0) == 4) displaySave.screen5();
		else if(display.barGet(0) == 5) displaySave.screen6();
		else if(display.barGet(0) == 6) displaySave.screen7();
		else if(display.barGet(0) == 7) displaySave.screen8();
		else if(display.barGet(0) == 8) displaySave.screen9();
		else if(display.barGet(0) == 9) Texture.alphabet1();
		else if(display.barGet(0) == 10) Texture.alphabet2();
		else if(display.barGet(0) == 11) displaySave.screen10();
		else if(display.barGet(0) == 12) Texture.biomeLoad();
		
		//when the load is end go to the menus
		if(display.barGet(0) >= 100) {
			screen = "generateGame"; //a changer pour lancer le jeux avec les menus
			switche = true;
		}

	}
	
	private static void Menus(){
		display.Display();
	}
	private static void MenusGame(){
		GameUtility.echap("Menus");
		display.Display();
	}
	private static void new_game() {
		GameUtility.echap("Menus");
		display.Display();
	}
	private static void options() {
		GameUtility.echap("Menus");
		display.Display();
	}
	private static void optionsMusic() {
		GameUtility.echap("Options");
		display.Display();
		save.songMain = display.updateVerticalScroll(0);
		save.songGame = display.updateVerticalScroll(1);
		save.songMenus = display.updateVerticalScroll(2);
		save.songVox = display.updateVerticalScroll(3);
		save.songMusic = display.updateVerticalScroll(4);
		
		if(Keyboards.B612) {
			switche = true;
			screen = "Test";
		}
	}
	private static void optionsDisplay() {
		GameUtility.echap("Options");
		display.Display();
		
		save.zoomInt = display.updateVerticalScroll(0);
	}	
	
	private static void optionKeybords() {
		GameUtility.echap("Options");
		display.Display();
		
		save.type_keyboards = display.updateButtonServeralSwicth(0);

	}	
	private static void optionGraphics() {
		GameUtility.echap("Options");
		display.Display();
		
		save.chunks = display.updateVerticalScroll(0);
		save.fps = display.updateVerticalScroll(1);
		
		save.fpsview  = display.updateButtonSwicth(0);
		
	}

	public static void Test(){
	//is secret setting for launch this you going in the option menus and you press B 6 1 2 and for kick this use escape
			GameUtility.echap("Options");
						
			struc.display();
		 	
		}
	public static void generateGame() {
		display.Display();
				
		//load of the game
		//load of the game
		//load of the game
		if(milisecond >= 1) {
			milisecond = 0;
			display.barAdd(0, 1);
		}
		
		if(display.barGet(0) == 1) carte = new Map(new ParamMap(100,200, 234));//create here the Map
		if(display.barGet(0) == 2) carte.CorectifMap();
		else if(display.barGet(0) == 20) carte.fillBiome();
		else if(display.barGet(0) == 30) carte.fillTemperature();
		else if(display.barGet(0) == 40) carte.fillRelief();
		else if(display.barGet(0) == 50) carte.fillVegetation();
		else if(display.barGet(0) == 60) carte.fillStructure();
		else if(display.barGet(0) == 100) System.out.println("-->" + ColorPrint.RED + " The map has been load !" + ColorPrint.RESET);
		
		//when the load is end go to the menus
		if(display.barGet(0) >= 100) {
			screen = "Game";
			switche = true;
		}
		
	}
	
	public static void displayGame() {
		carte.displayMap();
		/*
		GameRun T1 = new GameRun("DisplayMap", 1, carte);
	    T1.start();
	      
	    GameRun T2 = new GameRun("Thread-2", 2, carte);
	    T2.start();

		 */
		

	}
}


		
	

