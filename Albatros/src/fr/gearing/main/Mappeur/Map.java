package fr.gearing.main.Mappeur;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Mappeur.Biome.*;
import fr.gearing.main.Mappeur.PerlinNoise.PerlinCenter;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;


public class Map {

	private ParamMap param ;



	//list Chunk
	List<Chunk> chunkInMap = new ArrayList<>();

	private double minMap;
	private double maxMap;
	private double intervalleMap;

	private List<Double> noiseBiome;
	private List<Double> noiseTemperature;
	
	public Map(ParamMap param) {
		/*
		 * create map for the game
		 */
		this.param = param;
		//System.out.println("map");


		float x = -170;
		float y = -150;
		float z = -200;
		
		/*
		 * void map 
		 */
		float size = (save.tchunk*4);
		int num_chunk = param.getNBColonne()*param.getNBLigne();
		for(int lig = 0; lig < param.getNBLigne(); lig++) {
			for(int col = 0; col < param.getNBColonne(); col++) {
				//Create chunk for the map
				Chunk chunk = new Chunk(num_chunk, 0, x+size*col, y+size*lig, z);
				chunkInMap.add(chunk);

				num_chunk++;

			}
		}
	}

	public void CorectifMap(){
		//for biome
		noiseBiome = PerlinCenter.noise(param.getseed(), param.getNBLigne()*4, param.getNBColonne()*4,param.getpersistence(), param.getfrequence(), param.getamplitude(), param.getoctave());

		//System.out.println("-------- la ---------");

		//System.out.println(noise);
		minMap = PerlinCenter.getMin();
		maxMap = PerlinCenter.getMax();

		intervalleMap = Math.abs(maxMap - minMap);

		//this loop is for have a good map
		while (intervalleMap > 30){
			param.addSeed();
			noiseBiome = PerlinCenter.noise(param.getseed(), param.getNBLigne()*4, param.getNBColonne()*4, param.getpersistence(), param.getfrequence(), param.getamplitude(), param.getoctave());
			minMap = PerlinCenter.getMin();
			maxMap = PerlinCenter.getMax();
			intervalleMap = Math.abs(maxMap - minMap);
		}
		/*
		System.out.println("min :"+ min);
		System.out.println("max :"+ max);

		System.out.println(intervalle);
		*/
	}

	public void fillBiome(){
	/*
	 * fill the map and put the biome here
	 */

		Biomes fantasy = new Fantasy(param.getPourcentRare(), 3);
		Biomes montain = new Montain(param.getPourcentMontagne(), 3);
		Biomes oceant = new Oceant(param.getPourcentOceant(), 3);
		Biomes plain = new Plain(param.getPourcentPlain(), 3);

		/*
		System.out.println(param.getPourcentRare());
		System.out.println(param.getPourcentMontagne());
		System.out.println(param.getPourcentOceant());
		System.out.println(param.getPourcentPlain());
		System.out.println("addition " + (param.getPourcentOceant() + param.getPourcentMontagne() + param.getPourcentRare() + param.getPourcentPlain()) );
	*/

		double OnePercentNoise = intervalleMap/100;

	/*
		System.out.println(min + " -> value min");
		System.out.println(max + " -> value max");
		System.out.println(OnePercentNoise + " -> 1%");
		System.out.println((OnePercentNoise * fantasy.getChance())+(OnePercentNoise * montain.getChance())+(OnePercentNoise * oceant.getChance())+(OnePercentNoise * plain.getChance()));
		System.out.println("seed -> " + seed);
		System.out.println("-----------");
	 */
	//this list is just for see the maap
		List<List<List<Integer>>> map = new ArrayList<>();
		List<List<Integer>> ligne = new ArrayList<>();

		int i;
		int iChunk = 1;

		for(Chunk chunk : chunkInMap) {
			//System.out.println("->new Chunk");
			List<Integer> Lchunk = new ArrayList<>();
			i=iChunk;
			int iLCase = 1;
			for(Cases cases : chunk.getcases()){
				//System.out.println(i+1);
				Lchunk.add(i);
				double valueNoise = 0;
				try {
					valueNoise = noiseBiome.get(i-1);
				}catch (IndexOutOfBoundsException e){
					//System.out.println(i+"maxx");
					//System.out.println(noise.size());
				}
				if(valueNoise < minMap + (OnePercentNoise * fantasy.getChance())){
					cases.setBiome(fantasy);
				}else if(valueNoise < minMap + (OnePercentNoise * fantasy.getChance()) + (OnePercentNoise * montain.getChance())){
					cases.setBiome(montain);
				}else if(valueNoise < minMap + (OnePercentNoise * fantasy.getChance()) + (OnePercentNoise * montain.getChance()) + (OnePercentNoise * plain.getChance())){
					cases.setBiome(plain);
				}else if(valueNoise < minMap + (OnePercentNoise * fantasy.getChance()) + (OnePercentNoise * montain.getChance()) + (OnePercentNoise * oceant.getChance()) + (OnePercentNoise * plain.getChance())+1){
					cases.setBiome(oceant);
				}else{
					System.out.println( "--------");
					System.out.println(valueNoise + " -> value");
					cases.setBiome(null);
				}
				if(i%4==0){
					i = iChunk+((4*param.getNBColonne())*iLCase);
					iLCase++;
				}else{
					i++;
				}
			}
			ligne.add(Lchunk);

			if((iChunk+3)%(param.getNBColonne()*4) == 0){
				iChunk = (iChunk+3)+((param.getNBColonne()*4)*3)+1;
				map.add(ligne);
				ligne = new ArrayList<>();
			}else{
				iChunk+=4;
			}
		}
		/*
		for(List<List<Integer>> lig : map){
			System.out.println(lig);
		}
		System.out.println(map.size());
		*/
		System.out.println("Biome was load");
	}
	public void fillTemperature(){



		noiseTemperature = PerlinCenter.noise(param.getseed(), param.getNBLigne()*4, param.getNBColonne()*4, param.getpersistence()*4, param.getfrequence()/2, param.getamplitude(), param.getoctave());

		double minNoise = PerlinCenter.getMin();
		double maxNoise = PerlinCenter.getMax();
		double intervale = Math.abs(maxNoise - minNoise);
		double OPN = intervale/100;//one pourcent of noise list

		Biomes freezeFantasy = new Fantasy(param.getPourcentFreezeTemp(1), 1);
		Biomes freezeMontain = new Montain(param.getPourcentFreezeTemp(2), 1);
		Biomes freezeOceant = new Oceant(param.getPourcentFreezeTemp(3), 1);
		Biomes freezePlain = new Plain(param.getPourcentFreezeTemp(4), 1);

		Biomes coldFantasy = new Fantasy(param.getPourcentColdTemp(1), 2);
		Biomes coldMontain = new Montain(param.getPourcentColdTemp(2), 2);
		Biomes coldOceant = new Oceant(param.getPourcentColdTemp(3), 2);
		Biomes coldPlain = new Plain(param.getPourcentColdTemp(4), 2);

		Biomes fantasy = new Fantasy(param.getPourcentNormalTemp(1), 3);
		Biomes montain = new Montain(param.getPourcentNormalTemp(2), 3);
		Biomes oceant = new Oceant(param.getPourcentNormalTemp(3), 3);
		Biomes plain = new Plain(param.getPourcentNormalTemp(4), 3);

		Biomes hotFantasy = new Fantasy(param.getPourcentHotTemp(1), 4);
		Biomes hotMontain = new Montain(param.getPourcentHotTemp(2), 4);
		Biomes hotOceant = new Oceant(param.getPourcentHotTemp(3), 4);
		Biomes hotPlain = new Plain(param.getPourcentHotTemp(4), 4);

		Biomes desertFantasy = new Fantasy(param.getPourcentDesertTemp(1), 5);
		Biomes desertMontain = new Montain(param.getPourcentDesertTemp(2), 5);
		Biomes desertOceant = new Oceant(param.getPourcentDesertTemp(3), 5);
		Biomes desertPlain = new Plain(param.getPourcentDesertTemp(4), 5);

		int i;
		int iChunk = 1;

		List<List<List<Integer>>> map = new ArrayList<>();
		List<List<Integer>> ligne = new ArrayList<>();

		/*
		System.out.println(param.getPourcentFreezeTemp());
		System.out.println(param.getPourcentColdTemp());
		System.out.println(param.getPourcentNormalTemp());
		System.out.println(param.getPourcentHotTemp());
		System.out.println(param.getPourcentDesertTemp());

		 */
		/*
		System.out.println(minNoise + " -> value min");
		System.out.println(maxNoise + " -> value max");
		System.out.println(OPN + " -> 1%");
		System.out.println(minNoise + (((float)OPN * param.getPourcentFreezeTemp()) + ((float)OPN * param.getPourcentColdTemp()) + ((float)OPN * param.getPourcentNormalTemp()) + ((float)OPN * param.getPourcentHotTemp())+ ((float)OPN * param.getPourcentDesertTemp())));

		System.out.println("total = "  +(param.getPourcentFreezeTemp() + param.getPourcentColdTemp() + param.getPourcentNormalTemp() + param.getPourcentHotTemp() + param.getPourcentDesertTemp()+1));

		 */
		for(Chunk chunk : chunkInMap) {
			//System.out.println("->new Chunk");
			List<Integer> Lchunk = new ArrayList<>();
			i=iChunk;
			int iLCase = 1;
			for(Cases cases : chunk.getcases()){
				//System.out.println(i+1);
				double valueNoise = 0;
				try {
					valueNoise = noiseTemperature.get(i-1);
					//System.out.println("valueNoise -> " + valueNoise);
				}catch (IndexOutOfBoundsException e){
					//System.out.println(i+"maxx");
					//System.out.println(noise.size());
				}
				//System.out.println("-> " +   minNoise + (OPN * param.getPourcentFreezeTemp()));
				//System.out.println(valueNoise);
				if(valueNoise < minNoise + (OPN * param.getPourcentFreezeTemp())){

					Lchunk.add(1);
					if(cases.getBiome() instanceof  Fantasy){
						cases.setBiome(freezeFantasy);
					}else if(cases.getBiome() instanceof  Montain){
						cases.setBiome(freezeMontain);
					}else if(cases.getBiome() instanceof  Oceant){
						cases.setBiome(freezeOceant);
					}else if(cases.getBiome() instanceof  Plain){
						cases.setBiome(freezePlain);
					}else{
						System.out.println("this biome doesn't exist");
					}

				}else if(valueNoise < minNoise + (OPN * param.getPourcentFreezeTemp()) + (OPN * param.getPourcentColdTemp())){
					Lchunk.add(2);
					if(cases.getBiome() instanceof  Fantasy){
						cases.setBiome(coldFantasy);
					}else if(cases.getBiome() instanceof  Montain){
						cases.setBiome(coldMontain);
					}else if(cases.getBiome() instanceof  Oceant){
						cases.setBiome(coldOceant);
					}else if(cases.getBiome() instanceof  Plain){
						cases.setBiome(coldPlain);
					}else{
						System.out.println("this biome doesn't exist");
					}

				}else if(valueNoise < minNoise + (OPN * param.getPourcentFreezeTemp()) + (OPN * param.getPourcentColdTemp()) + (OPN * param.getPourcentNormalTemp())){
					Lchunk.add(3);
					if(cases.getBiome() instanceof  Fantasy){
						cases.setBiome(fantasy);
					}else if(cases.getBiome() instanceof  Montain){
						cases.setBiome(montain);
					}else if(cases.getBiome() instanceof  Oceant){
						cases.setBiome(oceant);
					}else if(cases.getBiome() instanceof  Plain){
						cases.setBiome(plain);
					}else{
						System.out.println("this biome doesn't exist");
					}

				}else if(valueNoise < minNoise + ((float)OPN * param.getPourcentFreezeTemp()) + ((float)OPN * param.getPourcentColdTemp()) + ((float)OPN * param.getPourcentNormalTemp()) + ((float)OPN * param.getPourcentHotTemp())){
					Lchunk.add(4);
					if(cases.getBiome() instanceof  Fantasy){
						cases.setBiome(hotFantasy);
					}else if(cases.getBiome() instanceof  Montain){
						cases.setBiome(hotMontain);
					}else if(cases.getBiome() instanceof  Oceant){
						cases.setBiome(hotOceant);
					}else if(cases.getBiome() instanceof  Plain){
						cases.setBiome(hotPlain);
					}else{
						System.out.println("this biome doesn't exist");
					}

				}else if(valueNoise < minNoise + ((float)OPN * param.getPourcentFreezeTemp()) + ((float)OPN * param.getPourcentColdTemp()) + ((float)OPN * param.getPourcentNormalTemp()) + ((float)OPN * param.getPourcentHotTemp())+ ((float)OPN * param.getPourcentDesertTemp()+1)){
					Lchunk.add(5);
					if(cases.getBiome() instanceof  Fantasy){
						cases.setBiome(desertFantasy);
					}else if(cases.getBiome() instanceof  Montain){
						cases.setBiome(desertMontain);
					}else if(cases.getBiome() instanceof  Oceant){
						cases.setBiome(desertOceant);
					}else if(cases.getBiome() instanceof  Plain){
						cases.setBiome(desertPlain);
					}else{
						System.out.println("this biome doesn't exist");
					}
				}else{
					System.out.println( "----temp----");
					System.out.println(valueNoise + " -> value");
					cases.setBiome(null);
				}
				if(i%4==0){
					i = iChunk+((4*param.getNBColonne())*iLCase);
					iLCase++;
				}else{
					i++;
				}
			}

			ligne.add(Lchunk);

			if((iChunk+3)%(param.getNBColonne()*4) == 0){
				iChunk = (iChunk+3)+((param.getNBColonne()*4)*3)+1;
				map.add(ligne);
				ligne = new ArrayList<>();
			}else{
				iChunk+=4;
			}
		}

		//System.out.println(map);
		System.out.println("Temperature was load");
	}

	public void fillRelief(){
		//System.out.println("relief was load");
	}

	public void fillVegetation(){
		//System.out.println("Vegetation was load");
	}

	public void fillStructure(){
		//System.out.println("Structure was load");
	}

	public void displayMap(){
		/*
		* display map
		*/
		for(Chunk chunk : chunkInMap) {
			chunk.displayChunk();;
		}
	}


	public int getSeed() {
		return param.getseed();
	}
}
