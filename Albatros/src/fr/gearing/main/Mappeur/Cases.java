package fr.gearing.main.Mappeur;


import fr.gearing.main.Mappeur.Biome.Biomes;
import fr.gearing.main.Structure.Block.Loader;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Cases {
	
	//is the number to the chunk where is the case 
	private int Nchunk;
	/*
	 * P
	 *  |1 |2 |3 |4
	 *  |5 |6 |7 |8
	 *  |9 |10|11|12
	 *  |13|14|15|16
	 */
	private int P;
	//if there are a build when build > 0 else not building == -1 or none build == 0
	private int build;
	//name of the biome to the case
	private Biomes Biome;
	//name of the old biome of this case
	private Biomes oldIdBiome;

	private Texture texture;

	
	private float X;
	private float Y;
	
	private float Z0;
	private float Z1;
	private float Z2;
	private float Z3;
	private float Z4;
	private float Z5;

	//Constructeur par défaut
	public Cases(int nchunk, int p, Biomes biome, float x, float y, float z){
		/*
		 * Initialization to the case
		 */
		float t = save.tchunk;
		
		this.Nchunk = nchunk;
		this.P = p;
		this.build = 0;
		this.Biome = biome;
		this.oldIdBiome = biome;
		loadTexture();

		
		if(p%4 == 1){
			this.X = x - t/2 - t;
		}else if(p%4 == 2){
			this.X = x - t/2;
		}
		else if(p%4 == 3){
			this.X = x + t/2;
		}
		else if(p%4 == 0){
			this.X = x + t/2 + t;	
		}
		
		if(p <= 4){
			this.Y = y - t/2 - t;
		}else if(p <= 8){
			this.Y = y - t/2;
		}else if(p <= 12){
			this.Y = y + t/2;
		}else if(p <= 16){
			this.Y = y + t/2 + t;
		}
		
		this.Z0 = z;
		this.Z1 = z;
		this.Z2 = z;
		this.Z3 = z;
		this.Z4 = z;
		this.Z5 = z;
	} 
	
	public boolean getCasesBuild(){
		/*
		 * return if it is possible to build in this cases
		 */
		return build <= 0 && build != -1;
	}
	public int position() {
		return this.P;
	}
	public void resetBiome() {
		this.Biome = this.oldIdBiome;
	}
	public void setBiome(Biomes biome) {
		this.oldIdBiome = this.Biome;
		this.Biome = biome;
		loadTexture();
	}
	
	public Biomes getBiome(){
		/*
		 * return the id of the biome of the carte
		 */
		return Biome;
	}

	public int getTemperature(){
		/*
		 * return the id of the temperature of the carte
		 */
		return Biome.getTemperature();
	}
	public void setTemperature(int temp){
		/*
		 * return the id of the temperature of the carte
		 */
		loadTexture();
		Biome.setTemperature(temp);
	}

	public void setZ0(int z){
		this.Z0 = z;
	}
	public void setZ1(int z){
		this.Z1 = z;
	}
	public void setZ2(int z){
		this.Z2 = z;
	}
	public void setZ3(int z){
		this.Z3 = z;
	}
	public void setZ4(int z){
		this.Z4 = z;
	}
	public void setZ5(int z){
		this.Z5 = z;
	}
	
	public void add_build(int build){
		this.build = build;
	}
	
	public void displayCases() {
		
		float t = save.tchunk/2;

		float x0 = X - t;
		float x1 = X + t;
			
		float y0 = Y - t;
		float y1 = Y;
		float y2 = Y + t;
		
		float P0y = y0;
		float P0x = x0;
		float P0z = Z0;
			
		float P1y = y1;
		float P1x = x0;
		float P1z = Z1;
			
		float P2y = y1;
		float P2x = x1;
		float P2z = Z2;
			
		float P3y = y2;
		float P3x = x1;
		float P3z = Z3;
			
		float P4y = y2;
		float P4x = x0;
		float P4z = Z4;
			
		float P5y = y0;
		float P5x = x1;
		float P5z = Z5;
			
		//----------------------------------------------------------------------------------
		texture.bind();
		//triangle P5 P2 P0
		Loader.set_case(P5x, P2x, P0x, P5y, P2y, P0y, P5z, P2z, P0z);
			
		//triangle P0 P1 P2
		Loader.set_case(P0x, P1x, P2x, P0y, P1y, P2y, P0z, P1z, P2z);
			
		//triangle P1 P4 P2
		Loader.set_case(P1x, P4x, P2x, P1y, P4y, P2y, P1z, P4z, P2z);
			
		//triangle P2 P3 P4
		Loader.set_case(P2x, P3x, P4x, P2y, P3y, P4y, P2z, P3z, P4z);
			
			
		}

	public int getNchunk() {
		return Nchunk;
	}

	private void loadTexture(){
		if (Biome == null) {
			texture = Texture.bug;
		}else {
			try {
				//if the game fps is low stock the texture in the biome
				texture = Texture.biomeTexture.get(Biome.getId()-1).get(Biome.getTemperature()-1);
				/*
				if(Biome.getTemperature()==1){
					texture = Texture.T1;
				}else if(Biome.getTemperature()==2){
					texture = Texture.T2;
				}else if(Biome.getTemperature()==3){
					texture = Texture.T3;
				}else if(Biome.getTemperature()==4){
					texture = Texture.T4;
				}else if(Biome.getTemperature()==5){
					texture = Texture.T5;
				}else{
					System.out.println("nullTemperature");
				}
				 */

			} catch (NullPointerException e) {
				System.out.println(Biome);
				texture = Texture.bug;
			}
		}
	}
}
