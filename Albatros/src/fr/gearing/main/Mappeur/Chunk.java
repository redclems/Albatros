package fr.gearing.main.Mappeur;

import java.util.ArrayList;
import java.util.List;

public class Chunk {
	
	//is the number of the chunk
	private int Nchunk;	
	//temperature of the chunk
	private int Temp;
	//biome moyen to the chunk
	private String Biome;
	//list Cases
	private List<Cases> casesInChunk = new ArrayList<>();
	
		     
	//Constructeur par défaut
	public Chunk(int nchunk, int temp, float x, float y, float z){
  
		this.Nchunk = nchunk;
		this.Temp = temp;
		this.Biome = null;
		for(int i = 1; i <= 16; i++) {
			//Create case for the chunk
			Cases cases = new Cases(nchunk, i, null, x, y, z);
			casesInChunk.add(cases);
		}
	}
	public void displayChunk(){
		  /*
		   * display chunk
		   */
		for(Cases cases : casesInChunk) {
			cases.displayCases();
		}
	}
	
	
	public String getBiome(){
		/*
		 * return the moyene of the biome in the chunk
		 */
		return Biome;
	}
	public int getTemp(){
		/*
		 * return the temperature of the chunk
		 */
		return Temp;
	}
	public int getNchunk(){
		/*
		 * return the number of the chunk
		 */
		return Nchunk;
	}
	public void addCases(Cases cases){
		/*
		 * add case to the list of cases
		 */
		casesInChunk.add(cases);
	}
	public void setCases(int i, Cases cases){
		/*
		 * set case to the list of cases
		 */
		casesInChunk.set(i, cases);
	}
	
	public Cases getcases(int position){
		/*
		 *  1, 2, 3, 4
		 *  5, 6, 7, 8
		 *  9,10,11,12
		 * 13,14,15,16
		 * get case to the list of cases
		 */
		return casesInChunk.get(position);
	}
	public List<Cases> getcases(){
		/*
		 *  1, 2, 3, 4
		 *  5, 6, 7, 8
		 *  9,10,11,12
		 * 13,14,15,16
		 * get case to the list of cases
		 */
		return casesInChunk;
	}
	public int getNBCases() {
		/*
		 * return the number of case in chunk normal there are 16 case
		 */
		return casesInChunk.size();
	}
	
	
}
