package fr.gearing.main.Mappeur.Biome;

public class Fantasy extends Biomes{
    public Fantasy(int chance) {
        super(chance);
        this.setId(1);
    }
    public Fantasy(int chance, int temp) {
        super(chance, temp);
        this.setId(1);
    }
}
