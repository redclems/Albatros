package fr.gearing.main.Mappeur.Biome;

public class Oceant extends Biomes{
    public Oceant(int chance) {
        super(chance);
        this.setId(3);
    }

    public Oceant(int chance, int temp) {
        super(chance, temp);
        this.setId(3);
    }
}
