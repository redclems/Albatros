package fr.gearing.main.Mappeur.Biome;

public class Montain extends Biomes{
    public Montain(int chance) {
        super(chance);
        this.setId(2);
    }
    public Montain(int chance, int temp) {
        super(chance, temp);
        this.setId(2);
    }
}
