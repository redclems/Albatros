package fr.gearing.main.Mappeur.Biome;

public class Plain extends Biomes{
    public Plain(int chance) {
        super(chance);
        this.setId(4);
    }

    public Plain(int chance, int temp) {
        super(chance, temp);
        this.setId(4);
    }
}
