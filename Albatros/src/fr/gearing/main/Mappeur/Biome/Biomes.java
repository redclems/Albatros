package fr.gearing.main.Mappeur.Biome;

public abstract class Biomes {

    private int id; // id
    private int chance; // chance to spawn in the map
    private int temperature;

    Biomes(int chance) {
        this.chance = chance;
    }
    Biomes(int chance, int temp) {
        this.chance = chance;
        this.temperature = temp;
    }
    void setId(int id) {
        this.id = id;
    }
    public void setTemperature(int temp){
        this.temperature = temp;
    }

    public int getTemperature(){ return temperature; }

    public int getId() { return id; }

    public double getChance() { return chance; }

    public void setChance(int prob) {
        this.chance = prob;
    }

    @Override
    public String toString(){
        if(id < 1) return "none Type Biome negative value";
        else if(id == 1) return "Fantasy";
        else if(id == 2) return "Montain";
        else if(id == 3) return "Oceant";
        else if(id == 4) return "Plain";
        return "none Type Biome";
    }

    private String temp(){
        if(temperature < 1) return "none ";
        else if(temperature == 1) return "freeze ";
        else if(temperature == 2) return "cold ";
        else if(temperature == 3) return "";
        else if(temperature == 4) return "hot ";
        else if(temperature == 4) return "tropical ";
        return "none ";
    }



}
