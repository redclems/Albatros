package fr.gearing.main.Mappeur.PerlinNoise;

import java.util.ArrayList;
import java.util.List;


public class PerlinCenter {
	
	private static double max ;
	private static double min ;
	
	public static List<Double> noise(int seed, int ligmax, int colmax) {
									//sorry I write in French here
									//la frequence c'est pour le lissage de la carte permet en l'augmentant d'avoir plus d'ilot
									//l'amplitude c'est pour le plus bas et le plus haut 
									//seed, persistence, frequency, amplitude, 
									//octaves plus ou moin la note central
									//seed, persistence, frequency, amplitude, octaves
		double valmax = 0;
		double valmin = 0;
		PerlinNoise p = new PerlinNoise(seed, 50, 5, 5, 1);
		List<Double> noise = new ArrayList<>();
		float y=0;

		for(int  lig = 0; lig < ligmax; lig++) {
			float x =0;
			y += .01;
			for(int col = 0; col < colmax; col++){
				x += .01;
				Double val = p.getHeight(x, y);
				noise.add(val);
				if(val < valmin)valmin = val;
				if(val > valmax)valmax = val;
			}
		}
		//System.out.println(noise.size()+" -> size");
		min = valmin;
		max = valmax;
		return noise;
	}
	public static List<Double> noise(int seed, int ligMax, int colMax, int persistence,int frequence, int amplitude, int octave) {
		//sorry I write in French here
		//la frequence c'est pour le lissage de la carte
		//l'amplitude c'est pour le plus bas et le plus haut 
		//seed, persistence, frequency, amplitude, 
		//octaves plus ou moin la note central
		//seed, persistence, frequency, amplitude, octaves
		double valmax = 0;
		double valmin = 0;
		PerlinNoise p = new PerlinNoise(seed, persistence, frequence, amplitude, octave);
		List<Double> noise = new ArrayList<>();
		float y=0;

		for(int  lig = 0; lig < ligMax; lig++) {
			float x =0;
			y += .01;
			for(int col = 0; col < colMax; col++){
				x += .01;
				Double val = p.getHeight(x, y);
				noise.add(val);
				if(val < valmin)valmin = val;
				if(val > valmax)valmax = val;
			}
		}
		//System.out.println(noise.size()+" -> size");
		min = valmin;
		max = valmax;
		return noise;
	}
	public static double getMax() {
		return max;
	}
	public static double getMin() {
		return min;
	}


}
