package fr.gearing.main.Mappeur;

public class ParamMap {

    private int NBColonne ;
    private int NBLigne ;
    private int seed;

    private int persistence ;
    private int frequence ;
    private int amplitude;
    private int octave;

    private int pourcentLand;//it is the pourcent of the land have on the map
    private int pourcentMontagne;//it is the pourcent of the montagne have on the land
    private int pourcentRare;//it is the pourcent of the rare biome have on the land


    private int pourcExtremTemp;//if value == 100 the extrem temperature is majoritaire
    private int pourcHot;//if value == 100 the hot temperature is favorised
    private int pourcCold;//if value == 100 the cold temperature is favorised

    public ParamMap(int NBColonne, int NBLigne, int seed) {
        this.NBColonne = NBColonne;
        this.NBLigne = NBLigne;
        this.seed = seed;

        constante();
    }

    public ParamMap(int nbLigne, int nbColonne) {
        this.NBColonne = nbColonne;
        this.NBLigne = nbLigne;
        this.seed = randomeSeed();

        constante();
    }

    private void constante(){
        this.persistence = 50;
        this.frequence = 5;
        this.amplitude = 5;
        this.octave = 1;

        //for the biome
        this.pourcentLand = 55;//normal 55 || if increase there are more land
        this.pourcentMontagne = 42;//normal 42
        this.pourcentRare = 15;///normal 15

        //for the temp
        this.pourcExtremTemp = 50;
        this.pourcHot = 20;
        this.pourcCold = 100-pourcHot;

    }

    private int randomeSeed(){
        //random seed here
        int min = 0;
        int max = 99999999;
        return (int)((Math.random()*((max-min)+1))+min);
    }

    public int getNBColonne(){return this.NBColonne;}
    public int getNBLigne(){return this.NBLigne;}
    public int getseed(){return this.seed;}

    public void setseed(int seed){this.seed = seed;}
    public void addSeed(){this.seed++;}

    public int getpersistence(){return this.persistence;}
    public int getfrequence(){return this.frequence;}
    public int getamplitude(){return this.amplitude;}
    public int getoctave(){return this.octave;}


    public int getPourcentOceant(){return 100-this.pourcentLand;}
    public int getPourcentPlain(){return (int) (((float) this.pourcentLand/100)*(100-(pourcentMontagne + pourcentRare)) + 1);}
    public int getPourcentMontagne(){return (int) (((float)this.pourcentLand/100) * this.pourcentMontagne);}
    public int getPourcentRare(){return (int) (((float) this.pourcentLand/100) * this.pourcentRare);}

    public double getPourcentFreezeTemp(){
        return (((float)this.pourcExtremTemp/100)*pourcCold);
    }

    public double getPourcentColdTemp(){
        float mediumTemp = ((float)(100-this.pourcExtremTemp)/100)*60;
        return ((float)mediumTemp/100)*pourcCold;
    }
    public double getPourcentNormalTemp(){
        return 100 - (getPourcentFreezeTemp()+getPourcentColdTemp()+getPourcentHotTemp()+getPourcentDesertTemp());
    }

    public double getPourcentHotTemp(){
        float mediumTemp = ((float)(100-this.pourcExtremTemp)/100)*60;
        return ((float)mediumTemp/100)*pourcHot;
    }

    public double getPourcentDesertTemp(){
        return (((float)this.pourcExtremTemp/100)*pourcHot);
    }

    public int getPourcentFreezeTemp(int idBiome){
        if(idBiome == 1)return (int) (((float) getPourcentRare()/100) * getPourcentFreezeTemp());
        else if(idBiome == 2)return (int) (((float) getPourcentMontagne()/100) * getPourcentFreezeTemp());
        else if(idBiome == 3)return (int) (((float) getPourcentOceant()/100) * getPourcentFreezeTemp());
        else if(idBiome == 4)return (int) (((float) getPourcentPlain()/100) * getPourcentFreezeTemp());
        else return 0;
    }

    public int getPourcentColdTemp(int idBiome){
        if(idBiome == 1)return (int) (((float) getPourcentRare()/100) * getPourcentColdTemp());
        else if(idBiome == 2)return (int) (((float) getPourcentMontagne()/100) * getPourcentColdTemp());
        else if(idBiome == 3)return (int) (((float) getPourcentOceant()/100) * getPourcentColdTemp());
        else if(idBiome == 4)return (int) (((float) getPourcentPlain()/100) * getPourcentColdTemp());
        else return 0;
    }

    public int getPourcentNormalTemp(int idBiome){

        if(idBiome == 1)return (int) (((float) getPourcentRare()/100) * getPourcentNormalTemp());
        else if(idBiome == 2)return (int) (((float) getPourcentMontagne()/100) * getPourcentNormalTemp());
        else if(idBiome == 3)return (int) (((float) getPourcentOceant()/100) * getPourcentNormalTemp());
        else if(idBiome == 4)return (int) (((float) getPourcentPlain()/100) * getPourcentNormalTemp());
        else return 0;
    }

    public int getPourcentHotTemp(int idBiome){
        float mediumTemp = ((float)(100-this.pourcExtremTemp)/100)*60;
        if(idBiome == 1)return (int) (((float) getPourcentRare()/100) * getPourcentHotTemp());
        else if(idBiome == 2)return (int) (((float) getPourcentMontagne()/100) * getPourcentHotTemp());
        else if(idBiome == 3)return (int) (((float) getPourcentOceant()/100) * getPourcentHotTemp());
        else if(idBiome == 4)return (int) (((float) getPourcentPlain()/100) * getPourcentHotTemp());
        else return 0;
    }

    public int getPourcentDesertTemp(int idBiome){
        if(idBiome == 1)return (int) (((float) getPourcentRare()/100) * getPourcentDesertTemp());
        else if(idBiome == 2)return (int) (((float) getPourcentMontagne()/100) * getPourcentDesertTemp());
        else if(idBiome == 3)return (int) (((float) getPourcentOceant()/100) * getPourcentDesertTemp());
        else if(idBiome == 4)return (int) (((float) getPourcentPlain()/100) * getPourcentDesertTemp());
        else return 0;
    }


}
