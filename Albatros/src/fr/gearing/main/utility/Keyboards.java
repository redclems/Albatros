package fr.gearing.main.utility;

import org.lwjgl.input.Keyboard;

import fr.gearing.main.game.save;

public class Keyboards {
	//key listen in the game
	static int n = 48;// number of key is listen
	public static boolean Key[] = new boolean [n];//list of key 
	public static int Keybind[] = new int [n];//list key is bind
	static int valbind = 5;//is values for the key is bind
	
	public static boolean KeyA = false;//1
	public static boolean KeyB = false;//2
	public static boolean KeyC = false;//3
	public static boolean KeyD = false;//4
	public static boolean KeyE = false;//5
	public static boolean KeyF = false;//6
	public static boolean KeyG = false;//7
	public static boolean KeyH = false;//8
	public static boolean KeyI = false;//9
	public static boolean KeyJ = false;//10
	public static boolean KeyK = false;//11
	public static boolean KeyL = false;//12
	public static boolean KeyM = false;//13
	public static boolean KeyN = false;//14
	public static boolean KeyO = false;//15
	public static boolean KeyP = false;//16
	public static boolean KeyQ = false;//17
	public static boolean KeyR = false;//18
	public static boolean KeyS = false;//19
	public static boolean KeyT = false;//20
	public static boolean KeyU = false;//21
	public static boolean KeyV = false;//22
	public static boolean KeyW = false;//23
	public static boolean KeyX = false;//24
	public static boolean KeyY = false;//25
	public static boolean KeyZ = false;//26
	public static boolean KeyESC = false;//27
	public static boolean KeySPACE = false;//28
	public static boolean KeyUP = false;//29
	public static boolean Keydelete = false;//30
	public static boolean KeyDOWN = false;//31
	public static boolean KeyLEFT = false;//32
	public static boolean KeyRIGHT = false;//33
	public static boolean KeyENTREE = false;//34
	public static boolean KeySHIFT = false;//35
	public static boolean KeyFN = false;//36
	public static boolean Key1 = false;//37
	public static boolean Key2 = false;//38
	public static boolean Key3 = false;//39
	public static boolean Key4 = false;//40
	public static boolean Key5 = false;//41
	public static boolean Key6 = false;//42
	public static boolean Key7 = false;//43
	public static boolean Key8 = false;//44
	public static boolean Key9 = false;//45
	public static boolean Key0 = false;//46
	public static boolean KeyMazerty = false;//47

	
	//action in the game
	public static boolean forward = false;
	public static boolean backward = false;
	public static boolean right = false;
	public static boolean left = false;
	public static boolean sprint = false;
	public static boolean up = false;
	public static boolean down = false;
	public static boolean rotation = false;
	public static boolean B612 = false;
	public static boolean kickMenu = false;//for kick seting or other
	public static boolean MousseP = false;//sea a position of the mouse in the display
	
	public static void runKEY() {
		for(int i = 0; i < Key.length; i++){
			Key[i] = false;
		}
	}
	public static void KEYaffichage() {
		for(int i = 0; i < Key.length; i++){
			if(Key[i] == true) {
				System.out.println(i + "- key");
			}
		}
	}
	
	public static String decoder_id(int id) {
		/*
		 * decode the id but it is in qwerty
		 */
		String key = "";
		if(id == 1) {
			key = "A";
		}else if(id == 2) {
			key = "B";
		}else if(id == 3) {
			key = "C";
		}else if(id == 4) {
			key = "D";
		}else if(id == 5) {
			key = "E";
		}else if(id == 6) {
			key = "F";
		}else if(id == 7) {
			key = "G";
		}else if(id == 8) {
			key = "H";
		}else if(id == 9) {
			key = "I";
		}else if(id == 10) {
			key = "J";
		}else if(id == 11) {
			key = "K";
		}else if(id == 12) {
			key = "L";
		}else if(id == 13) {
			key = "M";
		}else if(id == 14) {
			key = "N";
		}else if(id == 15) {
			key = "O";
		}else if(id == 16) {
			key = "P";
		}else if(id == 17) {
			key = "Q";
		}else if(id == 18) {
			key = "R";
		}else if(id == 19) {
			key = "S";
		}else if(id == 20) {
			key = "T";
		}else if(id == 21) {
			key = "U";
		}else if(id == 22) {
			key = "V";
		}else if(id == 23) {
			key = "W";
		}else if(id == 24) {
			key = "X";
		}else if(id == 25) {
			key = "Y";
		}else if(id == 26) {
			key = "Z";
		}else if(id == 27) {
			key = "ESC";
		}else if(id == 28) {
			key = "SPACE";
		}else if(id == 29) {
			key = "UP";
		}else if(id == 30) {
			key = "DEL";
		}else if(id == 31) {
			key = "DOWN";
		}else if(id == 32) {
			key = "LEFT";
		}else if(id == 33) {
			key = "RIGHT";
		}else if(id == 34) {
			key = "ENTER";
		}else if(id == 35) {
			key = "SHIFT";
		}else if(id == 36) {
			key = "FN";
		}else if(id == 37) {
			key = "1";
		}else if(id == 38) {
			key = "2";
		}else if(id == 39) {
			key = "3";
		}else if(id == 40) {
			key = "4";
		}else if(id == 41) {
			key = "5";
		}else if(id == 42) {
			key = "6";
		}else if(id == 43) {
			key = "7";
		}else if(id == 44) {
			key = "8";
		}else if(id == 45) {
			key = "9";
		}else if(id == 46) {
			key = "0";
		}


		return key;
		
	}
	public static void KEY() {
		int i=0;
		//KEYaffichage();
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {//1
			Key[i]= true;
			KeyA = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyA = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_B)) {//2
			Key[i] = true;
			KeyB = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyB = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_C)) {//3
			Key[i] = true;
			KeyC = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyC = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_D)) {//4
			Key[i] = true;
			KeyD = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyD=false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_E)) {//5
			Key[i] = true;
			KeyE = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyE = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_F)) {//6
			Key[i] = true;
			KeyF = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyF= false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_G)) {//7
			Key[i] = true;
			KeyG=(boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyG=false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_H)) {//8
			Key[i] = true;
			KeyH =(boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyH = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_I)) {//9
			Key[i] = true;
			KeyI = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyI = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_J)) {//10
			Key[i] = true;
			KeyJ = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyJ = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_K)) {//11
			Key[i] = true;
			KeyK = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyK = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_L)) {//12
			Key[i] = true;
			KeyL = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyL = false;
			Keybind[i] = 0;
			
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_M)) {//13
			Key[i] = true;
			KeyM = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyM = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_N)) {//14
			Key[i] = true;
			KeyN = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyN = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_O)) {//15
			Key[i] = true;
			KeyO = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyO=false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_P)) {//16
			Key[i] = true;
			KeyP= (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyP = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {//17
			Key[i] = true;
			KeyQ = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyQ = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_R)) {//18
			Key[i] = true;
			KeyR = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyR = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_S)) {//19
			Key[i] = true;
			KeyS = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyS = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_T)) {//20
			Key[i] = true;
			KeyT = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyT = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_U)) {//21
			Key[i] = true;
			KeyU = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyU = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_V)) {//22
			Key[i] = true;
			KeyV = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyV = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {//23
			Key[i] = true;
			KeyW = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyW = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_X)) {//24
			Key[i] = true;
			KeyX = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyX = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_Y)) {//25
			Key[i] = true;
			KeyY = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyY = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_Z)) {//26
			Key[i] = true;
			KeyZ = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			KeyZ = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {//27
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {//28
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {//29
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_BACK)) {//30
			Key[i] = true;
			Keydelete = true;
			
		}else if(Key[i] == true){
			Key[i] = false;
			Keydelete = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {//31
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {//32
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {//33
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_SLASH)) {//34//i don't know what is the key enter whith keyboards
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_CAPITAL)) {//35//i don't know what is the key enter whith keyboards
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_TAB)) {//36//i don't know what is the key enter whith keyboards
			Key[i] = true;
		}else if(Key[i] == true){
			Key[i] = false;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_1)) {//37
			Key[i] = true;
			Key1 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key1 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_2)) {//38
			Key[i] = true;
			Key2 =(boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key2=false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_3)) {//39
			Key[i] = true;
			Key3 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key3 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_4)) {//40
			Key[i] = true;
			Key4 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key4 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_5)) {//41
			Key[i] = true;
			Key5 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i]= false;
			Key5 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_6)) {//42
			Key[i] = true;
			Key6 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key6 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_7)) {//43
			Key[i] = true;
			Key7 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key7 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_8)) {//44
			Key[i] = true;
			Key8 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key8 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_9)) {//45
			Key[i] = true;
			Key9 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;
			Key9 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_0)) {//46
			Key[i] = true;
			Key0 = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;	
			Key0 = false;
			Keybind[i] = 0;
		}
		i++;
		if (Keyboard.isKeyDown(Keyboard.KEY_SEMICOLON)) {//47
			Key[i] = true;
			KeyMazerty = (boolean) Keybind(i);
		}else if(Key[i] == true){
			Key[i] = false;	
			KeyMazerty = false;
			Keybind[i] = 0;
		}
	}
	private static Object Keybind(int i) {
		Keybind[i] = Keybind[i]+1;
		boolean key = false;
		if(Keybind[i] <= 2 || Keybind[i] >= valbind) {
			key = true;
		}
				
		return key;
		
	}
	public static int key_indices() {
		int i = 0; //code to the key 
		for(int u = 0; u < Key.length; u++) {
			if(Key[u] == true) {
				i = u;
			}
		}
				
		return i;
	}
	
	public static void keyforaction() {
		int i = 0;
		i = save.forward; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			forward = true;
		}else {
			forward = false;
		}
		
		i = save.backward; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			
			backward = true;
		}else {
			backward = false;
		}
		
		i = save.right; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			
			right = true;
		}else {
			right = false;
		}
		
		i = save.left; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			
			left = true;
		}else {
			left = false;
		}
		
		
		i = save.sprint; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			sprint = true;
		}else {
			sprint = false;
		}
		
		i = save.up; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			up = true;
		}else {
			up = false;
		}
		
		i = save.down; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			down = true;
		}else {
			down = false;
		}
		
		i = save.rotation; //i is the val of touch for have action ...
		if(Key[i] == true){ 
			rotation = (boolean) Keybind(i);
			
		}else {
			rotation = false;
			Keybind[i] = 0;
		}
		
		if(Key[2] == true && Key[42] == true && Key[37] == true && Key[38] == true){ 
			B612 = true;
			
		}else {
			B612 = false;
		}
		
		i = save.kick;
		if(Key[i] == true ){ 
			kickMenu = true;
		}else {
			kickMenu = false;
		}
		
		if(Key[47] == true ){ 
			MousseP = true;
		}else {
			MousseP = false;
		}
		
	}
	
}
