package fr.gearing.main.utility;



import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.game.save;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;

public class Mousse {

	public static float mx;
	public static float my;
	public static float mx_back;
	public static float my_back;
	public static float speed_mouv;
	
	public static boolean event_click = false;
	public static boolean mousse_etat = false;

	
	public static void MoussePosition() {
		/*
		 * this function is here for have mouse in the display, it calcul the position of the mouse
		 * 
		 */
		//Render.mouvYOU = false; if you using this function mouvYou duty equal false
		float mouse_z = -1f; //z of the mouse
	    int width = Display.getWidth();
	    float syze = 0.23f/save.zoom;
	    
	    float pixel_x = GameUtility.pixel_X(mouse_z);
	    float pixel_y = GameUtility.pixel_Y(mouse_z);
	    	
		    mx = Mouse.getX()*pixel_x - (width/2)*pixel_x;
		    my = Mouse.getY()*pixel_y;
		    MousseAffichage();
		    
		    Texture.curs.bind();
		    LoaderMenus.setCurseur(syze, mx, my, mouse_z);	    
	}
	
	public static void MousseAffichage() {//affichage in the terminal
		/*
		 * is for see where is the mouse
		 */
	    if(Keyboards.MousseP == true) {
	    	System.out.print("mx_p :");
	    	System.out.println(Mouse.getX());
	    	System.out.print("my_p :");
	    	System.out.println(Mouse.getY());
	    	
	    	System.out.print("mx :");
	    	System.out.println(mx);
	    	System.out.print("my :");
	    	System.out.println(my);
	    }
		
	}
	public static void mousseClick() {
		/*
		 * this function verified if mouse is hover the object
		 */
		mousse_etat = false;
		if(Mouse.isButtonDown(0) == false){
			event_click = false;
		}
		
		if(event_click == false && Mouse.isButtonDown(0) == true) {
			mousse_etat = true;
			event_click = true;
		}
	}
	
	public static boolean MousseHover(float Width, float Height, float X, float Y, float Z) {
		/*
		 * this function verified if mouse is hover the object
		 */
		boolean hover = false;
	    int width = Display.getWidth();
	    
	    float pixel_x = GameUtility.pixel_X(Z);
	    float pixel_y = GameUtility.pixel_Y(Z);
	    	

		float verif_mx = Mouse.getX()*pixel_x - (width/2)*pixel_x;
		float verif_my = Mouse.getY()*pixel_y;
		
		if(verif_mx > X-Width/2 && verif_mx < X+Width/2 && verif_my > Y-Height/2 && verif_my < Y+Height/2) {
			hover = true;
		}
		
		return hover;
	}
	
	public static float Moussefollow_y(float Z) {
		/*
		 * this function verified if mouse is hover the object
		 */

	    float pixel_y = GameUtility.pixel_Y(Z);
		float verif_my = Mouse.getY()*pixel_y;
		
		
		return verif_my;
	}
	public static float Moussefollow_x(float Z) {
		/*
		 * this function verified if mouse is hover the object
		 */

	    int width = Display.getWidth();
	    
	    float pixel_x = GameUtility.pixel_X(Z);
		float verif_mx = Mouse.getX()*pixel_x - (width/2)*pixel_x;
		
		
		return verif_mx;
	}
}

	
