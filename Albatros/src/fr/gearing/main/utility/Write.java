package fr.gearing.main.utility;


import fr.gearing.main.game.GameUtility;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;

public class Write {
	
	public static float write(String word ,int color ,float Font_size, float X, float Y, float Z, boolean center) {
		/*
		 * write the word with in the parameter and  the color white is 1 and color black is 2
		 * 
		 */
		
		return display(word , color , Font_size, X, Y, Z, center);
	}
	public static float write(String word ,int color ,int Font_size, int X, int Y, float Z, boolean center) {
		/*
		 * write the word with in the parameter and  the color white is 1 and color black is 2
		 *  
		 */
		float x = GameUtility.change_percentage_by_Co_X(X, Z);
		float y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		float font_size = GameUtility.change_percentage_by_height(Font_size, Z);
		
		return display(word , color , font_size, x, y, Z, center);
	}
	private static float display(String word ,int color ,float font_size, float x, float y, float Z, boolean center) {
		char letter[]  = word.toCharArray();//letter 
		
		int tl = letter.length;
		char let;
		int i = 0;
		float width = font_size * tl;
		float lentgth_word = 0.0f;
		
		if(center == true) {
			x = x - width/2;
		}
		
		while(i < tl) {
			
			let = letter[i];
			x = x + font_size * 0.75f;
			
			if(let == 'a') {
				String key = "a";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'b') {
				String key = "b";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'c') {
				String key = "c";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'd') {
				String key = "d";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else	
			if(let == 'e') {
				String key = "e";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else	
			if(let == 'f') {
				String key = "f";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'g') {
				String key = "g";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'h') {
				String key = "h";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'i') {
				String key = "i";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else	
			if(let == 'j') {
				String key = "j";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else		
			if(let == 'k') {
				String key = "k";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'l') {
				String key = "l";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'm') {
				String key = "m";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else
			if(let == 'n') {
				String key = "n";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else	
			if(let == 'o') {
				String key = "o";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else			
			if(let == 'p') {
				String key = "p";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'q') {
				String key = "q";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else
			if(let == 'r') {
				String key = "r";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 's') {
				String key = "s";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else	
			if(let == 't') {
				String key = "t";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else			
			if(let == 'u') {
				String key = "u";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'v') {
				String key = "v";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else
			if(let == 'w') {	
				String key = "w";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'x') {
				String key = "x";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'y') {	
				String key = "y";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'z') {
				String key = "z";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else	
			if(let == 'A') {
				String key = "a";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'B') {
				String key = "b";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'C') {
				String key = "c";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'D') {
				String key = "d";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else	
			if(let == 'E') {
				String key = "e";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else	
			if(let == 'F') {
				String key = "f";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let== 'G') {
				String key = "g";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'H') {
				String key = "h";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else
			if(let == 'I') {
				String key = "i";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else	
			if(let == 'J') {
				String key = "j";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else		
			if(let == 'K') {
				String key = "k";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'L') {
				String key = "l";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'M') {
				String key = "m";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else
			if(let == 'N') {
				String key = "n";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else	
			if(let == 'O') {
				String key = "o";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else			
			if(let == 'P') {
				String key = "p";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'Q') {
				String key = "q";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else
			if(let == 'R') {
				String key = "r";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'S') {
				String key = "s";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else	
			if(let == 'T') {
				String key = "t";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else			
			if(let == 'U') {
				String key = "u";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let== 'V') {
				String key = "v";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}		
			}else
			if(let == 'W') {
				String key = "w";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'X') {
				String key = "x";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'Y') {	
				String key = "y";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == 'Z') {
				String key = "z";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else	
			if(let == '0') {
				String key = "0";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == 'O') {
				String key = "o";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else			
			if(let == '1') {
				String key = "1";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == '2') {
				String key = "2";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == '3') {
				String key = "3";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == '4') {
				String key = "4";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else	
			if(let == '5') {
				String key = "5";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else			
			if(let == '6') {
				String key = "6";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == '7') {
				String key = "7";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}	
			}else
			if(let == '8') {	
				String key = "8";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == '9') {
				String key = "9";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == ':') {
				String key = "deuxpoint";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
			}else
			if(let == '%') {
				String key = "percent";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
										
			}else
			if(let == '/') {
				String key = "slach";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
										
			}else
			if(let == '.') {
				String key = "point";
				if(color == 1) {
					Texture.alphabetWhite.get(key).bind();
				}else if (color == 2) {
					Texture.alphabetBlack.get(key).bind();
				}
										
			}else{
					Texture.espace.bind();	
					
			}
			

			LoaderMenus.setTextetData(font_size, x, y, Z);
			i++;

			}
		return lentgth_word;
		}
		
	}


