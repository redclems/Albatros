package fr.gearing.main.utility.Screen;

import static org.lwjgl.opengl.GL11.GL_NEAREST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.gearing.main.Albatros;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Screen.Banner.Banner;
import fr.gearing.main.utility.Screen.Banner.BarDownload;
import fr.gearing.main.utility.Screen.Button.ButtonCaseGame;
import fr.gearing.main.utility.Screen.Button.ButtonServeralSwicth;
import fr.gearing.main.utility.Screen.Button.ButtonSwicth;
import fr.gearing.main.utility.Screen.Button.ButtonSwicthScreen;
import fr.gearing.main.utility.Screen.Scroll.ScrollGame;
import fr.gearing.main.utility.Screen.Scroll.ScrollKey;
import fr.gearing.main.utility.Screen.Scroll.ScrollVertical;
import fr.gearing.main.utility.Screen.TextEdit.NameEnter;

public class ScreenSave {
	
	Map<String, Screen> screen;
	
	public ScreenSave() {
		screen = new HashMap<>();
		screen1();
	}
	public Screen getScreen(String nom) {
		return screen.get(nom);//screen is Map
	}
	
	
	
	public void screen1() {
		//background 1
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonLoader.png", GL_NEAREST));
		BarDownload bar = new BarDownload(50, 5, 0, -30, -9, 0, 100, 2, "donwload");
		
		screen.put("Load", new Screen(-9, back, bar));
	}
	public void screen2() {

		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
		
		int Y = 27;
		int X = 0;
		int Z = -4;
		int espace = 30;
		//banner
		Banner banner = new Banner(new Texture(Albatros.Srepcentral +"/textures/icon/albatros.png", GL_NEAREST), 70, 30, X, Y, Z);
		
		List<ButtonSwicthScreen> button = new ArrayList<>();
		button.add(new ButtonSwicthScreen(50, 22, X, Y-espace  , Z, "play", "MenusGame"));
		button.add(new ButtonSwicthScreen(50, 22, X, Y-espace*2, Z, "option", "Options"));
		
		Screen display = new Screen(Z, back, button, banner);
		
		screen.put("Menus", display);
	}
	public void screen3() {

		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
	
		//button at the left to the menus
		int widthBp = 39;
		int heightBp = 15;
		int x = 35;
		int y = 40;
		int Z = -4;
		int espace = 20;
		
		ScrollGame scroll = new ScrollGame(3, 100, 70, 100, 13, 0, Z, true, 15);
		
		List<ButtonSwicthScreen> button = new ArrayList<>();
		List<ButtonCaseGame> button2 = new ArrayList<>();
		button.add(new ButtonSwicthScreen(widthBp, heightBp, x, y  , Z, "New game", "NewGame"));
		
		button2.add(new ButtonCaseGame(widthBp, heightBp, x, y-espace  , Z, "start game", "play", scroll, true, true));
		button2.add(new ButtonCaseGame(widthBp, heightBp, x, y-espace*2, Z, "delete", "del", scroll, true, true));

		button.add(new ButtonSwicthScreen(widthBp, heightBp, x, y-espace*3, Z, "Menus", "Menus"));
		
		Screen display = new Screen(Z, back, button, button2, scroll);
		
		screen.put("MenusGame", display);
	}
	public void screen4() {

		int widthBp = 40;
		int heightBp = 15;
		int x = 0;
		int y = -40;
		int Z = -4;
		
		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
		
		NameEnter name = new NameEnter(65, 18, 0, 20, Z);
		
		List<ButtonSwicthScreen> button = new ArrayList<>();
		button.add(new ButtonSwicthScreen(widthBp, heightBp, x + widthBp, y, Z, "start", "generateGame"));
		button.add(new ButtonSwicthScreen(widthBp, heightBp, x - widthBp, y, Z, "cancel", "Menus"));
		
		Screen display = new Screen(Z, back, button, name);
		
		screen.put("NewGame", display);
	}
	public void screen5() {

		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
		
		int width = 45;
		int height = 18;
		int x1 = -30;
		int x2 = 30;
		int y1 = 30;
		int y2 = (int) (y1 - height*1.5f);
		int Z = -4;
		
		List<ButtonSwicthScreen> button = new ArrayList<>();
		button.add(new ButtonSwicthScreen(width, height, x1, y1, Z, "Music", "Option music"));
		button.add(new ButtonSwicthScreen(width, height, x2, y1, Z, "Display", "Option display"));
		button.add(new ButtonSwicthScreen(width, height, x1, y2, Z, "Keybords", "Option Keybords"));
		button.add(new ButtonSwicthScreen(width, height, x2, y2, Z, "graphics", "Option graphics"));
		button.add(new ButtonSwicthScreen(31, 14, 0, -40, Z, "Back", "Menus"));
		
		Screen display = new Screen(Z, back, button);
		
		screen.put("Options",display);
	}
	public void screen6() {
		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
		
		//initialisation
		int height = 20;
		int y1 = 40;
		int y2 = (int) (y1 - height*1.5f);
		int y3 = (int) (y2 - height*1.5f);
		int Z = -4;
		
		int x1 = 30;
		int x2 = -30;
		
		List<ButtonSwicthScreen> button = new ArrayList<>();
		button.add(new ButtonSwicthScreen(31, 14, 0, -40, Z, "Back", "Options"));

		List<ScrollVertical> scrollV = new ArrayList<>();
		scrollV.add(new ScrollVertical(80, 10, 80, 20, 0, y1, Z, save.songMain, 100, 0, 5, "main song"));
		scrollV.add(new ScrollVertical(50, 10, 50, 20, x1, y2, Z, save.songGame, 100, 0, 5, "game song"));
		scrollV.add(new ScrollVertical(50, 10, 50, 20, x2, y2, Z, save.songMenus, 100, 0, 5, "menus song"));
		scrollV.add(new ScrollVertical(50, 10, 50, 20, x1, y3, Z, save.songVox, 100, 0, 5, "vox song"));
		scrollV.add(new ScrollVertical(50, 10, 50, 20, x2, y3, Z, save.songMusic, 100, 0, 5, "music"));
		
		Screen display = new Screen(Z, back, button, scrollV);

		
		screen.put("OptionsMusic",display);
	}
	public void screen7() {

		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
		
		int Z = -4;

		List<ScrollVertical> scrollV = new ArrayList<>();
		scrollV.add(new ScrollVertical(60, 10, 60, 20, 0, 0, Z, save.zoomInt, 0, 10, 1, "zoom"));

		List<ButtonSwicthScreen> button = new ArrayList<>();
		button.add(new ButtonSwicthScreen(31, 14, 0, -40, Z, "Back", "Options"));
		
		Screen display = new Screen(Z, back, button, scrollV);
		
		screen.put("OptionsDisplay",display);
	}
	public void screen8() {
		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
		int Z = -4;
		
		ScrollKey scroll = new ScrollKey(2, 82, 70, 82, 20, 9, Z, true, 15);
		
		List<ButtonSwicthScreen> button = new ArrayList<>();
		button.add(new ButtonSwicthScreen(31, 14, 0, -40, Z, "Back", "Options"));

		
		int i = save.type_keyboards;
		int length = save.list_tipe_keyboards.size()-1;
		List<ButtonServeralSwicth> button2 = new ArrayList<>();
		button2.add(new ButtonServeralSwicth(43, 14, -39, -40, Z, save.list_tipe_keyboards, 0, length, i, "key"));

		Screen display = new Screen(Z, back, button, button2, scroll, true);
		
		screen.put("OptionsKey",display);
	}
	public void screen9() {

		
		//background
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonMenus.png", GL_NEAREST));
		int Z = -4;
		int y1 = 35;
		int height = 30;
		int y2 = y1 - height;
		int x1 = 30;
		int x2 = -33;
		//scroll
		
		List<ScrollVertical> scrollV = new ArrayList<>();
		scrollV.add(new ScrollVertical(70, 10, 60, 20, 0, y1, Z, save.chunks, 100, 10, 5, "render distance"));
		scrollV.add(new ScrollVertical(30, 10, 60, 20, x1, y2, Z, save.fps, 200, 20, 5, "FPS"));

		
		List<ButtonSwicth> button1 = new ArrayList<>();
		button1.add(new ButtonSwicth(50, 12, x2, y2, Z, "fps view", save.fpsview));

		List<ButtonSwicthScreen> button = new ArrayList<>();
		button.add(new ButtonSwicthScreen(31, 14, 0, -40, Z, "back", "Options"));
		
		Screen display = new Screen(Z, back, button, button1, scrollV,69);

		
		screen.put("OptionsGraphics",display);
	}

	public void screen10() {
		//background 1
		Background back = new Background(new Texture(Albatros.Srepcentral +"/textures/backgroung/fonLoader.png", GL_NEAREST));
		BarDownload bar = new BarDownload(50, 5, 0, -30, -9, 0, 100, 1, "donwload");

		screen.put("Load2", new Screen(-9, back, bar));
	}
	

	

}
