package fr.gearing.main.utility.Screen;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.utility.Screen.Banner.Banner;
import fr.gearing.main.utility.Screen.Banner.BarDownload;
import fr.gearing.main.utility.Screen.Button.Button;
import fr.gearing.main.utility.Screen.Button.ButtonCaseGame;
import fr.gearing.main.utility.Screen.Button.ButtonServeralSwicth;
import fr.gearing.main.utility.Screen.Button.ButtonSwicth;
import fr.gearing.main.utility.Screen.Button.ButtonSwicthScreen;
import fr.gearing.main.utility.Screen.Scroll.ScrollBar;
import fr.gearing.main.utility.Screen.Scroll.ScrollGame;
import fr.gearing.main.utility.Screen.Scroll.ScrollKey;
import fr.gearing.main.utility.Screen.Scroll.ScrollVertical;
import fr.gearing.main.utility.Screen.TextEdit.NameEnter;

public class Screen {
	
	Background back;
	private List<ScreenElement> element;
	private List<BarDownload> bar;
	private List<Button> button;
	private List<Banner> banner;
	private List<ButtonSwicthScreen> buttonSwicthScreen;
	private List<ButtonSwicth> buttonSwicth;
	private List<ButtonServeralSwicth> buttonServeralSwicth;
	private List<ButtonCaseGame> buttonCaseGame;
	private List<ScrollGame> scrollGame;
	private List<ScrollKey> scrollKey;
	private List<ScrollBar> scrollBar;
	private List<NameEnter>  nameEnter;
	private List<ScrollVertical>  scrollVertical;
	private boolean background = false;
	private boolean barBelow = false;
	private float Z;
	
	public Screen(float Z){
		element = new ArrayList<>();
		this.Z = Z;
	}
	
	public Screen(float Z, Background back, BarDownload bar){
		this.element = new ArrayList<>();
		this.bar = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		
		this.element.add(bar);
		this.bar.add(bar);
		
		
		this.Z = Z;
	}
	
	public Screen(int Z, Background back, List<ButtonSwicthScreen> button, Banner banner) {
		this.element = new ArrayList<>();
		this.banner = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		
		this.element.add(banner);
		this.banner.add(banner);
		
		for(ButtonSwicthScreen BP : button) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		
		
		this.Z = Z;
	}

	public Screen(int Z, Background back, List<ButtonSwicthScreen> button1, List<ButtonCaseGame> button2, ScrollGame scroll) {
		this.element = new ArrayList<>();
		this.scrollGame = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		this.buttonCaseGame = new ArrayList<>();
		this.scrollBar = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		

		
		for(ButtonSwicthScreen BP : button1) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		for(ButtonCaseGame BP : button2) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonCaseGame.add(BP);
		}
		this.element.add(scroll);
		this.scrollBar.add(scroll);
		this.scrollGame.add(scroll);
		
		this.Z = Z;
		
	}

	public Screen(int z, Background back, List<ButtonSwicthScreen> button, NameEnter name) {
		this.element = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		this.nameEnter = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		
		for(ButtonSwicthScreen BP : button) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}

		this.element.add(name);
		this.nameEnter.add(name);
		
		this.Z = z;
	}

	public Screen(int z, Background back, List<ButtonSwicthScreen> button) {
		this.element = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		this.nameEnter = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		
		for(ButtonSwicthScreen BP : button) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		
		this.Z = z;
	}


	public Screen(int z, Background back, List<ButtonSwicthScreen> button, List<ScrollVertical> scrollV) {
		this.element = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		this.nameEnter = new ArrayList<>();
		this.scrollVertical = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		
		for(ButtonSwicthScreen BP : button) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		
		for(ScrollVertical scroll : scrollV) {
			this.element.add(scroll);
			this.scrollVertical.add(scroll);
		}
		
		this.Z = z;
	}

	public Screen(int z, Background back, List<ButtonSwicthScreen> button1, List<ButtonServeralSwicth> button2, boolean useless) {
		this.element = new ArrayList<>();
		this.scrollGame = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonServeralSwicth = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		

		
		for(ButtonSwicthScreen BP : button1) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		for(ButtonServeralSwicth BP : button2) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonServeralSwicth.add(BP);
		}

		this.Z = z;
	}

	public Screen(int z, Background back, List<ButtonSwicthScreen> button1, List<ButtonSwicth> button2, int useless) {
		this.element = new ArrayList<>();
		this.scrollGame = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		this.buttonSwicth = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		

		
		for(ButtonSwicthScreen BP : button1) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		for(ButtonSwicth BP : button2) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicth.add(BP);
		}

		this.Z = z;
	}

	public Screen(int z, Background back, List<ButtonSwicthScreen> button1, List<ButtonServeralSwicth> button2, ScrollKey scroll, boolean bar) {
		this.element = new ArrayList<>();
		this.scrollGame = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonServeralSwicth = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		this.scrollKey = new ArrayList<>();
		this.scrollBar = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		

		
		for(ButtonSwicthScreen BP : button1) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		for(ButtonServeralSwicth BP : button2) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonServeralSwicth.add(BP);
		}
		this.element.add(scroll);
		this.scrollBar.add(scroll);
		this.scrollKey.add(scroll);
		
		this.barBelow = bar;

		this.Z = z;
	}

	public Screen(int z, Background back, List<ButtonSwicthScreen> button1, List<ButtonSwicth> button2, List<ScrollVertical> scrollV, int i) {
		this.element = new ArrayList<>();
		this.scrollGame = new ArrayList<>();
		this.button = new ArrayList<>();
		this.buttonSwicth = new ArrayList<>();
		this.buttonSwicthScreen = new ArrayList<>();
		this.scrollKey = new ArrayList<>();
		this.scrollVertical = new ArrayList<>();
		
		this.back = back;
		this.background = true;
		
		for(ButtonSwicthScreen BP : button1) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicthScreen.add(BP);
		}
		for(ButtonSwicth BP : button2) {
			this.element.add(BP);
			this.button.add(BP);
			this.buttonSwicth.add(BP);
		}


		this.Z = z;
		
		for(ScrollVertical scroll : scrollV) {
			this.element.add(scroll);
			this.scrollVertical.add(scroll);
		}
	}

	public void addElement(ScreenElement elem) {
		if(collision(elem))element.add(elem);
	}
	
	public void Display() {
		if(background)back.Display();
		
		//change type ok keyboards
		if(barBelow)GameUtility.setBarPosition(60, 0, 0);
		
		for(ScreenElement elem : element) {
			elem.Display();
		}
		
	}
	public void barAdd(int i, int val){
		bar.get(i).addVal(val);
	}
	public void barSet(int i, int val){
		bar.get(i).setVal(val);
	}
	public int barGet(int i){
		return bar.get(i).getVal();
	}
	
	public boolean collision(ScreenElement elem1){
		for(ScreenElement elem2 : element) {
			if(!elem1.hitboxOK(elem2))return false;
		}
		return true;
	}
	
	public int updateVerticalScroll(int i) {
		return scrollVertical.get(i).getvalSave();
	}
	public boolean updateButtonSwicth(int i) {
		return buttonSwicth.get(i).getvalSave();
	}
	public int updateButtonServeralSwicth(int i) {
		return buttonServeralSwicth.get(i).getvalSave();
	}
	
	
	
	public float getZ() {
		return Z;
	}
}
