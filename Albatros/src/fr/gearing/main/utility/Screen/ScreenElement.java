package fr.gearing.main.utility.Screen;

public abstract class ScreenElement {
	protected float width;
	protected float height;
	protected float x;
	protected float y;
	protected float z;
	
	public float yMin;
	public float yMax;
	public float xMin;
	public float xMax;
	
	
	
	public abstract void Display();

	
	private void set(){
		yMin = y - height/2; 
		yMax = y + height/2;
		xMin = x - width/2;
		xMax = x + width/2;
	}
	public boolean hitboxOK(ScreenElement elem) {
		this.set();
		elem.set();
		boolean res = false;
		if(this.yMin < elem.yMax || this.yMax > elem.yMin) {
			if(this.xMin < elem.xMax || this.xMax > elem.xMin) {
				res = true;
			}
		}
		
		return res;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	
	public float getZ() {
		return z;
	}
}
