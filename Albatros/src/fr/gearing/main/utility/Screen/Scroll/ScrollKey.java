package fr.gearing.main.utility.Screen.Scroll;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.game.save;
import fr.gearing.main.utility.Screen.Scroll.Case.CaseKey;


public class ScrollKey extends ScrollBar{

	
	protected List<CaseKey> cases;
	
	public ScrollKey(int WidthScroll, int HeightScroll, int Width, int Height, int X, int Y, int Z, boolean Vertical, int sizeCase) {
		super( WidthScroll, HeightScroll, Width, Height, X, Y, Z, Vertical, sizeCase);
		creatCase();

	}

	@Override
	public void displayCase() {

		for(CaseKey elem : cases) {		
			
			boolean dep_max = (elem.getMove() - heightCase/2) <= (y + height/2);
			boolean dep_min = (elem.getMove() - heightCase/3) >= (y - height/2);
			
			if(elem.getClick()) {
				caseBind();
				elem.setCaseBind(true);
				elem.setClick(false);
			}
			
			//background case
			if(dep_max && dep_min) {
				elem.display();
				
			}
			
			elem.setMove(-axeMouvCase);
			if(elem.update) {
				updateCases();
				elem.update = false;
			}
		}
		
	}

	@Override
	public void creatCase() {
		
		cases = new ArrayList<>();

		
  		heightCase = GameUtility.change_percentage_by_height(25, z);
  		sizeCase = heightCase;
  		widthCase = GameUtility.change_percentage_by_width(70, z);
  		xCases = GameUtility.change_percentage_by_Co_X(-25, z);
  		yCases = GameUtility.change_percentage_by_Co_Y(0, z);
  		ep = sizeCase/10;

		//set number of case
		NBCase = 7;

		
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "forward", save.forward));
		yCases = yCases-(ep+heightCase);
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "backward", save.backward));
		yCases = yCases-(ep+heightCase);
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "right", save.right));
		yCases = yCases-(ep+heightCase);
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "left", save.left));
		yCases = yCases-(ep+heightCase);
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "sprint", save.sprint));
		yCases = yCases-(ep+heightCase);
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "up", save.up));
		yCases = yCases-(ep+heightCase);
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "down", save.down));
		yCases = yCases-(ep+heightCase);
		cases.add(new CaseKey(widthCase, heightCase, xCases, yCases, z, "echap", save.kick));

		
	}
	public void updateCases() {
		int i =0;
		save.forward = cases.get(i).getId();
		i++;
		save.backward = cases.get(i).getId();
		i++;
		save.right = cases.get(i).getId();
		i++;
		save.left = cases.get(i).getId();
		i++;
		save.sprint = cases.get(i).getId();
		i++;
		save.up = cases.get(i).getId();
		i++;
		save.down = cases.get(i).getId();
		i++;
		save.kick = cases.get(i).getId();
		i++;

	}

	@Override
	public void caseBind() {
		for(CaseKey elem : cases) {
			elem.setCaseBind(false);
		}
		
	}


}
