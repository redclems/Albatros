package fr.gearing.main.utility.Screen.Scroll.Case;


import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Keyboards;
import fr.gearing.main.utility.Mousse;
import fr.gearing.main.utility.Write;
import fr.gearing.main.utility.Screen.Button.ButtonNone;

public class CaseKey extends Cases{


	protected String action;
	protected int id;
	
	protected float yMove;
	protected ButtonNone button;
	public boolean update = false;
	
	public CaseKey(float width, float height, float x, float y, float z, String action, int id) {
		super(width, height, x, y, z-0.1f);
		this.action = action;
		button = new ButtonNone(getHeight(), getHeight()/2, x+width/4, y, z, Keyboards.decoder_id(id));
		this.id = id;
	}

	@Override
	public void display() {

		float y = yMove;
		Write.write(action, 1, width*0.042f, (x - width/3), y, z+0.03f, false);
		button.setY(y);
		button.Display();
			
		if(Mousse.MousseHover(width, height, x, y, z) == true || caseBind) {
			Texture.view.bind();
			LoaderMenus.setTrans2D(width, height, x, y, z+0.01f);
			
			
			if(Mousse.mousse_etat) {
				click = true;
				
			}
		}

		
		if(caseBind) {
			update();
		}
	}
	public void update() {
		int idchange = Keyboards.key_indices();
		if(idchange != 0) {
			caseBind = false;
			id = idchange;
			button.setTitle(Keyboards.decoder_id(id));
			update = true;
		}
		
	}
	
	public void setMove(float val) {
		yMove = y + val;
	}
	public float getMove() {
		return yMove;
	}
	public int getId() {
		return id;
	}

}
