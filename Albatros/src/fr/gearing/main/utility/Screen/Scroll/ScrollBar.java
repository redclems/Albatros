package fr.gearing.main.utility.Screen.Scroll;


import org.lwjgl.input.Mouse;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Mousse;
import fr.gearing.main.utility.Screen.ScreenElement;



public abstract class ScrollBar extends ScreenElement{

	
	protected float axeMouvScroll;
	protected float axeMouvCase;
	protected boolean vertical;
	
	protected float sizeScroll;
	protected float sizeCase;
	
	protected float ep;
	protected int NBCase;
	protected boolean click;
	
	protected float widthScroll;
	protected float heightScroll;
	
	protected float widthCase;
	protected float heightCase;
	
	protected float xCases;
	protected float yCases;
	
	protected float xScroll;
	protected float yScroll;
	
	
	public ScrollBar(int WidthScroll, int HeightScroll, int Width, int Height, int X, int Y, int Z, boolean Vertical, int sizeCase) {
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		this.widthScroll = GameUtility.change_percentage_by_width(WidthScroll, Z);
		this.heightScroll = GameUtility.change_percentage_by_height(HeightScroll, Z);
		this.vertical = Vertical;
		this.ep = sizeCase/20;
		if(vertical) {
			this.sizeCase = GameUtility.change_percentage_by_height(sizeCase, Z);
			heightCase = sizeCase;
			widthCase = width - (ep + widthScroll);
			xScroll = x + (width/2 - widthScroll/2);
			xCases = x - (ep + widthScroll);
			yCases = y + (height/2 - (ep + heightCase/2));
			yScroll = y;
			
		}else {
			this.sizeCase = GameUtility.change_percentage_by_width(sizeCase, Z);
			heightCase = height - (ep + heightScroll);
			widthCase = sizeCase;
			yScroll = y + (height/2 - heightScroll/2);
			yCases = y - (ep + heightScroll);
			xCases = x + (width/2 - (ep + widthCase/2));
			xScroll = x;
		}
		
		creatCase();
		syzeScroll();
		maxScroll();
		
	}
	
	public void maxScroll() {
		/*
		 * set number of case
		 * this function doesn't return, change the map scroll for put the scroll to the top
		 */
		float max;
		if(vertical) {
			max = (y + heightScroll/2);
		}else {
			max = (x - widthScroll/2);
		}

		axeMouvScroll = max - sizeScroll/2;
	}
	public void mimScroll() {
		/*
		 * set number of case
		 * this function doesn't return, change the map scroll for put the scroll to the bottom
		 */
		float min;
		if(vertical) {
			min = (y - heightScroll/2);
		}else {
			min = (x - widthScroll/2);
		}

		axeMouvScroll = min + sizeScroll/2;
	
	}

	public void setNBCase(int nbcase) {
		this.NBCase = nbcase;
	}
	public void setSizeCase(int Sizecase) {
		 this.sizeCase = GameUtility.change_percentage_by_height(Sizecase, z);
	}
	
	public void syzeScroll() {
		/*
		 * this function calcul the size for the scroll bar
		 */
		float ep = this.ep;
		float sizeScroll;
		float NBCaseForOneScreen;
		if(vertical) {

			sizeScroll = heightScroll;
			NBCaseForOneScreen = (int) (heightScroll/(sizeCase+ep));
		
			if(NBCase > NBCaseForOneScreen) {
				sizeScroll = heightScroll/(NBCase/NBCaseForOneScreen);
				if(sizeScroll <= heightScroll/25) {
					sizeScroll = heightScroll / 25;
				}
			}
		}
		else {
			sizeScroll = widthScroll;
			NBCaseForOneScreen = (int) (widthScroll/(sizeCase+ep));
		
			if(NBCase > NBCaseForOneScreen) {
				sizeScroll = widthScroll/(NBCase/NBCaseForOneScreen);
				if(sizeScroll <= widthScroll/25) {
					sizeScroll = widthScroll / 25;
				}
			}

		}
		this.sizeScroll = sizeScroll;
	}
	
	public boolean exseedScroll() {
		/*
		 * set number of case
		 * this function doesn't return, change the map scroll for move the scroll bar with mouse
		 */
		boolean res = false; 
		float max;
		if(vertical) {
			max = (y + height/2);
		}else {
			max = (x + widthScroll/2);
		}
		float min;
		if(vertical) {
			min = (y - height/2);
		}else {
			min = (x - widthScroll/2);
		}

		if(axeMouvScroll > (max - sizeScroll/2.01f)) {
			maxScroll();
			res = true;
		}else 
		if(axeMouvScroll < (min + sizeScroll/2.01f)){
			mimScroll();
			res = true;
		}
		
		return res;
	}
	
	public void Display() {
		/*
		 *display the scroll bar in function to name 
		 */
			displayCase();
			
			boolean leftButtonDown = Mouse.isButtonDown(0);
			syzeScroll();
		
			Texture.fonscroll.bind();
			LoaderMenus.setTrans2D(widthScroll, height, x, y, z);
		
			moveWidthMouseScroll();
			
			//creation du scroll
			Texture.scroll.bind();
			LoaderMenus.setTrans2D(widthScroll*0.7f, sizeScroll, x, axeMouvScroll, z+0.02f);
			/*
			if(vertical) {
				LoaderMenus.setTrans2D(widthScroll*0.8f, sizeScroll, x, y, z+0.02f);
			}else {
				LoaderMenus.setTrans2D(sizeScroll, heightScroll*0.8f, x, y, z+0.02f);
				//axeMouvScroll
			}
			*/
		
			//move the scroll bar
			if((Mousse.MousseHover(widthScroll, heightScroll, x, y, z) == true || click == true)) {
				Texture.fon.bind();
				LoaderMenus.setTrans2D(widthScroll, heightScroll, x, y, z+0.04f);
			
				if(leftButtonDown && !exseedScroll()) {
					axeMouvScroll = Mousse.Moussefollow_y(z);
					click = true;
				}else {
					click = false;
				}
			
			}else {
				exseedScroll();
			}
			
			displayCase();
			axeMouvCase();

	}
	
	public void axeMouvCase() {
		/*
		 * for calcul the y of case
		 * 
		 */

		float max;
		if(vertical) {
			max = (y + height/2);
		}else {
			max = (x + widthScroll/2);
		}
		float min;
		if(vertical) {
			min = (y - height/2);
		}else {
			min = (x - widthScroll/2);
		}
			
		float heightCase = (sizeCase + ep) * NBCase;

		if ((max - min) > 0 || (max - min) < 0) {
			this.axeMouvCase = heightCase * (axeMouvScroll - max) / (max - min);
		}
		
	}
	
	public void moveWidthMouseScroll() {
		/*
		 * this function is for use scroll bar with the scroll mouse
		 */
		if(!exseedScroll()) {
			float notches = Mouse.getDWheel();  
			notches = notches/10000;
			axeMouvScroll = axeMouvScroll + notches;
		}

	}
	
	public abstract void caseBind();
	
	public abstract void displayCase();
	
	public abstract void creatCase();


}
