package fr.gearing.main.utility.Screen.Scroll;


import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.utility.Screen.Scroll.Case.CaseGame;

public class ScrollGame extends ScrollBar{
	 
    protected String action;
	protected List<CaseGame> cases;
	protected List<CaseGame> casesSave;
	protected CaseThread creatCase;
	protected boolean buttonBind;
    
	public ScrollGame(int WidthScroll, int HeightScroll, int Width, int Height, int X, int Y, int Z,boolean Vertical, int sizeCase) {
		super(WidthScroll, HeightScroll, Width, Height, X, Y, Z, Vertical, sizeCase);
		cases = new ArrayList<>();
		creatCase();
	}

	@Override
	public void displayCase() {
		buttonBind = true;
		for(CaseGame elem : cases) {	
			
			boolean dep_max = (elem.getMove() - elem.getHeight()/2) <= (y + height/2);
			boolean dep_min = (elem.getMove() >= (y - height/2));

			if(elem.getClick()) {
				caseBind();
				elem.setCaseBind(true);
				elem.setClick(false);
			}
			if(elem.getCaseBind() && action != "") {
				if(action.equals("play"))elem.play();
				if(action.equals("del")) {
					elem.delete();
					creatCase();
				}
			}
			if(elem.getCaseBind()) {
				buttonBind = false;
			}
	
			//background case
			if(dep_max && dep_min) {
				elem.display();
			}
			
			elem.setMove(-axeMouvCase);
		}
		
		action = "";
		if(!creatCase.end)creatCaseEdit();
		
	}

	@Override
	public void creatCase() {
		casesSave = new ArrayList<>();
		
  		heightCase = GameUtility.change_percentage_by_height(25, z);
  		sizeCase = heightCase;
  		widthCase = GameUtility.change_percentage_by_width(70, z);
  		xCases = GameUtility.change_percentage_by_Co_X(-25, z);
  		yCases = GameUtility.change_percentage_by_Co_Y(-14, z);
  		ep = sizeCase/10;
  		NBCase = 0;

  		
		creatCase = new CaseThread(widthCase, heightCase, xCases, yCases, z, ep);

		

  	
	}
	public void creatCaseEdit() {
		creatCase.run();
  		
  		//set number of case
  		this.casesSave = creatCase.cases;

  		if(creatCase.end) {
  			cases = new ArrayList<>();
  			this.cases = this.casesSave;
  			this.NBCase = creatCase.NBCase;
  			maxScroll();
  		}

		
	}
	
	

	
	public boolean getButtonBind() {
		return buttonBind;
	}
	
	public void action(String action) {
		/*
		 * 
		 * "del" for del game
		 * "play" for play of the game
		 */
		this.action = action;
		
	}

	@Override
	public void caseBind() {
		for(CaseGame elem : cases) {
			elem.setCaseBind(false);
		}
		
	}

}
