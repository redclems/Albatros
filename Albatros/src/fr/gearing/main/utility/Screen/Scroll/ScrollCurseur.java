package fr.gearing.main.utility.Screen.Scroll;



import fr.gearing.main.game.GameUtility;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Screen.ScreenElement;

public abstract class ScrollCurseur extends ScreenElement{
	
	protected int valMax, valMin, val;
	
	protected float axeMouvScroll;
	
	public ScrollCurseur(Texture texture, int Width, int Height, int X, int Y, int Z, int valMax, int valMin, int val) {
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		this.val = val;
		this.valMax = valMax;
		this.valMin = valMin;
		
	}


}
