package fr.gearing.main.utility.Screen.Scroll;

import java.io.File;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Albatros;
import fr.gearing.main.utility.Screen.Scroll.Case.CaseGame;

public class CaseThread {
	protected DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT);
	List<CaseGame> cases;
	float heightCase;
	float sizeCase;
	float widthCase;
	float xCases;
	float yCases;
	float ep = sizeCase/10;
	private float z;
	boolean end;
		
	//set number of case 
	int NBCase;
	int NBCaseMax;
	
	public CaseThread(float width, float height,float x, float y, float z, float ep){
	      this.z = z;
	      this.NBCase = 0;
	      this.end = false;
	      this.cases = new ArrayList<>();
	      
	      this.heightCase = height;
	      this.sizeCase = height;
	      this.widthCase = width;
	      this.xCases = x;
	      this.yCases = y;
	  	  this.ep = ep;
	}

	/*
	 * this class is for multy tread the load of the file
	 */

	   	   
	   public void run() {
	  		//files stocker in save map
	  		String[] nomFichiers = Albatros.repsave.list();
	 
	  		
	  		
	  		//set number of case
	  		NBCaseMax = nomFichiers.length;
	  		
	  		//display
	  		int ncase_trop_grand = 0;
	  		
	  		if(NBCase < NBCaseMax) {
	  			String game_name = nomFichiers[NBCase];
	  			File rep = new File (Albatros.repsave + "/" + game_name);
	  			
	  			Date d = new Date(rep.lastModified());
	  			String date_creation = "7/02/2003";//shortDateFormat.format(d);
	  			if(game_name.charAt(0) != '.') { //to don't have the hidden folder			
	  				if(game_name.length() > 18) {
	  					ncase_trop_grand++;
	  					char[] name_save = game_name.toCharArray();
	  					game_name = "";
	  					for(int i2 = 0; i2 < 18; i2++) {
	  						game_name = game_name + name_save[i2];
	  					}
	  					game_name = game_name + ncase_trop_grand + '.' + '.';
	  				}
	  				cases.add(new CaseGame(widthCase, heightCase, xCases, yCases, z,game_name, date_creation, rep));
	  				yCases -= (heightCase + ep);
	  				NBCase++;
	  			}
	  		}else {
	  			end = true;
	  		}

	   }
	   
	   public boolean getEnd() {
		   return end;
	   }


}
