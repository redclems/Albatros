package fr.gearing.main.utility.Screen.Scroll.Case;



public abstract class Cases{
	
	protected float width;
	protected float height;
	protected float x;
	protected float y;
	protected float z;
	protected boolean caseBind;
	protected boolean click;

	
	public Cases(float width, float height, float x, float y, float z) {
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public boolean getClick() {
		
		return click;
	}
	public void setClick(boolean etat) {
		
		 click = etat;
	}
	public void setCaseBind(boolean bind) {
		caseBind = bind;
	}
	
	public abstract void display();
	public float getY() {
		return y;
	}
	public float getHeight() {
		return height;
	}
	public boolean getCaseBind() {

		return caseBind;
	}

}
