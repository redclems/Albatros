package fr.gearing.main.utility.Screen.Scroll.Case;



import static org.lwjgl.opengl.GL11.GL_NEAREST;

import java.io.File;

import fr.gearing.main.Albatros;
import fr.gearing.main.game.Gamegenerator;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Filegestionaire;
import fr.gearing.main.utility.Mousse;
import fr.gearing.main.utility.Write;


public class CaseGame extends Cases{
	
	public Texture iconSave = new Texture(Albatros.Srepcentral + "/textures/icon/icon.png" , GL_NEAREST);
		
	protected float yMove;
	
	protected String name;
	protected String date;
	protected File finder;
		
	public CaseGame(float width, float height, float x, float y, float z,String name, String date, File finder) {
		super(width, height, x, y, z);
		this.name = name;
		this.date = date;
		this.finder = finder;
		
		String icon = finder + "/icon.png";
		File iconFind = new File(icon);
		if(iconFind.exists()) {
			iconSave = new Texture(icon , GL_NEAREST);
		}
	}

	@Override
	public void display() {
		
		float y = yMove;
		Texture.bpG.bind();
		LoaderMenus.setTrans2D(width, height, x, y, z);
	
		//icon
		float ep = height/10;
		float whIcon = height*0.70f;//width and height of icon
		float espace = height*0.26f;
		float xIcon = (x - width/2 ) + whIcon*0.7f + ep;
		
		iconSave.bind();//change for icon
		LoaderMenus.setTrans2D(whIcon, whIcon, xIcon, y, z+0.02f);
		
		float  Xtexte = (x - width/2.55f) + whIcon/2 + espace;
		//name of the game
		float fontSize = width*0.042f;
		float yFont = y + height/4 ;
		Write.write(name, 1,fontSize, Xtexte, yFont, z+0.02f, false);
		
		//name of the game
		float font_size_date = width*0.028f;
		Write.write(date, 1,font_size_date, Xtexte, yFont-fontSize, z+0.02f, false);
				
			
		if(Mousse.MousseHover(width, getHeight(), x, y, z) == true || caseBind) {
			Texture.fon.bind();
			LoaderMenus.setTrans2D(width, getHeight(), x, y, z+0.03f);
			if(Mousse.mousse_etat) {
				click = true;
				
			}
		}		
	}
	
	public void delete(){
		Filegestionaire.delete(finder);
		caseBind = false;
	}
	public void play(){
		System.out.println("no action ");
		
		Gamegenerator.switche = true;
		Gamegenerator.screen = "load map";
		caseBind = false;
	}
	
	public void setMove(float val) {
		yMove = y + val;
	}
	public float getMove() {
		return yMove;
	}

}
