package fr.gearing.main.utility.Screen.Scroll;

import org.lwjgl.input.Mouse;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Mousse;
import fr.gearing.main.utility.Write;
import fr.gearing.main.utility.Screen.ScreenElement;

public class ScrollVertical extends ScreenElement{
	
	protected float axeMouvScroll;
	
	protected int step;
	protected int valSave;
	protected int valMax;
	protected int valMin;
	
	protected float heightCurseur;
	protected float widthCurseur;
	protected float heightBar;
	
	protected String name;
	
	protected boolean focus = false;
	
	
	
	public ScrollVertical(int WidthScroll, int HeightScroll, int Width, int Height, int X, int Y, int Z, int valSave, int valMax, int valMin, int step, String name) {
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		
		this.valSave = valSave;
		this.valMax = valMax;
		this.valMin = valMin;
		this.step = step;
		
		this.heightCurseur = height*0.4f;
		this.widthCurseur = height*0.2f;
		this.heightBar = height*0.15f;
		this.name = name;
	}
	public void maxScroll() {
		/*
		 * set number of case
		 * this function doesn't return, change the map scroll for put the scroll to the top
		 */
		 
		axeMouvScroll = (x + width/2) - widthCurseur/2;
	}
	public void mimScroll() {
		/*
		 * set number of case
		 * this function doesn't return, change the map scroll for put the scroll to the bottom
		 */
		
		axeMouvScroll = (x - width/2) + widthCurseur/2;
	
	}
	
	public void exseedScroll() {
		/*
		 * set number of case
		 * this function doesn't return, change the map scroll for move the scroll bar with mouse
		 */

		float max = (x + width/2) - widthCurseur/2;
		float min = (x - width/2) + widthCurseur/2;
		

		if(axeMouvScroll > (max - widthCurseur/2.01f)) {
			maxScroll();
			focus = false;
		}else 
		if(axeMouvScroll < (min + widthCurseur/2.01f)){
			mimScroll();
			focus = false;
		}
		

	}
	public void setvalSave(int val) {
		valSave = val;
	}
	public int getvalSave() {
		return valSave;
	}
	
	public void moveWidthMouseScroll() {
		/*
		 * this function is for use scroll bar with the scroll mouse
		 */
		 float notches = Mouse.getDWheel();  
		 notches = notches/10000;
		 axeMouvScroll = axeMouvScroll + notches;

	}
	public boolean getFocus() {
		
		return focus;
	}
	public void setFocus(boolean etat) {
		
		focus = etat;
	}
	
	
	@Override
	public void Display() {
		boolean leftButtonDown = Mouse.isButtonDown(0);
		
		Texture.fonscroll.bind();
		LoaderMenus.setTrans2D(width, heightBar, x, y + height/2 - heightCurseur/2, z);
		
		//creation du scroll
		Texture.scroll.bind();
		LoaderMenus.setTrans2D(widthCurseur, heightCurseur, axeMouvScroll, y + height/2 - heightCurseur/2, z+0.003f);
		
		//texte
		Write.write(name + ":" +  valSave, 1 , heightCurseur*0.70f, x, y - height*0.6f + heightCurseur/2, z, true);
		exseedScroll();
		axeMouvCase();

		//move the scroll bar
		if(Mousse.MousseHover(width, height, x, y, z) || focus) {
			Texture.fon.bind();
			LoaderMenus.setTrans2D(width, heightCurseur, x, y + height/2 - heightCurseur, z);
			moveWidthMouseScroll();
			
			if(leftButtonDown) {
				axeMouvScroll = Mousse.Moussefollow_x(z);
				focus = true;
				
			}else {
				focus = false;
			}
		
		}
		
		
	}
	public void axeMouvCase() {
		/*
		 * for calcul the y of case
		 * 
		 */

		float xMax = x + width/2 - widthCurseur/2;
		float xMin = x - width/2 + widthCurseur/2;

		float val = GameUtility.echelleToOther(axeMouvScroll, xMin, xMax, valMin, valMax);
		float modulo = val % step;
		if(modulo >= step/2) {
			valSave = (int) ((val - modulo) + step); 
		}else {
			valSave = (int) (val - modulo); 
		}
		
		
	}

}
