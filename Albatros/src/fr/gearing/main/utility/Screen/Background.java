package fr.gearing.main.utility.Screen;


import fr.gearing.main.game.GameUtility;
import fr.gearing.main.game.save;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;

public class Background extends ScreenElement{
	Texture texture;

	public Background(Texture texture) {
		this.texture = texture;
		
	}

	@Override
	public void Display() {
		texture.bind();
		float z = -10f;
		float x = GameUtility.change_percentage_by_Co_X(0, z);
		float y = GameUtility.change_percentage_by_Co_Y(0, z);
		int zoom = (int) (102*save.zoom);
		float width = GameUtility.change_percentage_by_width(zoom, z);
		float height = GameUtility.change_percentage_by_height(zoom, z);
		LoaderMenus.setTrans2D(width, height, x, y, z);
		
	}

}
