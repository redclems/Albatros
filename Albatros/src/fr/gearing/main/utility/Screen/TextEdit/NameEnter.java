package fr.gearing.main.utility.Screen.TextEdit;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.game.save;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Keyboards;
import fr.gearing.main.utility.Write;
import fr.gearing.main.utility.Screen.ScreenElement;

public class NameEnter extends ScreenElement{
	
	protected String nameSave = "";
	public static char none = 'å';
	private static char delete = '„';
	
	private static int max_buffer = 30;
	private static int buffer = 0;
	
	private static char last_letter;

	public NameEnter(int Width, int Height, int X, int Y, int Z) {
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);

	}
	
	public void listen() {
		//test keybords
		char letter = none;
		if(save.type_keyboards == 0){
			letter = NameEntersAZERTY();
		}else if(save.type_keyboards == 1){
			letter = NameEntersQWERTY();
		}else {
			System.out.println("Keyboards not find" + save.type_keyboards);
		}
		//stock new letter in Name
		if((buffer == 0 || (last_letter != letter && buffer > max_buffer/2)) && (nameSave.length() < 15 || letter == delete)) {
			
			buffer = 0;
			if(letter != none) {
				
				buffer++;
				last_letter = letter;
				if(letter != delete) {//if delete the case == true
					nameSave = nameSave + letter;
				}else {
					String name_save = nameSave;
					char save_letter[]  = name_save.toCharArray();//letter 
					nameSave = "";
					for(int i = 0; i < name_save.length()-1; i++) {
						nameSave = nameSave + save_letter[i];
					}
				}
			}
		}else {
			buffer++;
			if(buffer >= max_buffer ) buffer = 0;
		}	
	}
	
	public char NameEntersQWERTY() {
		/*
		 * listen the QWERTY keyboard and return the key 
		 */

	char Name =  none;

		if(Keyboards.Key0 == true) {
			
			Name = '0';
		}else if(Keyboards.Key1 == true ) {
			
			Name = '1';
		}else if(Keyboards.Key2 == true ) {
			
			Name = '2';
		}else if(Keyboards.Key3 == true ) {
			
			Name = '3';
		}else if(Keyboards.Key4 == true ) {
			
			Name ='4';
		}else if(Keyboards.Key5 == true ) {
			
			Name ='5';
		}else if(Keyboards.Key6 == true ) {
			
			Name ='6';
		}else if(Keyboards.Key7 == true ) {
			
			Name ='7';
		}else if(Keyboards.Key8 == true ) {
			
			Name ='8';
		}else if(Keyboards.Key9 == true ) {
			
			Name ='9';
		}else if(Keyboards.KeyA == true ) {
			
			Name ='a';
		}else if(Keyboards.KeyB == true ) {
			
			Name ='b';
		}else if(Keyboards.KeyC == true ) {
			
			Name ='c';
		}else if(Keyboards.KeyD == true ) {
			
			Name ='d';
		}else if(Keyboards.KeyE == true ) {
			
			Name ='e';
		}else if(Keyboards.KeyF == true ) {
			
			Name ='f';
		}else if(Keyboards.KeyG == true ) {
			
			Name ='g';
		}else if(Keyboards.KeyH == true ) {
			
			Name ='h';
		}else if(Keyboards.KeyI == true ) {
			
			Name ='i';
		}else if(Keyboards.KeyJ == true ) {
			
			Name ='j';
		}else if(Keyboards.KeyK == true ) {
			
			Name ='k';
		}else if(Keyboards.KeyL == true ) {
			
			Name ='l';
		}else if(Keyboards.KeyN == true ) {
			
			Name ='n';
		}else if(Keyboards.KeyM == true ) {
			
			Name ='m';
		}else if(Keyboards.KeyO == true ) {
			
			Name ='o';
		}else if(Keyboards.KeyP == true ) {
			
			Name ='p';
		}else if(Keyboards.KeyQ == true ) {
			
			Name ='q';
		}else if(Keyboards.KeyR == true ) {
			
			Name ='r';
		}else if(Keyboards.KeyS == true ) {
			
			Name ='s';
		}else if(Keyboards.KeyT == true ) {
			
			Name ='t';
		}else if(Keyboards.KeyX == true ) {
			
			Name ='x';
		}else if(Keyboards.KeyY == true ) {
			
			Name ='y';
		}else if(Keyboards.KeyW == true ) {
			
			Name ='w';
		}else if(Keyboards.KeyV == true ) {
			
			Name ='v';
		}else if(Keyboards.KeyU == true ) {
			
			Name ='u';
		}else if(Keyboards.KeyZ == true ) {
			
			Name ='z';
		}
	
		if(Keyboards.Keydelete) {
			Name = delete;
			
		}
	  return Name;
	}
	public char NameEntersAZERTY() {
		/*
		 * listen the AZERTY keyboard and return the key 
		 */
	char Name = none;

		if(Keyboards.Key0 == true) {
			
			Name = '0';
		}else if(Keyboards.Key1 == true ) {
			
			Name = '1';
		}else if(Keyboards.Key2 == true ) {
			
			Name = '2';
		}else if(Keyboards.Key3 == true ) {
			
			Name = '3';
		}else if(Keyboards.Key4 == true ) {
			
			Name ='4';
		}else if(Keyboards.Key5 == true ) {
			
			Name ='5';
		}else if(Keyboards.Key6 == true ) {
			
			Name ='6';
		}else if(Keyboards.Key7 == true ) {
			
			Name ='7';
		}else if(Keyboards.Key8 == true ) {
			
			Name ='8';
		}else if(Keyboards.Key9 == true ) {
			
			Name ='9';
		}else if(Keyboards.KeyA == true ) {
			
			Name ='q';
		}else if(Keyboards.KeyB == true ) {
			
			Name ='b';
		}else if(Keyboards.KeyC == true ) {
			
			Name ='c';
		}else if(Keyboards.KeyD == true ) {
			
			Name ='d';
		}else if(Keyboards.KeyE == true ) {
			
			Name ='e';
		}else if(Keyboards.KeyF == true ) {
			
			Name ='f';
		}else if(Keyboards.KeyG == true ) {
			
			Name ='g';
		}else if(Keyboards.KeyH == true ) {
			
			Name ='h';
		}else if(Keyboards.KeyI == true ) {
			
			Name ='i';
		}else if(Keyboards.KeyJ == true ) {
			
			Name ='j';
		}else if(Keyboards.KeyK == true ) {
			
			Name ='k';
		}else if(Keyboards.KeyL == true ) {
			
			Name ='l';
		}else if(Keyboards.KeyN == true ) {
			
			Name ='n';
		}else if(Keyboards.KeyMazerty == true ) {
			
			Name ='m';
		}else if(Keyboards.KeyO == true ) {
			
			Name ='o';
		}else if(Keyboards.KeyP == true ) {
			
			Name ='p';
		}else if(Keyboards.KeyQ == true ) {
			
			Name ='a';
		}else if(Keyboards.KeyR == true ) {
			
			Name ='r';
		}else if(Keyboards.KeyS == true ) {
			
			Name ='s';
		}else if(Keyboards.KeyT == true ) {
			
			Name ='t';
		}else if(Keyboards.KeyX == true ) {
			
			Name ='x';
		}else if(Keyboards.KeyY == true ) {
			
			Name ='y';
		}else if(Keyboards.KeyW == true ) {
			
			Name ='z';
		}else if(Keyboards.KeyV == true ) {
			
			Name ='v';
		}else if(Keyboards.KeyU == true ) {
			
			Name ='u';
		}else if(Keyboards.KeyZ == true ) {
			
			Name ='w';

		}
	
		if(Keyboards.Keydelete) {
			Name = delete;
		}
		
		return Name;
	}

	@Override
	public void Display() {

		//String word ,float f ,float Police, float X, float Y, float Z
		Texture.bpG.bind();
		LoaderMenus.setTrans2D(width, height, x, y, z);
		Write.write(nameSave, 1, height/2, x+height/4, y, z + 0.02f, true);
		listen();
				
	}
	
	public String getNameSave() {
		return nameSave;
	}

}