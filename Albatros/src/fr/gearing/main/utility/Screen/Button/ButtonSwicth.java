package fr.gearing.main.utility.Screen.Button;


public class ButtonSwicth extends Button{

	protected String titlesave;
	boolean switchSave;

	public ButtonSwicth(int Width, int Height, int X, int Y, int Z, String title, boolean bind, boolean bindView, boolean save) {
		super(Width, Height, X, Y, Z, title, bind, bindView);
		this.switchSave = save;
		this.titlesave = title;
		this.title = titlesave + " : " + switchSave;
	}
	public ButtonSwicth(int Width, int Height, int X, int Y, int Z, String title, boolean save) {
		super(Width, Height, X, Y, Z, title);
		this.switchSave = save;
		this.titlesave = title;
		this.title = titlesave + " : " + switchSave;
	}

	@Override
	void action() {
		this.switchSave = !this.switchSave;
		this.title = titlesave + " : " + switchSave;
		
	}
	@Override
	void refresh() {
		// TODO Auto-generated method stub
		
	}
	public boolean getvalSave() {
		return switchSave;
	}
	

}
