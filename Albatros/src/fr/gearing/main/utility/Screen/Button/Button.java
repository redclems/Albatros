package fr.gearing.main.utility.Screen.Button;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Mousse;
import fr.gearing.main.utility.Write;
import fr.gearing.main.utility.Screen.ScreenElement;

public abstract class Button extends ScreenElement{
	
	
	protected boolean hover;
	protected boolean bind;
	protected boolean bindView;
	protected float fontSize;
	protected String title;
	
	abstract void action();//it is action when you click on the button
	
	abstract void refresh();//it is action when you click on the button


	public Button(int Width, int Height, int X, int Y, float Z, String title) {
		
		//affichage
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		this.title = title;
	}
	public Button(float Width, float Height, float X, float Y, float Z, String title) {
		
		//affichage
		this.x = X;
		this.y = Y;
		this.z = Z;
		this.width = Width;
		this.height = Height;
		this.title = title;
	}
	
	public Button(float Width, float Height, float X, float Y, int Z, String title, boolean bind, boolean bindView) {
		
		//affichage
		this.x = X;
		this.y = Y;
		this.z = Z;
		this.width = Width;
		this.height = Height;
		this.title = title;
		this.bind = bind;
		this.bindView = bindView;
	}
	public Button(int Width, int Height, int X, int Y, int Z, String title, boolean bind, boolean bindView) {
		
		//affichage
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		this.title = title;
		this.bind = bind;
		this.bindView = bindView;
	}
	
	public void Hover() {
		hover = false;
		if(Mousse.MousseHover(width, height, x, y, z)) {
			Texture.fon.bind();
			LoaderMenus.setTrans2D(width, height, x, y, z+0.006f);
			hover = true;
		}
	}
	public void click() {
		if(hover) {
			if(Mousse.mousse_etat) action();
		}
	}
	
	public void switchBind() {
		bind = !bind;
	}
	public boolean getBind() {
		return bind;
	}
	
	public int NBletterMax(float fontSize) {
		int res = (int) (width/fontSize);

		return res;
	}
	
	public String tooBig() {
		String newtitle = new String(title);
		char[] name_save = newtitle.toCharArray();
		newtitle = "";
		for(int i2 = 0; i2 < NBletterMax(fontSize)-1; i2++) {
			newtitle = newtitle + name_save[i2];
		}
		newtitle = newtitle + '.' + '.' + '.';
		return newtitle;
	}

	@Override
	public void Display() {
		
		Texture.bpM.bind();
		LoaderMenus.setTrans2D(width, height, x, y, z);
		
		fontSize =height/2;
		String titre = new String(title);
		if(title.length() > NBletterMax(fontSize))titre = tooBig();
		Write.write(titre, 2, fontSize, x, y, z+0.003f, true);
		
		if(bind && bindView) {
			Texture.fon2.bind();
			LoaderMenus.setTrans2D(width, height, x, y, z+0.006f);
		}else if(!bind) {
			this.Hover();
			this.click();
		}
		refresh();
		
	}

}
