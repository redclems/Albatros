package fr.gearing.main.utility.Screen.Button;

import fr.gearing.main.utility.Screen.Scroll.ScrollGame;

public class ButtonCaseGame extends Button{
	
	protected String action;
	protected ScrollGame scroll;

	public ButtonCaseGame(float Width, float Height, float X, float Y, float Z, String title, String action, ScrollGame scroll) {
		super(Width, Height, X, Y, Z, title);
		this.action = action;
		this.scroll = scroll;

	}
	public ButtonCaseGame(int Width, int Height, int X, int Y, int Z, String title, String action, ScrollGame scroll, boolean bind, boolean bindView) {
		super(Width, Height, X, Y, Z, title, bind, bindView);
		this.action = action;
		this.scroll = scroll;
	}

	@Override
	void action() {
		scroll.action(action);
	}
	@Override
	void refresh() {
		if( scroll.getButtonBind())bind = true;
		if(!scroll.getButtonBind())bind = false;
	}
	

}
