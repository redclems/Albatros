package fr.gearing.main.utility.Screen.Button;

import java.util.List;

public class ButtonServeralSwicth extends Button{

	protected int valMin, valMax, val;
	protected boolean switchSave;
	protected String titleSave;
	protected List<String> list;

	public ButtonServeralSwicth(int Width, int Height, int X, int Y, int Z, List<String> list, boolean bind, boolean bindView, int val_min, int val_max,int val, String title) {
		super(Width, Height, X, Y, Z, title, bind, bindView);
		this.valMin = val_min;
		this.val = val;
		this.valMax = val_max;
		this.list = list;
		this.titleSave = title;
		this.title = titleSave + ":"+ list.get(val);

	}
	public ButtonServeralSwicth(int Width, int Height, int X, int Y, int Z, List<String> list, int val_min, int val_max, int val, String title) {
		super(Width, Height, X, Y, Z, title);
		this.valMin = val_min;
		this.val = val;
		this.valMax = val_max;
		this.list = list;
		this.titleSave = title;
		this.title = titleSave + ":"+ list.get(val);

	}

	@Override
	void action() {
		this.val++;
		if(this.val > this.valMax) this.val = this.valMin;
		this.title = titleSave + ":"+ list.get(val);
		
	}
	@Override
	void refresh() {
		// TODO Auto-generated method stub
		
	}
	public int getvalSave() {
		return val;
	}

}
