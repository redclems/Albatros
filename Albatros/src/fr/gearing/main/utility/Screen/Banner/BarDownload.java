package fr.gearing.main.utility.Screen.Banner;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.IntForString;
import fr.gearing.main.utility.Write;
import fr.gearing.main.utility.Screen.ScreenElement;

public class BarDownload extends ScreenElement{
	

	protected int val;
	protected int valMax;
	protected int donwload;
	protected int color;
	protected String Name;
	
	protected boolean displayName;
	protected boolean below;

	public BarDownload(int Width, int Height, int X, int Y, int Z, int val, int valMax,int color, String Name, boolean displayName, boolean below) {
		/*
		 * this function create bar to download the parameter download is percentage of charge and
		 *  the color is int 
		 *  green = 1 / red = 2 / yellow = 3 / blue = 4 / brown = 5
		 * 		
		 */
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		
		this.color = color;
		this.Name = Name;
		this.valMax = valMax;
		this.val = val;
		
		this.displayName = displayName;
		this.below = below;
		
		update();
		
	}
	public BarDownload(int Width, int Height, int X, int Y, int Z, int val, int valMax,int color, String Name) {
		/*
		 * this function create bar to download the parameter download is percentage of charge and
		 *  the color is int 
		 *  green = 1 / red = 2 / yellow = 3 / blue = 4 / brown = 5
		 * 		
		 */
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		
		this.color = color;
		this.Name = Name;
		this.valMax = valMax;
		this.val = val;
		
		update();
		
	}

	@Override
	public void Display() {
		if(donwload < 0 || donwload > 100) {
			System.out.println("there are problems with the BarDownload is expend");
			donwload = 100;
		}
		
		float ep = (height + width)/200;
		
		Texture.contBAR.bind();
		LoaderMenus.setTrans2D(width, ep, x, y+height/2-ep/2, z);
		LoaderMenus.setTrans2D(width, ep, x, y-height/2+ep/2, z);
			
		LoaderMenus.setTrans2D(ep, height, x+width/2-ep/2, y, z);
		LoaderMenus.setTrans2D(ep, height, x-width/2+ep/2, y, z);
		
		if(color == 1) {
			Texture.BARloadGreen.bind();
		}else if(color == 2) {
			Texture.BARloadRED.bind();
		}else if(color == 3) {		
			Texture.BARloadYellow.bind();
		}else if(color == 4) {
			Texture.BARloadBlue.bind();
		}else if(color == 5) {
			Texture.BARloadBrown.bind();
		}
		
		float p_donwload = ((width-ep*2)/100)* donwload;
		
		LoaderMenus.setTrans2D(p_donwload, height-ep*2, x - width/2 + p_donwload/2, y, z);
		
		if(displayName) {
			String percentage = IntForString.StringInt(donwload);
			if(below) {
				Write.write("donwload:" + percentage + "%", 2, height*0.70f, x, y - height, z+0.03f, true);
			}else {
				Write.write("donwload:" + percentage + "%", 2, height*0.70f, x, y, z+0.03f, true);
			}
		}
	}
	
	public void addVal(int val) { 
		this.val+=val;
		update();
	}
	public void setVal(int val) {
		this.val=val;
		update();
	}
	public int getVal() {
		return val;
	}
	
	private void update() {
		donwload = GameUtility.echelleToOther(val, 0, valMax, 0, 100);


	}
	
	

}