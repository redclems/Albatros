package fr.gearing.main.utility.Screen.Banner;

import fr.gearing.main.game.GameUtility;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;
import fr.gearing.main.utility.Screen.ScreenElement;

public class Banner  extends ScreenElement{
	
	Texture texture;

	public Banner(Texture texture, int Width, int Height, int X, int Y, int Z) {
		this.x = GameUtility.change_percentage_by_Co_X(X, Z);
		this.y = GameUtility.change_percentage_by_Co_Y(Y, Z);
		this.z = Z;
		this.width = GameUtility.change_percentage_by_width(Width, Z);
		this.height = GameUtility.change_percentage_by_height(Height, Z);
		this.texture = texture;
		
	}

	@Override
	public void Display() {
		texture.bind();
		
		LoaderMenus.setTrans2D(width, height, x, y, z);
		
	}

}
