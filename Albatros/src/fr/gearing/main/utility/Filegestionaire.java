package fr.gearing.main.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Filegestionaire {
	
	public static boolean copier(File source, File dest) { 
	    try (InputStream sourceFile = new java.io.FileInputStream(source);  
	            OutputStream destinationFile = new FileOutputStream(dest)) {  
	        byte buffer[] = new byte[512 * 1024]; 
	        int nbLecture; 
	        while ((nbLecture = sourceFile.read(buffer)) != -1){ 
	            destinationFile.write(buffer, 0, nbLecture); 
	        } 
	    } catch (IOException e){ 
	        e.printStackTrace(); 
	        return false; // Erreur 
	    } 
	    return true; // Résultat OK   
	}
	
	public static void delete(File path) { 
	try { 
		if (!path.exists()) throw new IOException("File not found '" + path.getAbsolutePath() + "'"); 
		if (path.isDirectory()) { 
			File[] children = path.listFiles(); 
			for (int i=0; children != null && i<children.length; i++) delete(children[i]); 
			if (!path.delete()) throw new IOException("No delete path '" + path.getAbsolutePath() + "'"); 
		}else if (!path.delete()) throw new IOException("No delete file '" + path.getAbsolutePath() + "'"); 

	} catch (IOException e) {  
		e.printStackTrace(); 
	} 
	}
}
