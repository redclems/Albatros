package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.CubeT;
import fr.gearing.main.Structure.Block.Cylindre2;
import fr.gearing.main.Structure.Block.Cylindre3;
import fr.gearing.main.Structure.Block.Cylindre4;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class ManaPomp extends Structure{

	private Cylindre4 cyl1;
	private Cylindre3 cyl2;
	private Cylindre3 cyl3;
	private Cylindre4 cyl4;
	private Cylindre3 cyl5;
	
	private List<CubeT> pillar;
	
	private Cylindre2 pump1;
	private Cylindre2 pump2;
	private Cylindre2 pump3;

	
	private float T = save.tstructure;
	
	public ManaPomp(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T;
		
		//housse
		cyl1 = new Cylindre4(T/4, T*2, T/4, x + (((T/4)*1.16f))*2, z, y+T/4, 2.36f);
		cyl2 = new Cylindre3(T/4, T*2, T/4, x - (((T/4)*1.16f))*2, z, y+T/4, 2.36f);
		cyl3 = new Cylindre3(T/2, T, T/2, x - (((T/4)*2.225f))*2, z-T/4+T/4, y, 4.45f);
		cyl4 = new Cylindre4(T/4, T*2, T/4, x + (((T/4)*1.16f))*2, z, y-T/4, 2.36f);
		cyl5 = new Cylindre3(T/4, T*2, T/4, x - (((T/4)*1.16f))*2, z, y-T/4, 2.36f);
		
		pillar = new ArrayList<>();
		pillar.add(new CubeT(T/2, T/2, T, x + ((((T/4)*4.225f))*2+T/1.25f)/2 - T/4, y, z+T/2+T/2, 10));
		pillar.add(new CubeT(T/2, T/2, T, x + ((((T/4)*4.225f))*2+T/1.25f)/2 - T/4, y, z+T/2-T/2, 11));
		pillar.add(new CubeT(T/2, T/2, T, x + ((((T/4)*4.225f))*2+T/1.25f)/2 + T/4, y, z+T/2+T/2, 9));
		pillar.add(new CubeT(T/2, T/2, T, x + ((((T/4)*4.225f))*2+T/1.25f)/2 + T/4, y, z+T/2-T/2, 12));

		pump1 = new Cylindre2(T/2, T*0.90f, T/2, x- (((T/4)*4.225f))*2+T, z-T/2, y);
		pump2 = new Cylindre2(T/1.5f, T/8, T/1.5f, x- (((T/4)*4.225f))*2+T, z-T+T/16, y);
		pump3 = new Cylindre2(T/1.5f, T/8, T/1.5f, x- (((T/4)*4.225f))*2+T, z-T/16, y);
	}
	@Override
	public void display() {
		Texture.metal.bind();
		for(int pill = 0; pill<pillar.size(); pill++) {
			pillar.get(pill).display();
		}
		
		cyl1.display();
		cyl2.display();
		cyl3.display();
		cyl4.display();
		cyl5.display();
		
		Texture.pumpArgon.bind();
		pump1.display();
		
		Texture.bottowerstone.bind();
		pump2.display();
		pump3.display();
	}
		
}
