package fr.gearing.main.Structure;

public abstract class Structure {
	
	protected int durabilityMax;
	protected int durability;
	protected int level;
	
	protected boolean rotation;
	protected byte turn;
	
	protected float width;
	protected float height;
	protected float x;
	protected float y;
	protected float z;
	
	abstract void display();
	abstract void initialize();

	protected void damage(int nbDamge){
		//add damage to the structure
		durability-= nbDamge;
	}
	protected void Repair(){
		//repair the structure
		durability = this.durabilityMax;
	}
	
	protected void levelUp(){
		//add level
		this.level++;
	}
}