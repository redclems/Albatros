package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cone2;
import fr.gearing.main.Structure.Block.Cylindre2;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class TowerMage extends Structure{


	
	private List<Cylindre2> tower;
	private Cone2 toit;
	

	
	private float T = save.tstructure;
	
	public TowerMage(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/2;
		
		//housse
		toit = new Cone2(T*2.5f, T*2, T*2.5f, x, z+T*3.5f, y);

		
		tower = new ArrayList<>();
		tower.add(new Cylindre2(T*2, T, T*2, x, z, y));
		tower.add(new Cylindre2(T*2, T, T*2, x, z+T, y));
		tower.add(new Cylindre2(T*2, T, T*2, x, z+T*2, y));

		
		

	}
	@Override
	public void display() {

		Texture.rooftemple.bind();
		toit.display();
		
		Texture.mageTower1.bind();
		for(int tow = 0; tow < tower.size(); tow++) {
			tower.get(tow).display();
		}

	}
		
}
