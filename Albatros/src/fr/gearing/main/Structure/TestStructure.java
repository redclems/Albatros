package fr.gearing.main.Structure;


import fr.gearing.main.game.save;
import fr.gearing.main.render.LoaderMenus;
import fr.gearing.main.render.Texture;


public class TestStructure {
	
	float v = 2;
	float i = -5;
	
	Tower tower1, tower2, tower3, tower4;
	Wall wall1, wall2, wall3, wall4, wall5, wall6, wall7, wall8;
	Door door1, door2, door3, door4;
	Gate door;
	Temple temple;
	LumberJackHousse lumberjackHousse;
	MilitaryArea militaryArea;
	Mine mine;
	BuildHousse buildHousse;
	Farms farms;
	Camps camps;
	Forge forge;
	ManaPomp manapomp;
	TowerMage TowerMage;
	Zeplin zeplin;
	Catapult catapult;
	Ballist Ballist;
	
	public TestStructure() {
		tower1 = new Tower(v, 1, i, 3);
		tower2 = new Tower(v+(save.tchunk/4)*4, 1, i, 4);
	 	tower3 = new Tower(v, 1+(save.tchunk/4)*4, i, 4);
		tower4 = new Tower(v+(save.tchunk/4)*4, 1+(save.tchunk/4)*4, i, 4);
		
		wall1 = new Wall(v+(save.tchunk/4)*1, 1, i, 1, (byte)1);
		door1 = new Door(v+(save.tchunk/4)*2, 1, i, 1, (byte)1);
		wall2 = new Wall(v+(save.tchunk/4)*3, 1, i, 1, (byte)1);
		
	 
		wall3 = new Wall(v, 1+(save.tchunk/4)*1, i, 1, (byte)2);
		door2 = new Door(v, 1+(save.tchunk/4)*2, i, 1, (byte)2);
		wall4 = new Wall(v, 1+(save.tchunk/4)*3, i, 1, (byte)2);
	 

		wall5 = new Wall(v+(save.tchunk/4)*1, 1+(save.tchunk/4)*4, i, 1, (byte)3);
		door3 = new Door(v+(save.tchunk/4)*2, 1+(save.tchunk/4)*4, i, 1, (byte)3);
		wall6 = new Wall(v+(save.tchunk/4)*3, 1+(save.tchunk/4)*4, i, 1, (byte)3);
		
		
		wall7 = new Wall(v+(save.tchunk/4)*4, 1+(save.tchunk/4)*1, i, 1, (byte)4);
		door4 = new Door(v+(save.tchunk/4)*4, 1+(save.tchunk/4)*2, i, 1, (byte)4);
		wall8 = new Wall(v+(save.tchunk/4)*4, 1+(save.tchunk/4)*3, i, 1, (byte)4);

		v = v-(save.tchunk/4)*10;
		door = new Gate(v, 0, i, 4);
		v = v-(save.tchunk/4)*16;
		temple = new Temple(v, 0, i, 4);
		v = v-(save.tchunk/4)*10;
		lumberjackHousse = new LumberJackHousse(v, 0, i, 4);
		v = v-(save.tchunk/4)*6;
		militaryArea = new MilitaryArea(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		mine = new Mine(v, 0, i, 4);
		v = v-(save.tchunk/4)*8;
		buildHousse = new BuildHousse(v, 0, i, 4);
		v = v-(save.tchunk/4)*5;
		farms = new Farms(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		camps = new Camps(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		forge= new Forge(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		manapomp = new ManaPomp(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		TowerMage = new TowerMage(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		zeplin = new Zeplin(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		catapult = new Catapult(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
		Ballist = new Ballist(v, 0, i, 4);
		v = v-(save.tchunk/4)*4;
	}
	
	public void display() {
		Texture.floor.bind();
		LoaderMenus.setTrans2D(100, 100, 0, 0, -5);
		
		tower1.display();
		tower2.display();
		tower3.display();
		tower4.display();
		wall1.display();
		wall2.display();
		wall3.display();
		wall4.display();
		wall5.display();
		wall6.display();
		wall7.display();
		wall8.display();
		door1.display();
		door2.display();
		door3.display();
		door4.display();
		door.display();
		temple.display();
		lumberjackHousse.display();
		militaryArea.display();
		mine.display();
		buildHousse.display();
		farms.display();
		camps.display();
		forge.display();
		manapomp.display();
		TowerMage.display();
		zeplin.display();
		catapult.display();
		Ballist.display();
	}
}
