package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Cylindre;
import fr.gearing.main.Structure.Block.Cylindre3;
import fr.gearing.main.Structure.Block.Cylindre4;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Ballist extends Structure{

	private List<Cylindre3> arrow;
	private List<Cylindre> structure;
	private List<Cube> structure2;
	
	Cylindre4 arch1;
	Cylindre3 arch2;
	Cylindre3 arch3;
	Cylindre4 arch4;
	Cylindre3 arch5;
	

	
	private float T = save.tstructure;
	
	public Ballist(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T*1.25f;
		
		//housse
		 arch1 = new Cylindre4(T/4, T*2, T/4, x + (((T/4)*1.16f))*2, z, y+T/4, 2.36f);
		 arch2 = new Cylindre3(T/4, T*2, T/4, x - (((T/4)*1.16f))*2, z, y+T/4, 2.36f);
		 arch3 = new Cylindre3(T/2, T/2, T/2, x - (((T/4)*2.225f))*2, z+T/2-T/1.5f, y, 2.225f);
		 arch4 = new Cylindre4(T/4, T*2, T/4, x + (((T/4)*1.16f))*2, z, y-T/4, 2.36f);
		 arch5 = new Cylindre3(T/4, T*2, T/4, x - (((T/4)*1.16f))*2, z, y-T/4, 2.36f);
		
		 arrow = new ArrayList<>();
		 arrow.add(new Cylindre3(T/8, T/8, T/8, x - (((T/4)*2.225f))-T/2, z+T/2-T/1.5f+T/4-T/4, y, 2.225f));
		 arrow.add(new Cylindre3(T/8, T/8, T/8, x - (((T/4)*2.225f))-T/4, z+T/2-T/1.5f+T/4-T/8, y, 2.225f));
		 arrow.add(new Cylindre3(T/8, T/8, T/8, x - (((T/4)*2.225f)), z+T/2-T/1.5f+T/4, y, 2.225f));
		 arrow.add(new Cylindre3(T/8, T/8, T/8, x - (((T/4)*2.225f))+T/4, z+T/2-T/1.5f+T/4+T/8, y, 2.225f));
		 arrow.add(new Cylindre3(T/8, T/8, T/8, x - (((T/4)*2.225f))+(T/4)*2, z+T/2-T/1.5f+T/4+(T/8)*2, y, 2.225f));
		 arrow.add(new Cylindre3(T/8, T/8, T/8, x - (((T/4)*2.225f))+(T/4)*3, z+T/2-T/1.5f+T/4+(T/8)*3, y, 2.225f));

		 structure = new ArrayList<>();
		 structure.add(new Cylindre(T/3, T/3, T/3, x + (((T/4)*1.16f))*2, y+T/2+T/4, z-T));
		 structure.add(new Cylindre(T/3, T/3, T/3, x - (((T/4)*1.16f))*2, y+T/2+T/4, z-T));
		 structure.add(new Cylindre(T/3, T/3, T/3, x + (((T/4)*1.16f))*2, y-T/2-T/4, z-T));
		 structure.add(new Cylindre(T/3, T/3, T/3, x - (((T/4)*1.16f))*2, y-T/2-T/4, z-T));
		
		 structure2 = new ArrayList<>();
		 structure2.add(new Cube(T/5, T*2, T/5, x, y, z+T/4));
		 structure2.add(new Cube(T/4, T, T/4, x, y, z-T));
		 structure2.add(new Cube(T*2, T/4, T/4, x, y+T/2, z-T));
		 structure2.add(new Cube(T*2, T/4, T/4, x, y-T/2, z-T));
	}
	@Override
	public void display() {

		Texture.woodstick.bind();
		arch1.display();
		arch2.display();
		arch3.display();
		arch4.display();
		arch5.display();
		
		for(int tree = 0; tree < structure2.size(); tree++) {
			structure2.get(tree).display();
		}
		
		Texture.arrow.bind();
		for(int tree = 0; tree < arrow.size(); tree++) {
			arrow.get(tree).display();
		}
		
		Texture.metal.bind();
		for(int tree = 0; tree < structure.size(); tree++) {
			structure.get(tree).display();
		}

		
	}
		
}
