package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.CubeT;
import fr.gearing.main.Structure.Block.Paint2DIn3d;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Mine extends Structure{
	private CubeT mine;
	private Paint2DIn3d textureMine;
	
	private List<Gravel> tat;
	private List<Cube> beam;

	

	
	private float T = save.tstructure;
	
	public Mine(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/2;
		
		//housse
		mine = new CubeT(T, T*1.25f, T, x+T/2, y+T/2, z, 5);
		textureMine = new Paint2DIn3d(T-T/6, T*1.25f/7, T-T/6, x+T/2, y-T/7, z-T/12);
		
		beam = new ArrayList<>();
		beam.add(new Cube(T/7, T/7, T-T/7, x+T-T/14, y-T/7, z-T/14));
		beam.add(new Cube(T/7, T/7, T-T/7, x+T/4-T/7, y-T/7, z-T/14));
		beam.add(new Cube(T*1.3f, T/7, T/7, x+T/2, y-T/7, z-T/14+T/2-T/14));
		
		tat = new ArrayList<>();
		tat.add(new Gravel(x-T/2-(T*1.25f)/2, y+T/2, z));
		tat.add(new Gravel(x-T/2-(T*1.25f)/2, y-T/2, z));
		tat.add(new Gravel(x+T/2-(T*1.25f)/2, y-T/2-T/4, z));


		
		

	}
	@Override
	public void display() {

		Texture.gravel.bind();
		mine.display();

		Texture.Mine.bind();
		textureMine.display();
	
		Texture.woodstick.bind();
		for(int tree = 0; tree < beam.size(); tree++) {
			beam.get(tree).display();
		}
		

		Texture.gravel.bind();
		int stop = 0;
		if(this.level > 1 ) {
			stop+=1;
		}
		if(this.level > 2 ) {
			stop+=1;
		}
		if(this.level > 3 ) {
			stop+=1;
		}
		
		for(int tree = 0; tree < stop; tree++) {
			tat.get(tree).display();
		}

	}
}
