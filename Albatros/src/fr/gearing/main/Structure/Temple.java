package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Cylindre2;
import fr.gearing.main.Structure.Block.roof1;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Temple extends Structure{
	private List<Cube> stair;
	private List<Cylindre2> pillar;
	private Cube foundation;
	
	private Cube roof;
	private roof1 roof1;
	private roof1 roof2;
	
	private float T = save.tstructure;
	
	public Temple(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/2;
		
		foundation = new Cube(T*8, T*6, T, x, y, z);
		
		this.stair = new ArrayList<>(); 
		
		//stair
		this.stair.add(new Cube(T*1.5f, T*2, T/5, x+T*4+(T*1.5f)/2, y, z-T/2+T/5-T/10));
		this.stair.add(new Cube(T*1.5f-T/5*1, T*2, T/5, x+T*4+(T*1.5f)/2-(T/5)*1, y, z-T/2+(T/5)*2-T/10));
		this.stair.add(new Cube(T*1.5f-T/5*2, T*2, T/5, x+T*4+(T*1.5f)/2-(T/5)*2, y, z-T/2+(T/5)*3-T/10));
		this.stair.add(new Cube(T*1.5f-T/5*3, T*2, T/5, x+T*4+(T*1.5f)/2-(T/5)*3, y, z-T/2+(T/5)*4-T/10));
		this.stair.add(new Cube(T*1.5f-T/5*4, T*2, T/5, x+T*4+(T*1.5f)/2-(T/5)*4, y, z-T/2+(T/5)*5-T/10));
		
		//pillar
		this.pillar = new ArrayList<>(); 
		this.pillar.add(new Cylindre2(T, T*4, T, x+T*4-T/2, z+T*2+T/2, y+T*3-T/2-(T*0.75f)*2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2, z+T*2+T/2, y+T*3-T/2-(T*0.75f)*2));
		this.pillar.add(new Cylindre2(T, T*4, T, x+T*4-T/2, z+T*2+T/2, y+T*3-T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2, z+T*2+T/2, y+T*3-T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2+(T*0.85f)*2, z+T*2+T/2, y+T*3-T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2+(T*0.85f)*4, z+T*2+T/2, y+T*3-T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2+(T*0.85f)*6, z+T*2+T/2, y+T*3-T/2));
		
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2+(T*0.85f)*6, z+T*2+T/2, y-T*3+T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2+(T*0.85f)*4, z+T*2+T/2, y-T*3+T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2+(T*0.85f)*2, z+T*2+T/2, y-T*3+T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2, z+T*2+T/2, y-T*3+T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x+T*4-T/2, z+T*2+T/2, y-T*3+T/2));
		this.pillar.add(new Cylindre2(T, T*4, T, x+T*4-T/2, z+T*2+T/2, y-T*3+T/2+(T*0.75f)*2));
		this.pillar.add(new Cylindre2(T, T*4, T, x-T*4+T/2, z+T*2+T/2, y-T*3+T/2+(T*0.75f)*2));
		
		//roof
		roof = new Cube(T*8, T*6, T/2, x, y, z+T*4+T/1.5f);
		roof1 = new roof1(T*8.1f, T*6.1f, T*2, x, y, z+T*5.2f+T/1.5f);
		roof2 = new roof1(T*7.8f, T*6.1f, T*2, x, y, z+T*5.2f+T/1.1f);
	}
	@Override
	public void display() {
		
		
		Texture.marble.bind();
		foundation.display();
		for(int cube = 0; cube < stair.size(); cube++) {
			this.stair.get(cube).display();
		}
		for(int cube = 0; cube < pillar.size(); cube++) {
			this.pillar.get(cube).display();
		}
		roof.display();
		roof1.display();
		
		Texture.rooftemple.bind();
		roof2.display();

		
	}
		
	
}

