package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.CubeT;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Door extends Structure{
	
	
	//wall
	private Cube pillar1;
	private Cube pillar2;
	private CubeT angle1;
	private CubeT angle2;
	private Cube cubeTop;
	
	
	//niche
	private List<Cube> niche;
	
	//door
	private Cube botDoor;
	private CubeT topdoor1;
	private CubeT topdoor2;
	
	private float T = save.tstructure;
	
	public Door(float x, float y, float z, int level, byte turn) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dDoor;
		this.turn = turn;
		this.rotation = true;
		initialize();
		
	}
	@Override
	public void initialize() {
		if(this.turn == 1) {
			initialize1();
		}else if(this.turn == 2) {
			initialize2();
		}else if(this.turn == 3) {
			initialize3();
		}else if(this.turn == 4) {
			initialize4();
		}
		
	}
	private void initialize1() {
		float z = this.z+T/2;
		
		

		//wall
		pillar1 = new Cube(T/4, T/2, T, x+T/2.7f, y+T/2-T/4, z);
		pillar2 = new Cube(T/4, T/2, T, x-T/2.7f, y+T/2-T/4, z);
		
		angle1 = new CubeT(T/4, T/2, T/4, x+T/8, y+T/2-T/4, z+T/2.7f, 11);
		angle2 = new CubeT(T/4, T/2, T/4, x-T/8, y+T/2-T/4, z+T/2.7f, 12);
		
		cubeTop = new Cube(T, T/2, T, x, y+T/2-T/4, z+T);
		
		//door
		topdoor1 = new CubeT(T/4, T/4, T/4, x+T/8, y+T/2-T/4, z+T/2.7f, 9);
		topdoor2 = new CubeT(T/4, T/4, T/4, x-T/8, y+T/2-T/4, z+T/2.7f, 10);
		
		botDoor = new Cube(T/2, T/4, T-T/5, x, y+T/2-T/4, z-T/10);
		
		//niche
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y+T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y+T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*3-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
	}
	
	private void initialize2() {
		float z = this.z+T/2;


		//wall
		pillar1 = new Cube(T/2, T/4, T, x+T/2-T/4, y+T/2.7f, z);
		pillar2 = new Cube(T/2, T/4, T, x+T/2-T/4, y-T/2.7f, z);
		
		angle1 = new CubeT(T/2, T/4, T/4, x+T/2-T/4, y+T/8, z+T/2.7f, 7);
		angle2 = new CubeT(T/2, T/4, T/4, x+T/2-T/4, y-T/8, z+T/2.7f, 6);
		
		cubeTop = new Cube(T/2, T, T, x+T/2-T/4, y, z+T);
		
		//door
		topdoor1 = new CubeT(T/4, T/4, T/4, x+T/2-T/4, y+T/8, z+T/2.7f, 5);
		topdoor2 = new CubeT(T/4, T/4, T/4, x+T/2-T/4, y-T/8, z+T/2.7f, 8);
		
		botDoor = new Cube(T/4, T/2, T-T/5, x+T/2-T/4, y, z-T/10);
		
		//niche
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T-T/2), z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T*3-T/2), z+(((T*4)*1.5f)+T/2)));
	}
	private void initialize3() {
		float z = this.z+T/2;
		

		//wall
		pillar1 = new Cube(T/4, T/2, T, x-T/2.7f, y-T/2+T/4, z);
		pillar2 = new Cube(T/4, T/2, T, x+T/2.7f, y-T/2+T/4, z);
		
		angle1 = new CubeT(T/4, T/2, T/4, x+T/8, y-T/2+T/4, z+T/2.7f, 11);
		angle2 = new CubeT(T/4, T/2, T/4, x-T/8, y-T/2+T/4, z+T/2.7f, 12);
		
		cubeTop = new Cube(T, T/2, T, x, y-T/2+T/4, z+T);
		
		//door
		topdoor1 = new CubeT(T/4, T/4, T/4, x+T/8, y-T/2+T/4, z+T/2.7f, 9);
		topdoor2 = new CubeT(T/4, T/4, T/4, x-T/8, y-T/2+T/4, z+T/2.7f, 10);
		
		botDoor = new Cube(T/2, T/4, T-T/5, x, y-T/2+T/4, z-T/10);
		
		//niche
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y-T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y-T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*3-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
	}
	private void initialize4() {
		float z = this.z+T/2;
	
		
		//wall
		pillar1 = new Cube(T/2, T/4, T, x-T/2+T/4, y+T/2.7f, z);
		pillar2 = new Cube(T/2, T/4, T, x-T/2+T/4, y-T/2.7f, z);
		
		angle1 = new CubeT(T/2, T/4, T/4, x-T/2+T/4, y+T/8, z+T/2.7f, 7);
		angle2 = new CubeT(T/2, T/4, T/4, x-T/2+T/4, y-T/8, z+T/2.7f, 6);
		
		cubeTop = new Cube(T/2, T, T, x-T/2+T/4, y, z+T);
		
		//door
		topdoor1 = new CubeT(T/4, T/4, T/4, x-T/2+T/4, y+T/8, z+T/2.7f, 5);
		topdoor2 = new CubeT(T/4, T/4, T/4, x-T/2+T/4, y-T/8, z+T/2.7f, 8);
		
		botDoor = new Cube(T/4, T/2, T-T/5, x-T/2+T/4, y, z-T/10);
		
		//niche
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T-T/2), z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T*3-T/2), z+(((T*4)*1.5f)+T/2)));
	}
	
	@Override
	public void display() {
		if(this.level == 1) {
			
			Texture.bottowerwood.bind();
		}else if(this.level == 2) {
			
			Texture.bottowerstone.bind();
		}else if(this.level == 3) {
			
			Texture.bottoweriron.bind();
		}else if(this.level == 4) {
			
			Texture.bottowerobs.bind();
		}
		pillar1.display();
		pillar2.display();
		angle1.display();
		angle2.display();
		cubeTop.display();

		for(int cube = 0; cube < niche.size(); cube++) {
			niche.get(cube).display();
		}
		Texture.doorwood.bind();
		botDoor.display();
		topdoor1.display();
		topdoor2.display();
	}
		
}
