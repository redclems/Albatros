package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Cylindre;
import fr.gearing.main.Structure.Block.Cylindre2;
import fr.gearing.main.Structure.Block.Paint2DIn3d;
import fr.gearing.main.Structure.Block.roof1;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class LumberJackHousse extends Structure{
	private Cube maison;
	private roof1 toit1;
	private roof1 toit2;
	private Paint2DIn3d textureHousse;
	
	private List<Cylindre> log;
	private Cylindre2 log2;
	

	
	private float T = save.tstructure;
	
	public LumberJackHousse(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/2;
		
		//housse
		maison = new Cube(T*1.25f, T, T, x+T/2, y+T/2, z);
		toit1 = new roof1(T*1.28f, T*1.1f, T/2, x+T/2, y+T/2, z+T/1.5f);
		toit2 = new roof1(T*1.27f, T*1.1f, T/2, x+T/2, y+T/2, z+T/1.3f);;
		textureHousse = new Paint2DIn3d(T*1.26f, T*1.01f, T*1.01f, x+T/2, y+T/2, z);
		
		log = new ArrayList<>();
		log.add(new Cylindre(T/4, T, T/4, x-T/2, y+T/2, z-T/2+(T/4)/2));
		
		log.add(new Cylindre(T/4, T, T/4, x-T/2-T/4, y+T/2, z-T/2+(T/4)/2));
		log.add(new Cylindre(T/4, T, T/4, x-T/2-(T/4)*2, y+T/2, z-T/2+(T/4)/2));
		
		log.add(new Cylindre(T/4, T, T/4, x-T/2-(T/4)*3, y+T/2, z-T/2+(T/4)/2));
		log.add(new Cylindre(T/4, T, T/4, x-T/2-(T/4)*0.5f, y+T/2, z-T/2+(T/3.0f)));
		
		log.add(new Cylindre(T/4, T, T/4, x-T/2-(T/4)*1.5f, y+T/2, z-T/2+(T/3.0f)));

		
		log2 = new Cylindre2(T/4, T/4, T/4, x+T/2, z-T/2+(T/4)/2, y-T/2);
		
		

	}
	@Override
	public void display() {

		Texture.wallwood.bind();
		maison.display();
		toit1.display();
		
		Texture.rooftemple.bind();
		toit2.display();
		
		Texture.lumberjackHousse.bind();
		textureHousse.display();
		
		Texture.trees.bind();
		int stop = 1;
		if(this.level > 1 ) {
			stop+=2;
		}
		if(this.level > 2 ) {
			stop+=2;
		}
		if(this.level > 3 ) {
			stop+=1;
		}
		
		for(int tree = 0; tree < stop; tree++) {
			log.get(tree).display();
		}
		log2.display();
	}
		
}
