package fr.gearing.main.Structure.Block;


import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import org.lwjgl.opengl.GL11;

public class Cone {
	private float X;
	private float X1;
	private float X2;
	private float X3;
	private float X4;
	
	private float Y1;
	private float Y2;
	
	private float Z;
	private float Z1;
	private float Z2;
	private float Z3;
	private float Z4;
	
	public Cone(float Width, float Height, float Dephth, float X, float Y, float Z) {
		
		this.setX(X);
		this.setX1(X + (Width/2));
		this.setX2(X + (Width/4));
		this.setX3(X - (Width/4));
		this.setX4(X - (Width/2));
		
		this.setY1(Y + (Height/2));
		this.setY2(Y - (Height/2));
		
		this.setZ(Z);
		this.setZ1(Z + (Dephth/2));
		this.setZ2(Z + (Dephth/4));
		this.setZ3(Z - (Dephth/4));
		this.setZ4(Z - (Dephth/2));
		
	}
	
	public void display(){
		Loader.initialization();
		
		//sol1
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X1, Z3, Y1);
				glTexCoord2f(0, 1);glVertex3f(X1, Z2, Y1);
				glEnd();

				
				//soll2
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X1, Z2, Y1);
				glTexCoord2f(0, 1);glVertex3f(X2, Z1, Y1);
				glEnd();
				
				
				//soll3
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X2, Z1, Y1);
				glTexCoord2f(0, 1);glVertex3f(X3, Z1, Y1);
				glEnd();
				
				
				//soll4
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X3, Z1, Y1);
				glTexCoord2f(0, 1);glVertex3f(X4, Z2, Y1);
				glEnd();
				

				
				//soll5
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X4, Z3, Y1);
				glTexCoord2f(0, 1);glVertex3f(X4, Z2, Y1);
				glEnd();
				

				
				//soll6
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X3, Z4, Y1);
				glTexCoord2f(0, 1);glVertex3f(X4, Z3, Y1);
				glEnd();
				
				
				//soll7
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X2, Z4, Y1);
				glTexCoord2f(0, 1);glVertex3f(X3, Z4, Y1);
				glEnd();
				

				
				//soll8
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X, Z, Y2);
				glTexCoord2f(1, 1);glVertex3f(X1, Z3, Y1);
				glTexCoord2f(0, 1);glVertex3f(X2, Z4, Y1);
				glEnd();
				
				
				//haut
				//t1
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X1, Z3, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X1, Z2, Y1);
				glEnd();
				
				//t2
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X1, Z2, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X2, Z1, Y1);
				glEnd();
				
				//t3
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X2, Z1, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X3, Z1, Y1);
				glEnd();
				
				//t4
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X3, Z1, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X4, Z2, Y1);
				glEnd();
				
				//t5
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X4, Z2, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X4, Z3, Y1);
				glEnd();
				
				//t6
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X4, Z3, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X3, Z4, Y1);
				glEnd();
				
				//t7
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X3, Z4, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X2, Z4, Y1);
				glEnd();
				
				//t8
				glBegin(GL11.GL_TRIANGLES);
				glTexCoord2f(0, 0);glVertex3f(X2, Z4, Y1);
				glTexCoord2f(1, 0);glVertex3f( X, Z, Y1);
				glTexCoord2f(1, 1);glVertex3f(X1, Z3, Y1);
				glEnd();
		
		Loader.close();
	}

	public float getX1() {
		return X1;
	}

	public void setX1(float x1) {
		X1 = x1;
	}

	public float getX2() {
		return X2;
	}

	public void setX2(float x2) {
		X2 = x2;
	}

	public float getY1() {
		return Y1;
	}

	public void setY1(float y1) {
		Y1 = y1;
	}

	public float getY2() {
		return Y2;
	}

	public void setY2(float y2) {
		Y2 = y2;
	}

	public float getZ1() {
		return Z1;
	}

	public void setZ1(float z1) {
		Z1 = z1;
	}

	public float getZ2() {
		return Z2;
	}

	public void setZ2(float z2) {
		Z2 = z2;
	}

	public float getX() {
		return X;
	}

	public void setX(float x) {
		X = x;
	}

	public float getX4() {
		return X4;
	}

	public void setX4(float x4) {
		X4 = x4;
	}

	public float getX3() {
		return X3;
	}

	public void setX3(float x3) {
		X3 = x3;
	}

	public float getZ3() {
		return Z3;
	}

	public void setZ3(float z3) {
		Z3 = z3;
	}

	public float getZ4() {
		return Z4;
	}

	public void setZ4(float z4) {
		Z4 = z4;
	}

	public float getZ() {
		return Z;
	}

	public void setZ(float z) {
		Z = z;
	}

}
