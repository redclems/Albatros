package fr.gearing.main.Structure.Block;

public class roof2 {
	private float width;
	private float height;
	private float dephth;
	
	private float x;
	private float x1;
	private float x2;
	private float y;
	private float z;
	
	private CubeT10 toit1;
	private CubeT9 toit2;
	
	public roof2(float Width, float Height, float Dephth, float X,float Y, float Z) {
		this.width = Width/2;
		this.height = Height;
		this.dephth = Dephth;
		this.x = X;
		this.y = Y;
		this.z = Z;
		
		renitialize();

	}
	
	public void display() {
		toit1.display();
		toit2.display();
	}
	
	private void renitialize() {
		this.x1 = x-this.width/2;
		this.x2 = x+this.width/2;
		toit1 = new CubeT10(this.width, this.height, this.dephth, this.x1, this.y, this.z);
		toit2 = new CubeT9(this.width, this.height, this.dephth, this.x2, this.y, this.z);

	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
		renitialize();
	}

	public float getheight() {
		return height;
	}

	public void setheight(float height) {
		this.height = height;
		renitialize();
	}

	public float getDephth() {
		return dephth;
	}

	public void setDephth(float dephth) {
		this.dephth = dephth;
		renitialize();
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
		renitialize();
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
		renitialize();
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
		renitialize();
	}
}	