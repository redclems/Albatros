package fr.gearing.main.Structure.Block;


import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import org.lwjgl.opengl.GL11;



public class Loader {
	//% selont le z formule

	
	public static void set_case(float P0x, float P1x, float P2x, float P0y, float P1y, float P2y, float P0z, float P1z, float P2z) {
		
		close();
	
		glBegin(GL11.GL_TRIANGLES);
		glTexCoord2f(0, 0);glVertex3f(P0x, P0y, P0z);
		glTexCoord2f(1, 0);glVertex3f(P1x, P1y, P1z);
		glTexCoord2f(1, 1);glVertex3f(P2x, P2y, P2z);
		glEnd();
		
		initialization();

	}



public static void initialization() {
	// TODO Auto-generated method stub
	GL11.glMatrixMode(GL11.GL_TEXTURE);
	GL11.glEnable(GL11.GL_TEXTURE_2D);
	GL11.glEnable(GL11.GL_BLEND);
	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	GL11.glPushMatrix();		

	GL11.glColor4d(1,1,1,1);
	
}

public static void close() {
	GL11.glPopMatrix();
	GL11.glDisable(GL11.GL_BLEND);
	GL11.glMatrixMode(GL11.GL_MODELVIEW);
	
}

}	
