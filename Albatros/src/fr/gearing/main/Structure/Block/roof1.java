package fr.gearing.main.Structure.Block;

public class roof1 {
	private float width;
	private float height;
	private float dephth;
	
	private float x;
	private float y;
	private float y1;
	private float y2;
	private float z;
	
	private CubeT5 toit1;
	private CubeT8 toit2;
	
	public roof1(float Width, float Height, float Dephth, float X,float Y, float Z) {
		this.width = Width;
		this.height = Height/2;
		this.dephth = Dephth;
		this.x = X;
		this.y = Y;
		this.z = Z;
		
		renitialize();

		
	}
	
	public void display() {
		toit1.display();
		toit2.display();
	}
	
	private void renitialize() {
		this.y1 = y+this.height/2;
		this.y2 = y-this.height/2;
		toit1 = new CubeT5(this.width, this.height, this.dephth, this.x, this.y1, this.z);
		toit2 = new CubeT8(this.width, this.height, this.dephth, this.x, this.y2, this.z);

	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
		renitialize();
	}

	public float getheight() {
		return height;
	}

	public void setheight(float height) {
		this.height = height;
		renitialize();
	}

	public float getDephth() {
		return dephth;
	}

	public void setDephth(float dephth) {
		this.dephth = dephth;
		renitialize();
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
		renitialize();
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
		renitialize();
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
		renitialize();
	}
}
