package fr.gearing.main.Structure.Block;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import org.lwjgl.opengl.GL11;

 class CubeT6 {
	private float X1;
	private float X2;
	
	private float Y1;
	private float Y2;
	
	private float Z1;
	private float Z2;
	
	public CubeT6(float Width, float Height, float Dephth, float X, float Y, float Z) {
		
		this.setX1(X + (Width/2));
		this.setX2(X - (Width/2));
		
		this.setY1(Y + (Height/2));
		this.setY2(Y - (Height/2));
		
		this.setZ1(Z + (Dephth/2));
		this.setZ2(Z - (Dephth/2));
		
	}
	
	public void display(){
		Loader.initialization();
		
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);glVertex3f(X2, Y1, Z1);
		glTexCoord2f(1, 0);glVertex3f(X1, Y1, Z1);
		glTexCoord2f(1, 1);glVertex3f(X1, Y2, Z2);
		glTexCoord2f(0, 1);glVertex3f(X2, Y2, Z2);
		glEnd();
		
		//fond
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);glVertex3f(X2, Y1, Z1);
		glTexCoord2f(1, 0);glVertex3f(X1, Y1, Z1);
		glTexCoord2f(1, 1);glVertex3f(X1, Y2, Z1);
		glTexCoord2f(0, 1);glVertex3f(X2, Y2, Z1);
		glEnd();
		
		
		//sol
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);glVertex3f(X1, Y2, Z1);
		glTexCoord2f(1, 0);glVertex3f(X1, Y2, Z2);
		glTexCoord2f(1, 1);glVertex3f(X2, Y2, Z2);
		glTexCoord2f(0, 1);glVertex3f(X2, Y2, Z1);
		glEnd();
		
		

		//droit
		glBegin(GL11.GL_TRIANGLES);
		glTexCoord2f(0, 0);glVertex3f(X1, Y1, Z1);
		glTexCoord2f(1, 1);glVertex3f(X1, Y2, Z2);
		glTexCoord2f(0, 1);glVertex3f(X1, Y2, Z1);
		glEnd();
		
		//gauche
		glBegin(GL11.GL_TRIANGLES);
		glTexCoord2f(0, 0);glVertex3f(X2, Y1, Z1);
		glTexCoord2f(1, 1);glVertex3f(X2, Y2, Z2);
		glTexCoord2f(0, 1);glVertex3f(X2, Y2, Z1);
		glEnd();
		
		Loader.close();
	}

	public float getX1() {
		return X1;
	}

	public void setX1(float x1) {
		X1 = x1;
	}

	public float getX2() {
		return X2;
	}

	public void setX2(float x2) {
		X2 = x2;
	}

	public float getY1() {
		return Y1;
	}

	public void setY1(float y1) {
		Y1 = y1;
	}

	public float getY2() {
		return Y2;
	}

	public void setY2(float y2) {
		Y2 = y2;
	}

	public float getZ1() {
		return Z1;
	}

	public void setZ1(float z1) {
		Z1 = z1;
	}

	public float getZ2() {
		return Z2;
	}

	public void setZ2(float z2) {
		Z2 = z2;
	}
}