package fr.gearing.main.Structure.Block;

public class CubeT {
	private int T;
	private CubeT1 cubeT1;
	private CubeT2 cubeT2;
	private CubeT3 cubeT3;
	private CubeT4 cubeT4;
	private CubeT5 cubeT5;
	private CubeT6 cubeT6;
	private CubeT7 cubeT7;
	private CubeT8 cubeT8;
	private CubeT9 cubeT9;
	private CubeT10 cubeT10;
	private CubeT11 cubeT11;
	private CubeT12 cubeT12;
	
	public CubeT(float Width, float Height, float Dephth, float X, float Y, float Z, int T) {//T is number of CubeT
		this.T = T;
		if(T == 1) {
			this.cubeT1 = new CubeT1(Width, Height, Dephth, X, Y, Z);
		}else if(T == 2) {
			this.cubeT2 = new CubeT2(Width, Height, Dephth, X, Y, Z);
		}else if(T == 3) {
			this.cubeT3 = new CubeT3(Width, Height, Dephth, X, Y, Z);
		}else if(T == 4) {
			this.cubeT4 = new CubeT4(Width, Height, Dephth, X, Y, Z);
		}else if(T == 5) {
			this.cubeT5 = new CubeT5(Width, Height, Dephth, X, Y, Z);
		}else if(T == 6) {
			this.cubeT6 = new CubeT6(Width, Height, Dephth, X, Y, Z);
		}else if(T == 7) {
			this.cubeT7 = new CubeT7(Width, Height, Dephth, X, Y, Z);
		}else if(T == 8) {
			this.cubeT8 = new CubeT8(Width, Height, Dephth, X, Y, Z);
		}else if(T == 9) {
			this.cubeT9 = new CubeT9(Width, Height, Dephth, X, Y, Z);
		}else if(T == 10) {
			this.cubeT10 = new CubeT10(Width, Height, Dephth, X, Y, Z);
		}else if(T == 11) {
			this.cubeT11 = new CubeT11(Width, Height, Dephth, X, Y, Z);
		}else if(T == 12) {
			this.cubeT12 = new CubeT12(Width, Height, Dephth, X, Y, Z);
		}
		
	}
	public void display(){
		if(T == 1) {
			this.cubeT1.display();
		}else if(T == 2) {
			this.cubeT2.display();
		}else if(T == 3) {
			this.cubeT3.display();
		}else if(T == 4) {
			this.cubeT4.display();
		}else if(T == 5) {
			this.cubeT5.display();
		}else if(T == 6) {
			this.cubeT6.display();
		}else if(T == 7) {
			this.cubeT7.display();
		}else if(T == 8) {
			this.cubeT8.display();
		}else if(T == 9) {
			this.cubeT9.display();
		}else if(T == 10) {
			this.cubeT10.display();
		}else if(T == 11) {
			this.cubeT11.display();
		}else if(T == 12) {
			this.cubeT12.display();
		}
	}
	public float getX1() {
		if(T == 1) {
			return this.cubeT1.getX1();
		}else if(T == 2) {
			return this.cubeT2.getX1();
		}else if(T == 3) {
			return this.cubeT3.getX1();
		}else if(T == 4) {
			return this.cubeT4.getX1();
		}else if(T == 5) {
			return this.cubeT5.getX1();
		}else if(T == 6) {
			return this.cubeT6.getX1();
		}else if(T == 7) {
			return this.cubeT7.getX1();
		}else if(T == 8) {
			return this.cubeT8.getX1();
		}else if(T == 9) {
			return this.cubeT9.getX1();
		}else if(T == 10) {
			return this.cubeT10.getX1();
		}else if(T == 11) {
			return this.cubeT11.getX1();
		}else if(T == 12) {
			return this.cubeT12.getX1();
		}else {
			return -0.0f;
		}
	}

	public void setX1(float x) {
		if(T == 1) {
			 this.cubeT1.setX1(x);
		}else if(T == 2) {
			 this.cubeT2.setX1(x);
		}else if(T == 3) {
			 this.cubeT3.setX1(x);
		}else if(T == 4) {
			 this.cubeT4.setX1(x);
		}else if(T == 5) {
			 this.cubeT5.setX1(x);
		}else if(T == 6) {
			 this.cubeT6.setX1(x);
		}else if(T == 7) {
			 this.cubeT7.setX1(x);
		}else if(T == 8) {
			 this.cubeT8.setX1(x);
		}else if(T == 9) {
			 this.cubeT9.setX1(x);
		}else if(T == 10) {
			 this.cubeT10.setX1(x);
		}else if(T == 11) {
			 this.cubeT11.setX1(x);
		}else if(T == 12) {
			this.cubeT12.setX1(x);
		}else {
			
		}
	}

	public float getX2() {
		if(T == 1) {
			return this.cubeT1.getX2();
		}else if(T == 2) {
			return this.cubeT2.getX2();
		}else if(T == 3) {
			return this.cubeT3.getX2();
		}else if(T == 4) {
			return this.cubeT4.getX2();
		}else if(T == 5) {
			return this.cubeT5.getX2();
		}else if(T == 6) {
			return this.cubeT6.getX2();
		}else if(T == 7) {
			return this.cubeT7.getX2();
		}else if(T == 8) {
			return this.cubeT8.getX2();
		}else if(T == 9) {
			return this.cubeT9.getX2();
		}else if(T == 10) {
			return this.cubeT10.getX2();
		}else if(T == 11) {
			return this.cubeT11.getX2();
		}else if(T == 12) {
			return this.cubeT12.getX2();
		}else {
			return -0.0f;
		}
	}

	public void setX2(float x) {
		if(T == 1) {
			 this.cubeT1.setX2(x);
		}else if(T == 2) {
			 this.cubeT2.setX2(x);
		}else if(T == 3) {
			 this.cubeT3.setX2(x);
		}else if(T == 4) {
			 this.cubeT4.setX2(x);
		}else if(T == 5) {
			 this.cubeT5.setX2(x);
		}else if(T == 6) {
			 this.cubeT6.setX2(x);
		}else if(T == 7) {
			 this.cubeT7.setX2(x);
		}else if(T == 8) {
			 this.cubeT8.setX2(x);
		}else if(T == 9) {
			 this.cubeT9.setX2(x);
		}else if(T == 10) {
			 this.cubeT10.setX2(x);
		}else if(T == 11) {
			 this.cubeT11.setX2(x);
		}else if(T == 12) {
			this.cubeT12.setX2(x);
		}else {
			
		}
	}

	public float getY1() {
		if(T == 1) {
			return this.cubeT1.getY1();
		}else if(T == 2) {
			return this.cubeT2.getY1();
		}else if(T == 3) {
			return this.cubeT3.getY1();
		}else if(T == 4) {
			return this.cubeT4.getY1();
		}else if(T == 5) {
			return this.cubeT5.getY1();
		}else if(T == 6) {
			return this.cubeT6.getY1();
		}else if(T == 7) {
			return this.cubeT7.getY1();
		}else if(T == 8) {
			return this.cubeT8.getY1();
		}else if(T == 9) {
			return this.cubeT9.getY1();
		}else if(T == 10) {
			return this.cubeT10.getY1();
		}else if(T == 11) {
			return this.cubeT11.getY1();
		}else if(T == 12) {
			return this.cubeT12.getY1();
		}else {
			return -0.0f;
		}
	}

	public void setY1(float y) {
		if(T == 1) {
			 this.cubeT1.setY1(y);
		}else if(T == 2) {
			 this.cubeT2.setY1(y);
		}else if(T == 3) {
			 this.cubeT3.setY1(y);
		}else if(T == 4) {
			 this.cubeT4.setY1(y);
		}else if(T == 5) {
			 this.cubeT5.setY1(y);
		}else if(T == 6) {
			 this.cubeT6.setY1(y);
		}else if(T == 7) {
			 this.cubeT7.setY1(y);
		}else if(T == 8) {
			 this.cubeT8.setY1(y);
		}else if(T == 9) {
			 this.cubeT9.setY1(y);
		}else if(T == 10) {
			 this.cubeT10.setY1(y);
		}else if(T == 11) {
			 this.cubeT11.setY1(y);
		}else if(T == 12) {
			this.cubeT12.setY1(y);
		}else {
			
		}
	}

	public float getY2() {
		if(T == 1) {
			return this.cubeT1.getY2();
		}else if(T == 2) {
			return this.cubeT2.getY2();
		}else if(T == 3) {
			return this.cubeT3.getY2();
		}else if(T == 4) {
			return this.cubeT4.getY2();
		}else if(T == 5) {
			return this.cubeT5.getY2();
		}else if(T == 6) {
			return this.cubeT6.getY2();
		}else if(T == 7) {
			return this.cubeT7.getY2();
		}else if(T == 8) {
			return this.cubeT8.getY2();
		}else if(T == 9) {
			return this.cubeT9.getY2();
		}else if(T == 10) {
			return this.cubeT10.getY2();
		}else if(T == 11) {
			return this.cubeT11.getY2();
		}else if(T == 12) {
			return this.cubeT12.getY2();
		}else {
			return -0.0f;
		}
	}

	public void setY2(float y) {
		if(T == 1) {
			 this.cubeT1.setY2(y);
		}else if(T == 2) {
			 this.cubeT2.setY2(y);
		}else if(T == 3) {
			 this.cubeT3.setY2(y);
		}else if(T == 4) {
			 this.cubeT4.setY2(y);
		}else if(T == 5) {
			 this.cubeT5.setY2(y);
		}else if(T == 6) {
			 this.cubeT6.setY2(y);
		}else if(T == 7) {
			 this.cubeT7.setY2(y);
		}else if(T == 8) {
			 this.cubeT8.setY2(y);
		}else if(T == 9) {
			 this.cubeT9.setY2(y);
		}else if(T == 10) {
			 this.cubeT10.setY2(y);
		}else if(T == 11) {
			 this.cubeT11.setY2(y);
		}else if(T == 12) {
			this.cubeT12.setY2(y);
		}else {
			
		}
	}

	public float getZ1() {
		if(T == 1) {
			return this.cubeT1.getZ1();
		}else if(T == 2) {
			return this.cubeT2.getZ1();
		}else if(T == 3) {
			return this.cubeT3.getZ1();
		}else if(T == 4) {
			return this.cubeT4.getZ1();
		}else if(T == 5) {
			return this.cubeT5.getZ1();
		}else if(T == 6) {
			return this.cubeT6.getZ1();
		}else if(T == 7) {
			return this.cubeT7.getZ1();
		}else if(T == 8) {
			return this.cubeT8.getZ1();
		}else if(T == 9) {
			return this.cubeT9.getZ1();
		}else if(T == 10) {
			return this.cubeT10.getZ1();
		}else if(T == 11) {
			return this.cubeT11.getZ1();
		}else if(T == 12) {
			return this.cubeT12.getZ1();
		}else {
			return -0.0f;
		}
	}

	public void setZ1(float z) {
		if(T == 1) {
			 this.cubeT1.setZ1(z);
		}else if(T == 2) {
			 this.cubeT2.setZ1(z);
		}else if(T == 3) {
			 this.cubeT3.setZ1(z);
		}else if(T == 4) {
			 this.cubeT4.setZ1(z);
		}else if(T == 5) {
			 this.cubeT5.setZ1(z);
		}else if(T == 6) {
			 this.cubeT6.setZ1(z);
		}else if(T == 7) {
			 this.cubeT7.setZ1(z);
		}else if(T == 8) {
			 this.cubeT8.setZ1(z);
		}else if(T == 9) {
			 this.cubeT9.setZ1(z);
		}else if(T == 10) {
			 this.cubeT10.setZ1(z);
		}else if(T == 11) {
			 this.cubeT11.setZ1(z);
		}else if(T == 12) {
			this.cubeT12.setZ1(z);
		}else {
			
		}
	}

	public float getZ2() {
		if(T == 1) {
			return this.cubeT1.getZ2();
		}else if(T == 2) {
			return this.cubeT2.getZ2();
		}else if(T == 3) {
			return this.cubeT3.getZ2();
		}else if(T == 4) {
			return this.cubeT4.getZ2();
		}else if(T == 5) {
			return this.cubeT5.getZ2();
		}else if(T == 6) {
			return this.cubeT6.getZ2();
		}else if(T == 7) {
			return this.cubeT7.getZ2();
		}else if(T == 8) {
			return this.cubeT8.getZ2();
		}else if(T == 9) {
			return this.cubeT9.getZ2();
		}else if(T == 10) {
			return this.cubeT10.getZ2();
		}else if(T == 11) {
			return this.cubeT11.getZ2();
		}else if(T == 12) {
			return this.cubeT12.getZ2();
		}else {
			return -0.0f;
		}
	}

	public void setZ2(float z) {
		if(T == 1) {
			 this.cubeT1.setZ2(z);
		}else if(T == 2) {
			 this.cubeT2.setZ2(z);
		}else if(T == 3) {
			 this.cubeT3.setZ2(z);
		}else if(T == 4) {
			 this.cubeT4.setZ2(z);
		}else if(T == 5) {
			 this.cubeT5.setZ2(z);
		}else if(T == 6) {
			 this.cubeT6.setZ2(z);
		}else if(T == 7) {
			 this.cubeT7.setZ2(z);
		}else if(T == 8) {
			 this.cubeT8.setZ2(z);
		}else if(T == 9) {
			 this.cubeT9.setZ2(z);
		}else if(T == 10) {
			 this.cubeT10.setZ2(z);
		}else if(T == 11) {
			 this.cubeT11.setZ2(z);
		}else if(T == 12) {
			this.cubeT12.setZ2(z);
		}else {
			
		}
	}
}
