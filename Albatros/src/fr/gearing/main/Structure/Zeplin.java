package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cone3;
import fr.gearing.main.Structure.Block.Cone4;
import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.CubeT;
import fr.gearing.main.Structure.Block.Cylindre;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Zeplin extends Structure{


	
	private List<CubeT> fin;
	private Cube mid2;
	private Cylindre mid;
	private Cone3 bot;
	private Cone4 top;
	

	
	private float T = save.tstructure;
	
	public Zeplin(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T;
		
		//housse
		mid2 = new Cube(T/2, T, T/2, x, y , z-T/2);
		mid = new Cylindre(T, T*2, T, x, y, z);
		bot = new Cone3(T, T/2, T, x, y+T*1.25f, z);
		top = new Cone4(T, T, T, x, y-T*1.5f, z);

		
		fin = new ArrayList<>();
		fin.add(new CubeT(T/20, T*0.8f, T*0.8f, x, y-T*1.4f, z-T/2, 6));
		fin.add(new CubeT(T/20, T*0.8f, T*0.8f, x, y-T*1.4f, z+T/2, 5));
		fin.add(new CubeT(T*0.8f, T*0.8f, T/20, x+T/2, y-T*1.4f, z, 3));
		fin.add(new CubeT(T*0.8f, T*0.8f, T/20, x-T/2, y-T*1.4f, z, 4));

		
		

	}
	@Override
	public void display() {

		Texture.metal.bind();
		mid2.display();
		
		Texture.Wool.bind();
		mid.display();
		bot.display();
		top.display();
		
		for(int tow = 0; tow < fin.size(); tow++) {
			fin.get(tow).display();
		}

	}
		
}
