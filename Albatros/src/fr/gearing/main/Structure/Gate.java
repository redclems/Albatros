package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.CubeT;
import fr.gearing.main.Structure.Block.Cylindre2;
import fr.gearing.main.Structure.Block.Cylindre3;
import fr.gearing.main.Structure.Block.Cylindre4;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;
import fr.gearing.main.Structure.Block.Paint2DIn3d;

public class Gate extends Structure{
	
	
	private List<Cube> stair;
	private Cylindre2 pillar1;
	private Cylindre4 pillar2;
	private Cylindre2 pillar3;
	private Cylindre3 pillar4;
	
	private Cube doorBot;
	private CubeT doorTop1;
	private CubeT doorTop2;
	private Paint2DIn3d textureDoor;
	//arch
	private List<Cube> cubeArch;
	private List<CubeT> cubeTArch;
	
	private float T = save.tstructure;
	
	public Gate(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/12;

		this.stair = new ArrayList<>(); 
		
		//stair
		this.stair.add(new Cube(T*6, T*3, T/6, x, y, z));
		this.stair.add(new Cube(T*6-T/3, T*3-T/3, T/6, x, y, z+(T/3)/2));
		this.stair.add(new Cube(T*6-(T/3)*2, T*3-(T/3)*2, T/6, x, y, z+((T/3)/2)*2));
		
		//pillar
		this.pillar1 = new Cylindre2(T, T*4, T, x+((T*6-(T/3)*2)/2)-T/2, z+((T/3)/2)*2+(T*4)/2, y);
		this.pillar2 = new Cylindre4(T, T*2, T, x+((T*6-(T/3)*2)/2)-T/2, z+((T/3)/2)*2+(T*4)/2+T*3, y, 2.16f);
		this.pillar3 = new Cylindre2(T, T*4, T, x-((T*6-(T/3)*2)/2)+T/2, z+((T/3)/2)*2+(T*4)/2, y);
		this.pillar4 = new Cylindre3(T, T*2, T, x-((T*6-(T/3)*2)/2)+T/2, z+((T/3)/2)*2+(T*4)/2+T*3, y, 2.16f);
		
		//door
		this.doorBot = new Cube(T*4, T/4f, T*4, x, y, z+((T/3)/2)*2+T*2);
		this.doorTop1 = new CubeT(T*2, T/4, T*2, x+T, y, z+((T/3)/2)*2+T*5f, 9);
		this.doorTop2 = new CubeT(T*2, T/4, T*2, x-T, y, z+((T/3)/2)*2+T*5f, 10);
		this.textureDoor = new Paint2DIn3d(T*3.25f, T/3.80f, T*6, x, y, z+((T/3)/2)*2+T*3f);
		
		//arch1
		cubeArch = new ArrayList<>(); 
		cubeTArch = new ArrayList<>(); 
		this.cubeArch.add(new Cube(T, T/2, T*2.5f, x+((T*6-(T/3)*2)/2)*1.5f, y, z+(T*2.5f)/2));
		this.cubeTArch.add(new CubeT(T, T/2, T, x+((T*6-(T/3)*2)/2)*1.5f, y, z+(T*2.5f)+T/2, 9));
		this.cubeTArch.add(new CubeT(T, T/2, T, (x+((T*6-(T/3)*2)/2)*1.5f)-T, y, z+(T*2.5f)+T/2, 11));
		this.cubeTArch.add(new CubeT(T, T/2, T, (x+((T*6-(T/3)*2)/2)*1.5f)-T, y, z+(T*2.5f)+T/2+T, 9));

		  //arch2
		  this.cubeArch.add(new Cube(T, T/2, T*2.5f, x-((T*6-(T/3)*2)/2)*1.5f, y, z+(T*2.5f)/2));
		  this.cubeTArch.add(new CubeT(T, T/2, T, x-((T*6-(T/3)*2)/2)*1.5f, y, z+(T*2.5f)+T/2, 10));
		  this.cubeTArch.add(new CubeT(T, T/2, T, (x-((T*6-(T/3)*2)/2)*1.5f)+T, y, z+(T*2.5f)+T/2, 12));
		  this.cubeTArch.add(new CubeT(T, T/2, T, (x-((T*6-(T/3)*2)/2)*1.5f)+T, y, z+(T*2.5f)+T/2+T, 10));

		  //arch3
		  this.cubeArch.add(new Cube(T/2, T/2, T*2.4f, x+((T*6-(T/3)*2)/2)-T/2, y+T*2, z+(T*2.4f)/2));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y+T*2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 5));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y+T*2-T/2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 7));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y+T*2-(T/2)*1, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 5));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y+T*2-(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 7));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y+T*2-(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 5));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y+T*2-(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 7));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y+T*2-(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*3, 5));


		  //arch4
		  this.cubeArch.add(new Cube(T/2, T/2, T*2.4f, x-((T*6-(T/3)*2)/2)+T/2, y+T*2, z+(T*2.4f)/2));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y+T*2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 5));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y+T*2-T/2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 7));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y+T*2-(T/2)*1, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 5));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y+T*2-(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 7));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y+T*2-(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 5));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y+T*2-(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 7));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y+T*2-(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*3, 5));


		  //arch3
		  this.cubeArch.add(new Cube(T/2, T/2, T*2.4f, x+((T*6-(T/3)*2)/2)-T/2, y-T*2, z+(T*2.4f)/2));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y-T*2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 8));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y-T*2+T/2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 6));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y-T*2+(T/2)*1, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 8));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y-T*2+(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 6));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y-T*2+(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 8));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y-T*2+(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 6));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x+((T*6-(T/3)*2)/2)-T/2, y-T*2+(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*3, 8));


		  //arch4
		  
		  this.cubeArch.add(new Cube(T/2, T/2, T*2.4f, x-((T*6-(T/3)*2)/2)+T/2, y-T*2, z+(T*2.4f)/2));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y-T*2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 8));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y-T*2+T/2, z+((T*2.5f)/2)+T*1.75f/2+T/4, 6));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y-T*2+(T/2)*1, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 8));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y-T*2+(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2), 6));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y-T*2+(T/2)*2, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 8));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y-T*2+(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*2, 6));
		  this.cubeTArch.add(new CubeT(T/2, T/2, T/2, x-((T*6-(T/3)*2)/2)+T/2, y-T*2+(T/2)*3, z+((T*2.5f)/2)+T*1.75f/2+T/4+(T/2)*3, 8));
		
	}
	@Override
	public void display() {
		
		Texture.stairgate.bind();
		for(int cube = 0; cube < stair.size(); cube++) {
			this.stair.get(cube).display();
		}
		Texture.marble.bind();
		pillar1.display();
		pillar2.display();
		pillar3.display();
		pillar4.display();
		
		Texture.doorwood.bind();
		doorBot.display();
		doorTop1.display();
		doorTop2.display();
		Texture.DoorGraph.bind();
		textureDoor.display();
		
		Texture.archT.bind();
		for(int cube = 0; cube < cubeArch.size(); cube++) {
			this.cubeArch.get(cube).display();
		}
		for(int cube = 0; cube < cubeTArch.size(); cube++) {
			this.cubeTArch.get(cube).display();
		}

		
	}
		
	
}
