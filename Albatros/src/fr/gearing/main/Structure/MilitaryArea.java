package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Cylindre2;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class MilitaryArea extends Structure{
	private List<Cube> wall;
	private List<MilitaryHousse> housse;
	
	private Cylindre2 flagBot;
	private Cylindre2 flag;

	

	
	private float T = save.tstructure;
	
	public MilitaryArea(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/4;
		
		wall = new ArrayList<>();
		wall.add(new Cube(T*4, T/10, T/2, x, y+T*2, z));
		wall.add(new Cube(T/10, T*4, T/2, x+T*2, y, z));
		
		wall.add(new Cube(T/10, T*4, T/2, x-T*2, y, z));
		wall.add(new Cube(T*2-T/5, T/10, T/2, x-T-T/5, y-T*2, z));
		wall.add(new Cube(T*2-T/5, T/10, T/2, x+T+T/5, y-T*2, z));

		flagBot = new Cylindre2(T/10, T, T/10, x-T/1.5f, z-T/4+T/10+T/2, y);
		flag = new Cylindre2(T/2.5f, T/5, T/2.5f, x-T/1.5f, z-T/4, y);
		
		housse = new ArrayList<>();
		housse.add(new MilitaryHousse(x+T*1, y+T*1.5f, z));
		housse.add(new MilitaryHousse(x+T*1, y, z));
		housse.add(new MilitaryHousse(x+T*1+T/10, y-T*1.5f, z));
		housse.add(new MilitaryHousse(x-T*1, y+T*1.5f, z));

	}
	@Override
	public void display() {
		Texture.metal.bind();
		for(int wal = 0; wal < wall.size(); wal++) {
			wall.get(wal).display();
		}
		flagBot.display();
		Texture.bottowerstone.bind();
		flag.display();

		Texture.trees.bind();
		int stop = 1;
		if(this.level > 1 ) {
			stop+=1;
		}
		if(this.level > 2 ) {
			stop+=1;
		}
		if(this.level > 3 ) {
			stop+=1;
		}
		
		for(int house = 0; house < stop; house++) {
			housse.get(house).display();
		}

	}
		
}

