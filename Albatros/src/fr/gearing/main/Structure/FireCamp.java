package fr.gearing.main.Structure;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Paint2DIn3d;
import fr.gearing.main.Structure.Block.Paint2DIn3d2;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class FireCamp extends Structure{
	
	private Cube wood;
	private Paint2DIn3d couche1;
	private Paint2DIn3d2 couche2;


	

	
	private float T = save.tstructure;
	
	public FireCamp(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		initialize();
		
	}
	@Override
	public void initialize() {

		
		//housse
		wood = new Cube(T/2, T/2, T/100, x, y, z-T/4);
		couche1 = new Paint2DIn3d(T/3, T/100, T/2, x, y, z-T/8);
		couche2 = new Paint2DIn3d2(T/100, T/3, T/2, x, y, z-T/8);
	}
	@Override
	public void display() {

		Texture.firecamp.bind();
		wood.display();
		Texture.fire.bind();
		couche1.display();
		couche2.display();

	}
		
}
