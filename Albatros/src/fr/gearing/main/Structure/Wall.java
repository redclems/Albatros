package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Wall extends Structure {
	
	private Cube cubeBot;
	private Cube cubeTop;
	
	private List<Cube> niche;

	
	private float T = save.tstructure;
	
	public Wall(float x, float y, float z, int level, byte turn) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dWall;
		this.turn = turn;
		this.rotation = true;
		initialize();
		
	}
	@Override
	public void initialize() {
		if(this.turn == 1) {
			initialize1();
		}else if(this.turn == 2) {
			initialize2();
		}else if(this.turn == 3) {
			initialize3();
		}else if(this.turn == 4) {
			initialize4();
		}
		
	}
	private void initialize1() {
		float z = this.z+T/2;
		this.cubeBot = new Cube(T, T/2, T, x, y+T/2-T/4, z);
		this.cubeTop = new Cube(T, T/2, T, x, y+T/2-T/4, z+T);
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y+T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y+T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*3-T/2), y+T/4, z+(((T*4)*1.5f)+T/2)));
	}
	
	private void initialize2() {

		float z = this.z+T/2;
		this.cubeBot = new Cube(T/2, T, T, x+T/2-T/4, y, z);
		this.cubeTop = new Cube(T/2, T, T, x+T/2-T/4, y, z+T);
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T-T/2), z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x+T/4, y+((T*2)-T*3-T/2), z+(((T*4)*1.5f)+T/2)));
	}
	private void initialize3() {

		
		float z = this.z+T/2;
		this.cubeBot = new Cube(T, T/2, T, x, y-T/2+T/4, z);
		this.cubeTop = new Cube(T, T/2, T, x, y-T/2+T/4, z+T);
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T/2), y-T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*2-T/2), y-T/4, z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T, T/2, T, x+((T*2)-T*3-T/2), y-T/4, z+(((T*4)*1.5f)+T/2)));
	}
	private void initialize4() {

		float z = this.z+T/2;
		this.cubeBot = new Cube(T/2, T, T, x-T/2+T/4, y, z);
		this.cubeTop = new Cube(T/2, T, T, x-T/2+T/4, y, z+T);
		float T = this.T/4;
		this.niche = new ArrayList<>(); 
				
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T-T/2), z+(((T*4)*1.5f)+T/2)));
		
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2)));
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T*2-T/2), z+(((T*4)*1.5f)+T/2+T)));
		
		this.niche.add(new Cube(T/2, T, T, x-T/4, y+((T*2)-T*3-T/2), z+(((T*4)*1.5f)+T/2)));
	}
	
	@Override
	public void display() {
		if(this.level == 1) {
			
			Texture.bottowerwood.bind();
		}else if(this.level == 2) {
			
			Texture.bottowerstone.bind();
		}else if(this.level == 3) {
			
			Texture.bottoweriron.bind();
		}else if(this.level == 4) {
			
			Texture.bottowerobs.bind();
		}
		cubeBot.display();
		cubeTop.display();
		for(int cube = 0; cube < niche.size(); cube++) {
			niche.get(cube).display();
		}
	}
		
	
}

