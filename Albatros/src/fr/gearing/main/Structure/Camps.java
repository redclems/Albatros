package fr.gearing.main.Structure;


import fr.gearing.main.game.save;


public class Camps extends Structure{
	

	private tent tent1;
	private tent tent2;
	private FireCamp fire;

	
	private float T = save.tstructure;
	
	public Camps(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/4;
		

		tent1 = new tent(x-T, y+T/2, z);
		tent2 = new tent(x+T/2, y+T/2, z);

		fire = new FireCamp(x-T/2, y-T/2, z);

	}
	@Override
	public void display() {

		tent2.display();
		
		if(this.level > 2 ) {
			tent1.display();
		}
		
		fire.display();
	}
		
}
