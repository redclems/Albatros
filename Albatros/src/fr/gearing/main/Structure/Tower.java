package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Tower extends Structure{
	
	private Cube cubeBot;
	private Cube cubeMid;
	private Cube cubeTop;
	
	private List<Cube> niche;

	
	private float T = save.tstructure;
	
	public Tower(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/2;
		this.cubeBot = new Cube(T, T, T, x, y, z);
		this.cubeMid = new Cube(T, T, T, x, y, z+T);
		this.cubeTop = new Cube(T, T, T, x, y, z+T*2);
		float T = this.T/5;
		this.niche = new ArrayList<>(); 
		
		
		this.niche.add(new Cube(T, T, T*2, x+((T*2.5f)-T/2), y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T/2), y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T/2), y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T/2), y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x, y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x, y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x, y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x, y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T/2), y, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T/2), y, z+(((T*5)*2.5f)+T/2+T)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T/2), y+(T*2.5f)-T*1.5f, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T*1.5f), y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y+(T*2.5f)-T*1.5f, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T*1.5f), y+(T*2.5f)-T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T/2), y-(T*2.5f)+T*1.5f, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x-((T*2.5f)-T*1.5f), y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T/2), y-(T*2.5f)+T*1.5f, z+(((T*5)*2.5f)+T/2)));
		this.niche.add(new Cube(T, T, T, x+((T*2.5f)-T*1.5f), y-(T*2.5f)+T/2, z+(((T*5)*2.5f)+T/2)));
	}
	@Override
	public void display() {
		if(this.level == 1) {
			//wood
			Texture.bottowerwood.bind();
		}else if(this.level == 2) {
			//stone
			Texture.bottowerstone.bind();
		}else if(this.level == 3) {
			//iron
			Texture.bottoweriron.bind();
		}else if(this.level == 4) {
			//argon
			Texture.bottowerobs.bind();
		}
		cubeBot.display();
		cubeMid.display();
		cubeTop.display();
		for(int cube = 0; cube < niche.size(); cube++) {
			niche.get(cube).display();
		}
	}
		
	
}
