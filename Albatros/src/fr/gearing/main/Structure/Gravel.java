package fr.gearing.main.Structure;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.CubeT;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Gravel extends Structure{
	
	private Cube tat1;
	private Cube tat2;
	private Cube tat3;
	private CubeT tat4;

	

	
	private float T = save.tstructure;
	
	public Gravel(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		initialize();
		
	}
	@Override
	public void initialize() {

		
		//housse
		tat1 = new  Cube(T*0.45f, T-T*0.35f, T*0.65f, x, y, z+(T*0.65f)/2-T/2);
		tat2 = new  Cube(T*1.92f, T-T*1.43f, T*0.35f, x, y, z+(T*0.35f)/2-T/2);
		tat3 = new  Cube(T*1.05f, T-T*0.95f, T*0.45f, x, y, z+(T*0.45f)/2-T/2);
		tat4 = new CubeT(T*1.35f, T-T*0.55f, T*0.65f, x, y, z+(T*0.65f)/2-T/2, 3);

		

	}
	@Override
	public void display() {

		Texture.gravel.bind();
		tat1.display();
		tat2.display();
		tat3.display();
		tat4.display();

	}
		
}

