package fr.gearing.main.Structure;

import java.util.ArrayList;
import java.util.List;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Paint2DIn3d;
import fr.gearing.main.Structure.Block.roof1;
import fr.gearing.main.Structure.Block.roof2;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class Forge extends Structure{

	private List<Cube> pillar;
	private Cube maison1;
	private Cube forge;
	private Paint2DIn3d texture;
	private roof1 toit1;
	private roof1 toit2;
	private roof2 toit3;
	private roof2 toit4;
	
	

	
	private float T = save.tstructure;
	
	public Forge(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/2;
		
		//housse
		maison1 = new Cube(T*1.25f, T, T, x+T/2, y+T/2, z);
		forge = new Cube(T/4, T/4, T*2, x-T/2, y-T/2+(T*2.27f)/2-T/16, z+T/2);
		texture = new Paint2DIn3d(T*1.26f, T*1.01f, T*1.01f, x+T/2, y+T/2, z);
		toit1 = new roof1(T*1.28f, T*1.1f, T/2, x+T/2, y+T/2, z+T/1.5f);
		toit2 = new roof1(T*1.27f, T*1.1f, T/2, x+T/2, y+T/2, z+T/1.3f);
		toit3 = new roof2(T*1.1f, T*2.28f, T/2, x-T/2, y-T/2, z+T/1.2f);
		toit4 = new roof2(T*1.1f, T*2.27f, T/2, x-T/2, y-T/2, z+T/1.1f);
		
		pillar = new ArrayList<>();
		pillar.add(new Cube(T/6, T/6, T*1.2f, x-T/2+(T*2.27f)/4-T/12, y-T/2+(T*2.27f)/2-T/12, z));
		pillar.add(new Cube(T/6, T/6, T*1.2f, x-T/2-(T*2.27f)/4+T/12, y-T/2+(T*2.27f)/2-T/12, z));
		pillar.add(new Cube(T/6, T/6, T*1.2f, x-T/2+(T*2.27f)/4-T/12, y-T/2-(T*2.27f)/2+T/12, z));
		pillar.add(new Cube(T/6, T/6, T*1.2f, x-T/2-(T*2.27f)/4+T/12, y-T/2-(T*2.27f)/2+T/12, z));

		
		

	}
	@Override
	public void display() {
		Texture.wallwood.bind();
		toit1.display();
		toit3.display();
		maison1.display();
		
		Texture.lumberjackHousse.bind();
		texture.display();
		
		Texture.roof2.bind();
		toit2.display();
		toit4.display();
		
		Texture.bottowerstone.bind();
		forge.display();
		
		Texture.wallwood.bind();
		for(int pill = 0; pill < pillar.size(); pill++) {
			pillar.get(pill).display();
		}

	}
		
}
