package fr.gearing.main.Structure;


import fr.gearing.main.Structure.Block.Paint2DIn3d;
import fr.gearing.main.Structure.Block.roof2;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class tent extends Structure{
	
	private roof2 tent;
	private Paint2DIn3d texture;

	
	private float T = save.tstructure;
	
	public tent(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		initialize();
		
	}
	@Override
	public void initialize() {

		
		//housse
		tent = new roof2(T*1.27f, T*1.1f, T/2, x, y, z);
		texture = new Paint2DIn3d(T*1.29f, T*1.12f, T/2, x, y, z);
	}
	@Override
	public void display() {

		Texture.Wool.bind();
		tent.display();
		Texture.campgraph.bind();
		texture.display();


	}
		
}
