package fr.gearing.main.Structure;

import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Paint2DIn3d;
import fr.gearing.main.Structure.Block.roof1;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class MilitaryHousse extends Structure{
	
	private Cube maison;
	private roof1 toit1;
	private roof1 toit2;
	private Paint2DIn3d textureHousse;

	

	
	private float T = save.tstructure;
	
	public MilitaryHousse(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		initialize();
		
	}
	@Override
	public void initialize() {

		
		//housse
		maison = new Cube(T*2, T, T-T/4, x, y, z-T/4+T/2.75f);
		toit1 = new roof1(T*2, T*1.2f-T/4, T/2, x, y, z+T/1.5f-T/3+T/2.75f);
		toit2 = new roof1(T*2.01f, T*1.2f-T/4, T/2, x, y, z+T/1.3f-T/3+T/2.75f);
		textureHousse = new Paint2DIn3d(T*1.26f, T*1.01f, T*1.01f, x, y, z);

		

	}
	@Override
	public void display() {

		Texture.military.bind();
		maison.display();
		toit1.display();
		
		Texture.roof2.bind();
		toit2.display();
		
		Texture.militaryHousse.bind();
		textureHousse.display();
		
		Texture.trees.bind();

	}
		
}

