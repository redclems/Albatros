package fr.gearing.main.Structure;


import fr.gearing.main.Structure.Block.Cube;
import fr.gearing.main.Structure.Block.Paint2DIn3d;
import fr.gearing.main.Structure.Block.roof1;
import fr.gearing.main.game.save;
import fr.gearing.main.render.Texture;

public class BuildHousse extends Structure{
	private Cube maison;
	private roof1 toit1;
	private roof1 toit2;
	private Paint2DIn3d textureHousse;
	

	
	private float T = save.tstructure;
	
	public BuildHousse(float x, float y, float z, int level) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = T;
		this.height = T;
		this.level =level;
		this.durability = save.dTower;
		this.turn = 0;
		this.rotation = false;
		initialize();
		
	}
	@Override
	public void initialize() {
		float z = this.z+T/2;
		
		//housse
		maison = new Cube(T*2, T*1.5f, T, x, y, z);
		toit1 = new roof1(T*2.28f, T*1.6f, T*1.5f/2, x, y, z+T/1.4f);
		toit2 = new roof1(T*2.27f, T*1.6f, T*1.5f/2, x, y, z+T/1.2f);;
		textureHousse = new Paint2DIn3d(T*2.26f, T*1.51f, T*1.01f, x, y, z);
				

	}
	@Override
	public void display() {

		Texture.wallwood.bind();
		maison.display();
		toit1.display();
		
		Texture.roof2.bind();
		toit2.display();
		
		Texture.lumberjackHousse.bind();
		textureHousse.display();
		
	}
}
